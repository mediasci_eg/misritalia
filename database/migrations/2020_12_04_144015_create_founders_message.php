<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoundersMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('founders_message', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('content');
            $table->timestamps();
        });

        DB::table('founders_message')->insert([
        [
            'id' => '1',
            'content' => 'In Misr Italia Holding, we aim to innovate and listen closely to our customers to provide them with the best customer experience in every aspect, whether they are looking for livable compounds, office or retail space, travel services, furniture, or wooden flooring. Built on a philosophy of creativity and innovation, we have the privilege of offering our customers trend-setting projects and products with unique concepts and designs that are unmatched in the market. This privilege, however, comes with a great deal of responsibility and accountability.            ',
        ]]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('founders_message');
    }
}
