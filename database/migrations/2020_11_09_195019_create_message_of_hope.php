<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageOfHope extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_of_hope', function (Blueprint $table) {
            $table->integer('id' ,true);
            $table->text('description')->nullable();;
            $table->text('videos')->nullable();
        });
        DB::table('message_of_hope')->insert([
        [
            'id' => '1',
            'description' => 'Facing hard times with the spread of the Covid-19 pandemic we face a tremendous challenge for all people worldwide and for our common home but everything has a reason attached to it. Maybe it’s nature’s attempt to create harmony and balance in this world and challenging us as a species, to bring out the our best sense of humanity, community and compassion. History tells us that, crisis situations like these bring out the best in everyone so with flashes of hope, positivity and with the efforts of our Covid-19 warriors one day we will conquer Covid-19.

            One of the values of Misr Italia Properties is to strive to create a better, safer and healthier living environment for our community. As part of our endless efforts to support and alleviate the negative repercussions of the Covid-19 crisis on Egyptian citizens, Misr Italia Properties has launched ‘’A Message of Hope’’ initiative to send out positivity and hope messages to different people all around us. The initiative includes different activities supporting different sectors of people all over Egypt.',
        ]]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_of_hope');
    }
}
