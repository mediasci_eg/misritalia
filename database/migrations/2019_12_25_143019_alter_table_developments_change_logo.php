<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDevelopmentsChangeLogo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('developments', function (Blueprint $table) {
        $table->string('logo')->nullable()->change();
        $table->string('overview_image')->nullable()->change();
        $table->text('overview_content')->nullable()->change();
        $table->string('location_image')->nullable()->change();
        $table->text('location_content')->nullable()->change();
        $table->string('design_image')->nullable()->change();
        $table->text('design_content')->nullable()->change();
        $table->string('unit_types_image')->nullable()->change();
        $table->text('unit_types_content')->nullable()->change();
        $table->string('lat')->nullable()->change();
        $table->string('lng')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
