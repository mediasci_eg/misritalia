<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCsr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csr', function (Blueprint $table) {
            $table->integer('id' , true);
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('image')->nullable();

            $table->string('title2')->nullable();
            $table->text('description2')->nullable();

            $table->integer('parent_id')->nullable();
            $table->boolean('has_sub')->default(0);
            $table->boolean('has_2_titles')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csr');
    }
}
