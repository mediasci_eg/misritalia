<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageOfHopeTabs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_of_hope_tabs', function (Blueprint $table) {
            $table->integer('id' , true);
            $table->integer('message_of_hope_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->text('images')->nullable();
        });

        DB::table('message_of_hope_tabs')->insert([
            [
                'message_of_hope_id' => '1',
                'title' => "Medical Equipment Donation",
                'description' => "As part of our endless efforts to support the healthcare system in Egypt since the outbreak of COVID-19, Misr Italia Properties donated 10 ICU Monitors to Abasseya Fevers Hospital. In an attempt to enable the health sector to provide healthcare services, in light of COVID-19 repercussions and the pursuit of curbing its spread.",
                'images' => "",
            ],
            [
                'message_of_hope_id' => '1',
                'title' => "Hope for Nursing Homes",
                'description' => "As part of our endless efforts to support the healthcare system in Egypt since the outbreak of COVID-19, Misr Italia Properties donated 10 ICU Monitors to Abasseya Fevers Hospital. In an attempt to enable the health sector to provide healthcare services, in light of COVID-19 repercussions and the pursuit of curbing its spread.",
                'images' => "",
            ],
            [
                'message_of_hope_id' => '1',
                'title' => "Gifts of Hope",
                'description' => "As part of our endless efforts to support the healthcare system in Egypt since the outbreak of COVID-19, Misr Italia Properties donated 10 ICU Monitors to Abasseya Fevers Hospital. In an attempt to enable the health sector to provide healthcare services, in light of COVID-19 repercussions and the pursuit of curbing its spread.",
                'images' => "",
            ],
            [
                'message_of_hope_id' => '1',
                'title' => "Box of Gratitude",
                'description' => "As part of our endless efforts to support the healthcare system in Egypt since the outbreak of COVID-19, Misr Italia Properties donated 10 ICU Monitors to Abasseya Fevers Hospital. In an attempt to enable the health sector to provide healthcare services, in light of COVID-19 repercussions and the pursuit of curbing its spread.",
                'images' => "",
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_of_hope_tabs');
    }
}
