<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DevelopmentInquiresAddUtm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('developments_inquiries', function (Blueprint $table) {
            $table->string('utm_campaign')->after('sent')->nullable();
            $table->string('utm_source')->after('sent')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('developments_inquiries', function (Blueprint $table) {
            //
        });
    }
}
