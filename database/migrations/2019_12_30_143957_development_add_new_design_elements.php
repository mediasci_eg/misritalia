<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DevelopmentAddNewDesignElements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('developments', function (Blueprint $table) {
            $table->string('masterplan')->nullable()->after('logo') ;
            $table->string('location_link')->nullable()->after('logo') ;
            $table->string('cover_image')->nullable()->after('logo') ;
            $table->dropColumn('lat');
            $table->dropColumn('lng');
            $table->dropColumn('is_in_homepage');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('developments', function (Blueprint $table) {
            //
        });
    }
}
