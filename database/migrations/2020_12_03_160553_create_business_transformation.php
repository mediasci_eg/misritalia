<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessTransformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('business_transformation', function (Blueprint $table) {
            $table->integer('id' , true);
            $table->text('description');
            $table->timestamps();
        });

		Schema::create('business_transformation_tabs', function (Blueprint $table) {
            $table->integer('id' , true);
            $table->integer('table_id');
            $table->string('title');
            $table->text('content');
            $table->boolean('has_files')->default(0);
            $table->text('files')->nullable();
            $table->timestamps();
        });

		DB::table('business_transformation')->insert([
        [
            'id' => '1',
            'description' => 'Misr Italia Properties The partnership was concluded with Misr Italia Properties doing constructions and finishing including painting while the Educational Building Authorities will be responsible for furnishing Qalyubia.',
        ]]);

		DB::table('business_transformation_tabs')->insert([
		[
			'table_id' => '1',
			'title' => "THE JOURNEY",
			'content' => "In our 3rd edition of 'Box of Hope', amidst the COVID-19 spread Misr Italia Properties partnered with Al Orman Association to distribute food supplies boxes to those in need in Martouh governorate. This year our La Nuova Vista community and our employees participated with us in the initiative to help reach the biggest number of people possible, and thanks to them we were able to reach 1,300 families.",
			'has_files' => 1
		]]);

		DB::table('business_transformation_tabs')->insert([
		[
			'table_id' => '1',
			'title' => "BUSINESS EXCELLENCE",
			'content' => "In our 3rd edition of 'Box of Hope', amidst the COVID-19 spread Misr Italia Properties partnered with Al Orman Association to distribute food supplies boxes to those in need in Martouh governorate. This year our La Nuova Vista community and our employees participated with us in the initiative to help reach the biggest number of people possible, and thanks to them we were able to reach 1,300 families.",
			'has_files' => 0
		]]);

		DB::table('business_transformation_tabs')->insert([
		[
			'table_id' => '1',
			'title' => "PROCESS PATHWAY",
			'content' => "In our 3rd edition of 'Box of Hope', amidst the COVID-19 spread Misr Italia Properties partnered with Al Orman Association to distribute food supplies boxes to those in need in Martouh governorate. This year our La Nuova Vista community and our employees participated with us in the initiative to help reach the biggest number of people possible, and thanks to them we were able to reach 1,300 families.",
			'has_files' => 0
        ]]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_transformation');
    }
}
