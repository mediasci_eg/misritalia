<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevelopmentBrochuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('development_brochures', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('development_id');
            $table->text('brochure'); 
            $table->text('thumbnail'); 
            $table->timestamps();
            $table->foreign('development_id')->references('id')->on('developments')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('development_brochures');
    }
}
