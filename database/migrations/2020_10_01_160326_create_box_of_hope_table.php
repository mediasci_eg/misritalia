<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxOfHopeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('box_of_hope', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('image')->nullable();
            $table->string('welcome_image')->nullable();
            $table->text('welcome_content')->nullable();
            $table->string('volume_1_title')->nullable();
            $table->text('volume_1_images')->nullable();
            $table->text('volume_1_content')->nullable();
            $table->string('volume_2_title')->nullable();
            $table->text('volume_2_images')->nullable();
            $table->text('volume_2_content')->nullable();
            $table->string('volume_3_title')->nullable();
            $table->text('volume_3_images')->nullable();
            $table->text('volume_3_content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('box_of_hope');
    }
}