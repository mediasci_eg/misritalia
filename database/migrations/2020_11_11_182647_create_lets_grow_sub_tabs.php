<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLetsGrowSubTabs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lets_grow_sub_tabs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('lets_grow_tab_id');
            $table->string('title');
            $table->text('description');
            $table->text('images');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lets_grow_sub_tabs');
    }
}
