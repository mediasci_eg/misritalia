<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxOfHopeData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('box_of_hope')->insert([
        [
            'id' => '1',
            'description' => 'Misr Italia Properties was able to partner with the Educational Building Authorities for the sake of constructing a primary school at Tant Gizera, Qalyubia. The partnership was concluded with Misr Italia Properties doing constructions and finishing including painting while the Educational Building Authorities will be responsible for furnishing Qalyubia.',
        ]]);
        for($i=1;$i<4;$i++){
            DB::table('box_of_hope_volums')->insert([
            [
                'box_of_hope_id' => '1',
                'title' => "VOLUME ".$i,
                'description' => "In our 3rd edition of 'Box of Hope', amidst the COVID-19 spread Misr Italia Properties partnered with Al Orman Association to distribute food supplies boxes to those in need in Martouh governorate. This year our La Nuova Vista community and our employees participated with us in the initiative to help reach the biggest number of people possible, and thanks to them we were able to reach 1,300 families.",
                'date' => "2020-07-01",
                'images' => "[]",
            ]]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
