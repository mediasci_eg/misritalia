<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxOfHopeVolums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('box_of_hope_volums', function (Blueprint $table) {
            $table->integer('id' , true);
            $table->integer('box_of_hope_id');
            $table->string('title');
            $table->text('description');
            $table->date('date');
            $table->text('images')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('box_of_hope_volums');
    }
}
