<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevelopmentsInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('developments_inquiries', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('development_id');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->text('message');
            $table->boolean('is_read')->default(0);
            $table->softDeletes();
            $table->foreign('development_id')->references('id')->on('developments')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('developments_inquiries');
    }
}
