<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatedevelopmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('developments', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('title');
            $table->string('image');
            $table->string('overview_image');
            $table->text('overview_content');
            $table->string('location_image');
            $table->text('location_content');
            $table->string('design_image');
            $table->text('design_content');
            $table->string('unit_types_image');
            $table->text('unit_types_content');
            $table->string('video')->nullable();
            $table->string('lat');
            $table->string('lng');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('developments');
    }
}
