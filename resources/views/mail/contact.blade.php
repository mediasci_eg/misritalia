<?php
    $keys = ['phone','email','message','name','inquiry_type','request_type','unit_number'];
?>
@foreach($contact->getAttributes() as $k=>$value)
    @if(in_array($k , $keys))
        <label> {{ ucfirst( str_replace('_' , ' ' ,$k))}}: </label>  {{$value}}<br>
        @endif
    @if($k=='development_id')
        <label> Development: </label>  {{@$contact->development->title}}<br>
    @endif
    @if($k=='is_homeowner')
        <label> Is Homeowner: </label>  {{($contact->is_homeowner==1)?'Yes':'No'}}<br>
    @endif
@endforeach
