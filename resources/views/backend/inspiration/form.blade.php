<?php
    use App\Libs\Upload;
?>
@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update Inspiration #'.$model->id)
@else
	@section('title' , 'Create Inspiration')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'  >
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6">
                        <label for="InspirationTitle">Title</label>
                        <input type="text" name='Inspiration[title]' class="form-control" id="InspirationTitle" placeholder="Enter Title" value='{{$model->title}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="InspirationContent">Content</label>
                        <textarea style='height:200px' name='Inspiration[content]' class="form-control" id="InspirationContent" placeholder="Enter Content">{!!$model->content!!}</textarea>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="InspirationBrochure">Brochure</label>
                        <?=Upload::pdf([
                            'name'=>'Inspiration[brochure]',
                            'value'=>$model->brochure,
                        ])?>
                    </div>


                    <div class='clearfix' ></div>
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}
    @endpush
@endif


@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/inspiration')}}">Inspirations</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop
