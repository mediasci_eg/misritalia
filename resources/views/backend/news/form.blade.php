@extends('backend.layout.master')
@if($model->exists)
@section('title' , 'Update News #'.$model->id)
@else
@section('title' , 'Create News')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post' enctype="multipart/form-data">
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6">
                        <label for="NewsTitle">Title</label>
                        <input type="text" name='News[title]' class="form-control" id="NewsTitle"
                            placeholder="Enter Title" value='{{$model->title}}'>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="NewsTitle">First Paragraph <small>({{strlen($model->first_paragraph)}})</small></label>
                        <textarea style='height:200px' type="text" name='News[first_paragraph]' class="form-control max" id="NewsTitle"
                            placeholder="Enter paragraph">{{$model->first_paragraph}}</textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="NewsTitle">Second Paragraph</label>
                        <textarea type="text" style='height:200px' name='News[second_paragraph]' class="form-control" id="NewsTitle"
                            placeholder="Enter paragraph">{{$model->second_paragraph}}</textarea>
                    </div>


                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label"> Image </label>
                            <span>(1170 * 530)</span>
                            <input type="file" class='form-control' value="{{$model->image}}" name="image">
                            @if($model->image !='')
                            <img src='{{url('uploads/'.$model->image)}}' style='width:200px'>
                            @endif
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label"> Inner Image </label>
                            <span>(840 * 520)</span>
                            <input type="file" class='form-control' value="{{$model->inner_image}}" name="inner_image">
                            @if($model->inner_image !='')
                            <img src='{{url('uploads/'.$model->inner_image)}}' style='width:200px'>
                            @endif
                        </div>
                    </div>

					<div class="form-group col-md-6">
                        <label for="NewsTitle">Date</label>
                        <input type="date" name='News[date]' class="form-control" value='{{$model->date}}'>
                    </div>

                    <div class='clearfix'></div>
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary'>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
@push('footer')
{!!$model->viewErrors()!!}
@endpush
@endif


@stop



@section('breadcrumbs')
<li class="breadcrumb-item"><a href="{{url('backend/news')}}">News</a></li>
@if($model->exists)
<li class="breadcrumb-item active">Update</li>
@else
<li class="breadcrumb-item active">Create</li>
@endif
@stop

@push('footer')
    <script>
        $(document).on('keyup change',"textarea.max", function () {
            var val = $(this).val();
                length = val.length;
                //remain = 1700 - length  ;
                $(this).parent().find('small').html('('+length+')');
                //if(remain==0)return false ;
        })
    </script>
@endpush
