<?php
use App\Http\Controllers\Helpers\Upload ;
?>
@extends('backend.layout.master')
@if($model->exists)
@section('title' , 'Update Lets grow #'.$model->id)
@else
@section('title' , 'Create Lets grow')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'>
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6">
                        <label for="LetsGrowDescrption">Descrption</label>
                        <textarea name='LetsGrow[descrption]' class="form-control" id="LetsGrowDescrption"
                            placeholder="Enter Descrption">{!!$model->descrption!!}</textarea>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="LetsGrowVideo">Video</label>
                        <input type="text" name='LetsGrow[video]' class="form-control" id="LetsGrowVideo"
                            placeholder="Enter Video" value='{{$model->video}}'>
                    </div>


                    <div class='clearfix'></div>
                    <section class='letsGrowTabs' style='width:100%'>
                        <div class="col-md-12">
                            <h3>Lets grow tabs</h3>
                        </div>
                        <?php
                            $count['letsGrowTabs'] = 0 ;
                            if(!$model->exists)
                                $letsGrowTabs = [new \App\Models\LetsGrowTab];
                            else
                                $letsGrowTabs = $model->tabs;
                                $count['letsGrowSubTabs'] = 0 ;
                        ?>
                        @foreach($letsGrowTabs as $letsGrowTab)
                        <?php $count['letsGrowTabs']++ ?>
                        <input type="hidden"  name='LetsGrowTab[{{$count['letsGrowTabs']}}][id]'  value='{{$letsGrowTab->id}}' >
                        <div class='section_content row pl-1'>
                            <div class='col-md-10 row'>
                                <div class="form-group col-md-6">
                                    <label for="LetsGrowTabTitle">Title</label>
                                    <input type="text" name='LetsGrowTab[{{$count['letsGrowTabs']}}][title]'
                                        class="form-control" id="LetsGrowTabTitle" placeholder="Enter Title"
                                        value='{{$letsGrowTab->title}}'>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="LetsGrowTabDescription">Description</label>
                                    <textarea name='LetsGrowTab[{{$count['letsGrowTabs']}}][description]'
                                        class="form-control" id="LetsGrowTabDescription"
                                        placeholder="Enter Description">{!!$letsGrowTab->description!!}</textarea>
                                </div>
                            </div>
                            <div style='margin-top: 25px;' class='col-md-2'>
                                @if($count['letsGrowTabs'] ==1)
                                <input type='button' class='add btn btn-success' value="+">
                                @else
                                <input type='button' class='remove btn btn-danger' value="X">
                                @endif
                            </div>
                        </div>

                            <div class='col-md-12'>
                                <h4>Sub Tabs</h4>
                            </div>
                            <?php

                                $letsGrowSubTabs = $letsGrowTab->tabs ;
                            ?>
                            <section class='letsGrowSubTabs' style='background: lightgrey;width: 100%;padding: 30px;margin-bottom: 20px;'>
                                @foreach($letsGrowSubTabs as $letsGrowSubTab)
                                <?php $count['letsGrowSubTabs']++ ?>
                                <div class='section_content row pl-1'>
                                    <div class='col-md-12 row'>
                                        <input type="hidden"  name='letsGrowSubTabs[{{$count['letsGrowSubTabs']}}][id]'  value='{{$letsGrowSubTab->id}}' >
                                        <div class="form-group col-md-6">
                                            <label>Title</label>
                                        <input type="text" name='letsGrowSubTabs[{{$count['letsGrowSubTabs']}}][title]'
                                                class="form-control"
                                                value='{{$letsGrowSubTab->title}}'>
                                        </div>
                                        <div class="form-group col-md-6">
                                                <label class="control-label">Event Images</label>
                                                <?= Upload::images([
                                                    'name'=>'letsGrowSubTabs['.$count['letsGrowSubTabs'].'][images][]' ,
													'size' => '840 * 520',
                                                    'value'=>$letsGrowSubTab->images ,
                                                ]); ?>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="control-label">Content</label>
                                                <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css() ?>
                                                <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('content'.$letsGrowSubTab->id , 'letsGrowSubTabs['.$count['letsGrowSubTabs'].'][description]', $letsGrowSubTab->description) ?>
                                                <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js() ?>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                @endforeach
                            </section>

                        <div class='clearfix'></div>
                        @endforeach
                    </section>

                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary'>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
@push('footer')
{!!$model->viewErrors()!!}
@endpush
@endif


@stop


@push('footer')
<script>
    $(document).ready(function(){
        count = <?=json_encode($count)?> ;
        $('.add').click(function(){
            var content = $(this).parents('.section_content')[0].outerHTML+'\n\
                                <div class="clearfix" ></div>';
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $section.append(content);
            var $lastItem = $section.find('.section_content').last();
            $lastItem.find('input , select , textarea').each(function(){
                $(this).val('');
                var name = $(this).attr('name');
                if(typeof name!='undefined'){
                    var newName = name.replace(/\d+/ ,count[$section.attr('class')] );
                    $(this).attr('name' , newName);
                }
            })

            var $lastItemBtn = $lastItem.find('.add');
            $lastItemBtn.val('X');
            $lastItemBtn.removeClass('add');
            $lastItemBtn.removeClass('btn-success');
            $lastItemBtn.addClass('btn-danger');
            $lastItemBtn.addClass('remove');
        })

        $(document).on('click','.remove' , function(){
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $(this).closest('.section_content').remove();
        })
    })

</script>
@endpush

@section('breadcrumbs')
<li class="breadcrumb-item"><a href="{{url('backend/lets_grow')}}">Lets grows</a></li>
@if($model->exists)
<li class="breadcrumb-item active">Update</li>
@else
<li class="breadcrumb-item active">Create</li>
@endif
@stop
