@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update Vacancies apply #'.$model->id)	
@else
	@section('title' , 'Create Vacancies apply')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'  >
                <div class="row">
                    {{ csrf_field() }}		
                    <div class="form-group col-md-6">
                        <?php $vacancies=\App\Models\Vacancy::all() ?>                        
                        <label for="VacanciesApplyVacancy_id">Vacancy</label>
                        <select name='VacanciesApply[vacancy_id]' class="form-control" id="VacanciesApplyVacancy_id">
                            <option value='' >Choose Vacancy</option>
                            @foreach($vacancies as $vacancy)
                                <option <?=selected($vacancy->id , $model->vacancy_id)?> value='{{$vacancy->id}}' >{{$vacancy->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="VacanciesApplyFirst_name">First name</label>
                        <input type="text" name='VacanciesApply[first_name]' class="form-control" id="VacanciesApplyFirst_name" placeholder="Enter First name" value='{{$model->first_name}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="VacanciesApplyLast_name">Last name</label>
                        <input type="text" name='VacanciesApply[last_name]' class="form-control" id="VacanciesApplyLast_name" placeholder="Enter Last name" value='{{$model->last_name}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="VacanciesApplyEmail">Email</label>
                        <input type="text" name='VacanciesApply[email]' class="form-control" id="VacanciesApplyEmail" placeholder="Enter Email" value='{{$model->email}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="VacanciesApplyPhone">Phone</label>
                        <input type="text" name='VacanciesApply[phone]' class="form-control" id="VacanciesApplyPhone" placeholder="Enter Phone" value='{{$model->phone}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="VacanciesApplyCv">Cv</label>
                        <input type="text" name='VacanciesApply[cv]' class="form-control" id="VacanciesApplyCv" placeholder="Enter Cv" value='{{$model->cv}}'>
                    </div>


                    <div class='clearfix' ></div>                    
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}	
    @endpush
@endif


@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/vacancies_apply')}}">Vacancies applies</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop