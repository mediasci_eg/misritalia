@extends('backend.layout.master')
@section('title' , 'Vacancies apply')
@section('content')


<div class="card">
    <div class="card-header">
        <div class="btn-group float-md-right">
        </div>
    </div>
    <div class="card-content overflow-auto collapse show">
        <div class="card-body card-dashboard">
                <form method='get' class='search' >
                    <table class="table table-striped table-bordered zero-configuration">
                        <tr>
                            <th style="min-width: 65px">#</th>
                            <th>Vacancy</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Cv</th>
                            <th style="width: 65px">Actions</th>
                        </tr>
                        <tr>			
                            <th><input type='text' name='id' value='{{request('id')}}' class='form-control'></th>
                        <?php $vacancies=\App\Models\Vacancy::all() ?>						
                            <th>
                                <select name='vacancy_id' class="form-control" >
                                    <option value='' >all</option>
                                    @foreach($vacancies as $vacancy)
                                        <option <?=selected($vacancy->id , request('vacancy_id') )?> value='{{$vacancy->id}}' >{{$vacancy->title}}</option>
                                    @endforeach                                            
                                </select>
                            </th>                        <th><input type='text' name='first_name' value='{{request('first_name')}}' class='form-control'></th>
                        <th><input type='text' name='last_name' value='{{request('last_name')}}' class='form-control'></th>
                        <th><input type='text' name='email' value='{{request('email')}}' class='form-control'></th>
                        <th><input type='text' name='phone' value='{{request('phone')}}' class='form-control'></th>
                        <th><input type='text' name='cv' value='{{request('cv')}}' class='form-control'></th>
                            <th style="width: 65px">
                                
                                <input type='submit' class='d-none' name='search'>
                            </th>
						
                        </tr>
                        @if($data->isNotEmpty())
                            @foreach($data as $row)
                                <tr>
                                    <td>{{$row->id}}</td>

                                    <td>{{$row->vacancy->title}}</td>

                                    <td>{{$row->first_name}}</td>


                                    <td>{{$row->last_name}}</td>


                                    <td>{{$row->email}}</td>


                                    <td>{{$row->phone}}</td>


                                    <td>{{$row->cv}}</td>
                                    <td>
                                            <a href="{{url('backend/vacancies_apply/update/'.$row->id.'')}}" title='Edit'>
                                                <i class="fa fa-lg fa-edit"></i> 
                                            </a>
                                            <a href="{{url('backend/vacancies_apply/delete/'.$row->id.'')}}" title='Delete' onclick="return confirm('Are you sure you want to delete ?')" >
                                                <i class="fa fa-lg fa-remove"></i> 
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class='text-center' colspan='100'>No Data Available</td>
                            </tr> 
                        @endif
                    </table>
                </form>
            
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                {{ $data->appends(request()->except('page'))->links() }}			
            </div>
        </div>
    </div>
</div>
@stop

@push('footer')
    <script>
        $('.search input , .search select').change(function(){
            $('.search input[type=submit]').trigger('click');
        });
        $('.view').click(function(){
            var id=$(this).data().id;
            $.ajax({
                url:"{{url('backend/vacancies_apply/view')}}/"+id,
                success:function(data){
                    $('#modal').find('.modal-title').html('View Vacancies_apply #'+id);
                    $('#modal').find('.modal-body').html(data);
                    $('#modal').modal();
                }
            })
        })
    </script>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item active">Vacancies applies</li>
@stop

