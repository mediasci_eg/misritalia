@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update Founders message #'.$model->id)
@else
	@section('title' , 'Create Founders message')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'  >
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-12">
                        <label for="FoundersMessageContent">Content</label>
                        <textarea style='height:320px' name='FoundersMessage[content]' class="form-control" id="FoundersMessageContent" placeholder="Enter Content">{!!$model->content!!}</textarea>
                    </div>


                    <div class='clearfix' ></div>
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}
    @endpush
@endif


@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/founders_message')}}">Founders messages</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop
