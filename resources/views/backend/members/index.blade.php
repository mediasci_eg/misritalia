@extends('backend.layout.master')
@section('title' , 'Members')
@section('content')


<div class="card">
    <div class="card-header">
        <div class="btn-group float-md-right">
            <a href="{{url('backend/members/create')}}" class='btn btn-info' ><i class='fa fa-plus mr-1'></i>Create</a>
            <a data-toggle="modal" data-target="#sort" class='btn btn-primary active' >Sort</a>
        </div>
    </div>
    <div class="card-content overflow-auto collapse show">
        <div class="card-body card-dashboard">
                <form method='get' class='search' >
                    <table class="table table-striped table-bordered zero-configuration">
                        <tr>
                            <th style="min-width: 65px">#</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Image</th>
                            <th>Team</th>
                            <th style="width: 65px">Actions</th>
                        </tr>
                        <tr>
                            <th><input type='text' name='id' value='{{request('id')}}' class='form-control'></th>
                        <th><input type='text' name='name' value='{{request('name')}}' class='form-control'></th>
                        <th><input type='text' name='position' value='{{request('position')}}' class='form-control'></th>

                            <th></th>
                        <th><input type='text' name='team' value='{{request('team')}}' class='form-control'></th>
                                                    <th style="width: 65px">

                                <input type='submit' class='d-none' name='search'>
                            </th>

                        </tr>
                        @if($data->isNotEmpty())
                            @foreach($data as $row)
                                <tr>
                                    <td>{{$row->id}}</td>


                                    <td>{{$row->name}}</td>


                                    <td>{{$row->position}}</td>

                                    <td>
                                        @if($row->image!='')
                                            <img src={{url('uploads/'.$row->image)}} style='width:100px' >
                                        @endif
                                    </td>

                                    <td>{{$row->team}}</td>

                                                                        <td>
                                            <a href="{{url('backend/members/update/'.$row->id.'')}}" title='Edit'>
                                                <i class="fa fa-lg fa-edit"></i>
                                            </a>
                                            <a href="{{url('backend/members/delete/'.$row->id.'')}}" title='Delete' onclick="return confirm('Are you sure you want to delete ?')" >
                                                <i class="fa fa-lg fa-remove"></i>
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class='text-center' colspan='100'>No Data Available</td>
                            </tr>
                        @endif
                    </table>
                </form>

            <!-- /.box-body -->
            <div class="box-footer clearfix">
                {{ $data->appends(request()->except('page'))->links() }}
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="sort" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="post" action='./backend/members/sort'>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <ul class="sortable">
                  @foreach($data as $row)
                        {{ csrf_field() }}                        <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                            {{$row->name}}                            <input type='hidden' name='id[]' value='{{$row->id}}'>
                        </li>
                  @endforeach
              </ul>
              <style>
                  .sortable {
                      padding: 0;
                  }
                  .sortable li{
                      list-style: none;
                      cursor: pointer;
                  }
              </style>
            </div>
            <div class="modal-footer">
              <button type="submit" name='sort' class="btn btn-primary">Sort</button>
            </div>
        </form>
    </div>
  </div>
</div>

@stop

@push('footer')
   <script src="{{url('admin/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js')}}" ></script>
   <script>
        $( function() {
            $( ".sortable" ).sortable();
        } );
   </script>
    <script>
        $('.search input , .search select').change(function(){
            $('.search input[type=submit]').trigger('click');
        });
        $('.view').click(function(){
            var id=$(this).data().id;
            $.ajax({
                url:"{{url('backend/members/view')}}/"+id,
                success:function(data){
                    $('#modal').find('.modal-title').html('View Member #'+id);
                    $('#modal').find('.modal-body').html(data);
                    $('#modal').modal();
                }
            })
        })
    </script>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item active">Members</li>
@stop

@push('header')
    <link rel="stylesheet" href="{{url('admin/app-assets/vendors/css/ui/jquery-ui.min.css')}}"">
@endpush
