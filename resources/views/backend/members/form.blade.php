@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update Member #'.$model->id)
@else
	@section('title' , 'Create Member')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post' enctype="multipart/form-data" >
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6">
                        <label for="MemberName">Name</label>
                        <input type="text" name='Member[name]' class="form-control" id="MemberName" placeholder="Enter Name" value='{{$model->name}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="MemberOverview">Position</label>
                        <textarea name='Member[position]' class="form-control" id="MemberPosition" placeholder="Enter position">{!!$model->position!!}</textarea>
                    </div>
					
                    <div class="form-group col-md-6">
                        <label for="MemberOverview">Overview</label>
                        <textarea name='Member[overview]' class="form-control" id="MemberOverview" placeholder="Enter Overview">{!!$model->overview!!}</textarea>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="MemberImage">Image</label>
                        <span>(490 * 487)</span>
                        <input type="file" name='Member[image]' class="form-control" id="MemberImage" placeholder="Enter Image" value='{{$model->image}}'>
                        @if($model->image !='' )
                            <img src={{url('uploads/'.$model->image)}} style='width:100px;margin-top: 10px;' >
                        @endif
                    </div>


                    <div class="form-group col-md-6">
                        <label for="MemberTeam">Team</label>
                        <select name='Member[team]' class="form-control">
                            <option {{selected('board-of-directors' , $model->team )}} value="board-of-directors">Board Of Directors</option>
                            <option {{selected('executive-mangement' , $model->team )}} value="executive-mangement">Executive Mangement</option>
                            <option {{selected('mangement-team' , $model->team )}} value="mangement-team">Mangement Team</option>
                        </select>
                    </div>


                    <div class='clearfix' ></div>
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}
    @endpush
@endif


@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/members')}}">Members</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop
