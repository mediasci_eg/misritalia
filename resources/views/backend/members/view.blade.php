<table class='table table-striped'>
	<tr>
	  <th>Name</th>
	   <td>{{$model->name}}</td>
	</tr>
	<tr>
	  <th>Position</th>
	   <td>{{$model->position}}</td>
	</tr>
	<tr>
	  <th>Overview</th>
	   <td>{{$model->overview}}</td>
	</tr>
	<tr>
	  <th>Image</th>
	  <td><img src={{url('uploads/'.$model->image)}} style='width:100px' ></td>
	</tr>
	<tr>
	  <th>Team</th>
	   <td>{{$model->team}}</td>
	</tr>
	<tr>
	  <th>Sort</th>
	   <td>{{$model->sort}}</td>
	</tr>
</table>
  
  
