<div class="form-group">
    <label for="file-upload" class="btn btn-primary">
        <i class="fa fa-upload" aria-hidden="true"></i> choose Image
    </label>
    <input id="file-upload" class="file-uploader d-none" type="file" accept="image/*"/>
    @if (isset($config['display']))
        <img src="{{$config['display']}}" class="img-preview">
    @else    
        <img src="" class="img-preview" style="display:none">
    @endif
      
    <img src="{{url('vendor/elsayed_nofal/media-manager/loader.gif')}}" class="img-loading" style="width: 60px;display:none" />
    <input type="hidden" name="{{$config['name']}}" class="image-val" value="" @if(isset($config['required'])) required @endif>
</div>

