@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update Csr partner #'.$model->id)	
@else
	@section('title' , 'Create Csr partner')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post' enctype="multipart/form-data" >
                <div class="row">
                    {{ csrf_field() }}		
                    <div class="form-group col-md-6">
                        <label for="CsrPartnerName">Name</label>
                        <input type="text" name='CsrPartner[name]' class="form-control" id="CsrPartnerName" placeholder="Enter Name" value='{{$model->name}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="CsrPartnerLogo">Logo</label>
                        <input type="file" name='CsrPartner[logo]' class="form-control" id="CsrPartnerLogo" placeholder="Enter Logo" value='{{$model->logo}}'>
                        @if($model->logo !='' )
                            <img src={{url('uploads/'.$model->logo)}} style='width:100px;margin-top: 10px;' >
                        @endif
                    </div>


                    <div class="form-group col-md-6">
                        <label for="CsrPartnerLink">Link</label>
                        <input type="text" name='CsrPartner[link]' class="form-control" id="CsrPartnerLink" placeholder="Enter Link" value='{{$model->link}}'>
                    </div>


                    <div class='clearfix' ></div>                    
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}	
    @endpush
@endif


@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/csr_partners')}}">Csr partners</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop