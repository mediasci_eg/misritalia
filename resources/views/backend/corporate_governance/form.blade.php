<?php
use App\Libs\Upload;
?>
@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update Corporate governance #'.$model->id)
@else
	@section('title' , 'Create Corporate governance')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'  >
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6">
                        <label for="CorporateGovernanceCrisis_management_content">Crisis management content</label>
                        <textarea name='CorporateGovernance[crisis_management_content]' class="form-control" id="CorporateGovernanceCrisis_management_content" placeholder="Enter Crisis management content">{!!$model->crisis_management_content!!}</textarea>
                    </div>

                    @include('backend.corporate_governance._files')


                    <div class='clearfix' ></div>
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}
    @endpush
@endif


@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/corporate_governance')}}">Corporate governances</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop
