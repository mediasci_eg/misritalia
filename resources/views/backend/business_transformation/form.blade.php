<?php
use App\Libs\Upload;
?>
@extends('backend.layout.master')
@if($model->exists)
@section('title' , 'Update Business transformation #'.$model->id)
@else
@section('title' , 'Create Business transformation')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'>
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6">
                        <label for="BusinessTransformationDescription">Description</label>
                        <textarea name='BusinessTransformation[description]' class="form-control"
                            id="BusinessTransformationDescription"
                            placeholder="Enter Description">{!!$model->description!!}</textarea>
                    </div>


                    <div class='clearfix'></div>
                    <section class='businessTransformationTabs w-100'>
                        <div class="col-md-12">
                            <h3>Business transformation tabs</h3>
                        </div>
                        <?php
                            $count['businessTransformationTabs'] = 0 ;
                            if(!$model->exists)
                                $businessTransformationTabs = [new \App\Models\BusinessTransformationTab];
                            else
                                $businessTransformationTabs = $model->tabs;
                        ?>
                        @foreach($businessTransformationTabs as $businessTransformationTab)
                        <?php $count['businessTransformationTabs']++ ?>
                        <div class='section_content row pl-1'>
                            <input type="hidden" name='BusinessTransformationTab[{{$count['businessTransformationTabs']}}][id]'
                                value='{{$businessTransformationTab->id}}'>
                            <div class='col-md-12 row'>
                                <div class="form-group col-md-6">
                                    <label for="BusinessTransformationTabTitle">Title</label>
                                    <input type="text"
                                        name='BusinessTransformationTab[{{$count['businessTransformationTabs']}}][title]'
                                        class="form-control" id="BusinessTransformationTabTitle"
                                        placeholder="Enter Title" value='{{$businessTransformationTab->title}}'>
                                </div>
                                @if($businessTransformationTab->has_files==1)
                                    <div class="form-group col-md-6">
                                        <label for="BusinessTransformationTabTitle">PDF Files</label>
                                        <?=Upload::pdfs([
                                            'name'=>'BusinessTransformationTab['.$count['businessTransformationTabs'].'][files][]',
                                            //'folder' => 'brochures/pdf' ,
                                            'value'=>$businessTransformationTab->files,
                                        ])?>
                                    </div>
                                @endif
                                <div class="form-group col-md-12">
                                    <label for="BusinessTransformationTabContent">Content</label>
                                    <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css() ?>
                                    <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('content'.$loop->index, "BusinessTransformationTab[".$count['businessTransformationTabs']."][content]", $businessTransformationTab->content ) ?>
                                    <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js() ?>
                                </div>

                            </div>
                        </div>
                        <div class='clearfix'></div>
                        @endforeach
                    </section>

                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary'>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
@push('footer')
{!!$model->viewErrors()!!}
@endpush
@endif


@stop


@push('footer')
<script>
    $(document).ready(function(){
        count = <?=json_encode($count)?> ;
        $('.add').click(function(){
            var content = $(this).parents('.section_content')[0].outerHTML+'\n\
                                <div class="clearfix" ></div>';
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $section.append(content);
            var $lastItem = $section.find('.section_content').last();
            $lastItem.find('input , select , textarea').each(function(){
                $(this).val('');
                var name = $(this).attr('name');
                if(typeof name!='undefined'){
                    var newName = name.replace(/\d+/ ,count[$section.attr('class')] );
                    $(this).attr('name' , newName);
                }
            })

            var $lastItemBtn = $lastItem.find('.add');
            $lastItemBtn.val('X');
            $lastItemBtn.removeClass('add');
            $lastItemBtn.removeClass('btn-success');
            $lastItemBtn.addClass('btn-danger');
            $lastItemBtn.addClass('remove');
        })

        $(document).on('click','.remove' , function(){
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $(this).closest('.section_content').remove();
        })
    })

</script>
@endpush

@section('breadcrumbs')
<li class="breadcrumb-item"><a href="{{url('backend/business_transformation')}}">Business transformations</a></li>
@if($model->exists)
<li class="breadcrumb-item active">Update</li>
@else
<li class="breadcrumb-item active">Create</li>
@endif
@stop
