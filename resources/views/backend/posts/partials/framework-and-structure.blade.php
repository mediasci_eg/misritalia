<div class="col-12">
    <h2 class='pl-1 border-left-red border-left-3 mt-1'>Welcome Info</h2>
</div>
<input type="hidden" name="title" value="{{$post->title}}">



<div class="form-group col-md-6">
    <label>BOARD OF DIRECTORS</label>
    <textarea name='Post[data][board_of_directors]' style='height:200px'
        class="form-control">{{@$post->data['board_of_directors']}}</textarea>
</div>
<div class="form-group col-md-6">
    <label>EXECUTIVE MANAGEMENT</label>
    <textarea name='Post[data][executive_management]' style='height:200px'
        class="form-control">{{@$post->data['executive_management']}}</textarea>
</div>
<div class="form-group col-md-6">
    <label>MANAGEMENT TEAMup</label>
    <textarea name='Post[data][managment_team]' style='height:200px'
        class="form-control">{{@$post->data['managment_team']}}</textarea>
</div>
<div class="form-group col-md-6">
    <label>FRAMEWORK DIAGRAM</label>
    <textarea name='Post[data][framework_diagram]' style='height:200px'
        class="form-control">{{@$post->data['framework_diagram']}}</textarea>
</div>
<div class="form-group col-md-6">
    <label>CODE OF BUSINESS CONDUCT</label>
    <textarea name='Post[data][code_of_business_conduct]' style='height:200px'
        class="form-control">{{@$post->data['code_of_business_conduct']}}</textarea>
</div>
<div class="form-group col-md-6">
    <label>POLICY LANDSCAPE MANAGEMENT PROCESS</label>
    <textarea name='Post[data][policy_landscape]' style='height:200px'
        class="form-control">{{@$post->data['policy_landscape']}}</textarea>
</div>
{{-- <div class="col-6">
    <div class="form-group">
        <label class="control-label"> Image </label>
        <input type="file" class='form-control' value="{{$post->image}}" name="image">
@if($post->image !='')
<img src='{{url('uploads/'.$post->image)}}' style='width:200px'>
@endif
</div>
</div> --}}
