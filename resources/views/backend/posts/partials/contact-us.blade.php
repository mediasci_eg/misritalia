<div class="col-12">
    <h2 class='pl-1 border-left-red border-left-3 mt-1'>Main Info</h2>
</div>
<input type="hidden" name="title" value="{{$model->title}}">

<div class="form-group col-md-6">
    <label>Mobile</label>
    <input type='text' name='Post[data][mobile]' class="form-control" placeholder="Enter Mobile"
        value="{{@$model->data['mobile']}}">
</div>

<div class="form-group col-md-6">
    <label>Mail</label>
    <input type='text' name='Post[data][mail]' class="form-control" placeholder="Enter Mail"
        value="{{@$model->data['mail']}}">
</div>

<div class="form-group col-md-6">
    <label>Address</label>
    <input type='text' name='Post[data][address]' class="form-control" placeholder="Enter Address"
        value="{{@$model->data['address']}}">
</div>
<div class="form-group col-md-6">
    <label>Location</label>
    <input type='text' name='Post[data][location]' class="form-control" placeholder="Enter location on map"
        value="{{@$model->data['location']}}">
</div>
<div class="form-group col-md-6">
    <label>Facebook</label>
    <input type='text' name='Post[data][facebook]' class="form-control" placeholder="Enter Facebook Link"
        value="{{@$model->data['facebook']}}">
</div>
<div class="form-group col-md-6">
    <label>Twitter</label>
    <input type='text' name='Post[data][twitter]' class="form-control" placeholder="Enter Twitter Link"
        value="{{@$model->data['twitter']}}">
</div>
<div class="form-group col-md-6">
    <label>Instagram</label>
    <input type='text' name='Post[data][instagram]' class="form-control" placeholder="Enter Instagram Link"
        value="{{@$model->data['instagram']}}">
</div>
<div class="form-group col-md-6">
    <label>Youtube</label>
    <input type='text' name='Post[data][youtube]' class="form-control" placeholder="Enter Youtube Channel Link"
        value="{{@$model->data['youtube']}}">
</div>
<div class="form-group col-md-6">
    <label>Content</label>
    <textarea name='content' style='height:200px' class="form-control"
        placeholder="Enter Content">{{@$model->content}}</textarea></div>
</div>

<div class="form-group col-md-6 ">
    <label>Main Image</label>
    {!!\HossamAldeen\Upload\Upload::image([
    'name'=>'image',
    'value'=>old("image" , $model->image )
    ])!!}
    {!! $errors->first("image", '<p class="danger text-muted">:message</p>') !!}
</div>
