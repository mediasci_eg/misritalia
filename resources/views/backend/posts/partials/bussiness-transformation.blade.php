<?php
// use App\Http\Controllers\Helpers\Upload ;
use App\Libs\Upload;

?>
<div class="col-12">
    <h2 class='pl-1 border-left-red border-left-3 mt-1'>Welcome Info</h2>
</div>
<input type="hidden" name="title" value="{{$post->title}}">



<div class="form-group col-md-6">
    <label>Content</label>
    <textarea name='content' style='height:200px' class="form-control">{{@$post->content}}</textarea>
</div>
<div class="form-group col-md-6">
    <label>THE JOURNEY</label>
    <textarea name='Post[data][the_journey]' style='height:200px'
        class="form-control">{{@$post->data['the_journey']}}</textarea>
</div>

<div class="form-group col-md-6">
    <label for="pdfName">journey pdf volume one</label>
    <?=Upload::pdf([
                    'name'=>'Post[data][journey_pdf_volume_one]',
                    'value'=>@$post->data['journey_pdf_volume_one'],
                ])?>
</div>

<div class="form-group col-md-6">
    <label for="pdfName">journey pdf volume two</label>
    <?=Upload::pdf([
                    'name'=>'Post[data][journey_pdf_volume_two]',
                    'value'=>@$post->data['journey_pdf_volume_two'],
                ])?>
</div>


<div class="form-group col-md-6">
    <label for="pdfName">journey pdf volume three</label>
    <?=Upload::pdf([
                    'name'=>'Post[data][journey_pdf_volume_three]',
                    'value'=>@$post->data['journey_pdf_volume_three'],
                ])?>
</div>


<div class="form-group col-md-6">
    <label for="pdfName">journey pdf volume four</label>
    <?=Upload::pdf([
                    'name'=>'Post[data][journey_pdf_volume_four]',
                    'value'=>@$post->data['journey_pdf_volume_four'],
                ])?>
</div>

<div class="form-group col-md-6">
    <label>BUSINESS EXCELLENCE</label>
    <textarea name='Post[data][business_excellence]' style='height:200px'
        class="form-control">{{@$post->data['business_excellence']}}</textarea>
</div>


<div class="event-detials w-100 ">
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <label class="control-label">PROCESS PATHWAY</label>
                <?= Upload::image([
                    'name'=>'Post[data][process_pathway]' ,
                    'value'=>@$post->data['process_pathway'] ,
                ]); ?>
            </div>
        </div>

    </div>
</div>
