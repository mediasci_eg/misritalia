<div class="card">
    <div class="card-content">
        <div class="card-body">
          @success
          @errors
            <form action="backend/posts/store" method="post" id="bu-form">
                @csrf
                <input type="hidden" name="id" value="{{$post->id}}">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                          <label for="title">Title</label>
                          <input required placeholder="Post Title" type="text" name="title" value="{{old('title' , $post->title)}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="w-100"></div>
                    
					          
					          <div class="col-6">
                        <div class="form-group">
                          <label for="title">Business</label>
                          <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('business', 'data[business]', $post->data['business'])?>
                        </div>
                    </div>
					          <div class="col-6">
                        <div class="form-group">
                          <label for="title">Humanities</label>
                          <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('humanities', 'data[humanities]', $post->data['humanities'])?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                          <label for="title">pharmaceutical sciences</label>
                          <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('pharmaceutical', 'data[pharmaceutical]', $post->data['pharmaceutical'])?>
                        </div>
                    </div>
                    <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css()?>
                    <?=App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js()?>
                    
                </div>
                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
