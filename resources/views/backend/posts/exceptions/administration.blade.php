<div class="card">
    <div class="card-content">
        <div class="card-body">
          @success
          @errors
            <form action="backend/posts/store" method="post" id="bu-form">
                @csrf
                <input type="hidden" name="id" value="{{$post->id}}">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                          <label for="title">Title</label>
                          <input required placeholder="Post Title" type="text" name="title" value="{{$post->title}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="w-100"></div>
                    
					          <div class="col-6">
                        <div class="form-group">
                          <label for="title">About the degree</label>
                          <textarea name="data[about_degree]" class="form-control" cols="30" rows="10">{{@$post->data['about_degree']}}</textarea>
                        </div>
                    </div>
					          <div class="col-6">
                        <div class="form-group">
                          <label for="title">Why this degree</label>
                          <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('why_degree', 'data[why_degree]', $post->data['why_degree'])?>
                        </div>
                    </div>
					          <div class="col-6">
                        <div class="form-group">
                          <label for="title">What job can i get</label>
                          <textarea name="data[job]" class="form-control" cols="30" rows="10">{{@$post->data['job']}}</textarea>
                        </div>
                    </div>
            
                    <div class="col-12">
                      <div class="form-group">
                          <label class="control-label">Content</label>
                         <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css()?>
                         <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('content', 'content', $post->content)?>
                         <?=App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js()?>
                      </div>
                  </div>
                    
                </div>
                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save    "></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
