<?php

use App\Http\Controllers\Helpers\Upload;
?>
<div class="card">
    <div class="card-content">
        <div class="card-body">
            @success
            @errors
            <form action="backend/posts/store" method="post" id="bu-form">
                @csrf
                <input type="hidden" name="id" value="{{$post->id}}">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input required placeholder="Post Title" type="text" name="title" value="{{$post->title}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="w-100"></div>
					<div class="col-6">
                        <div class="form-group">
                            <label for="title">Sub Title</label>
                            <input required placeholder="Post Title" type="text" name="data[sub_title]" value="{{$post->data['sub_title']}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">Second Title</label>
                            <input required placeholder="Second Title" type="text" name="data[second_title]" value="{{$post->data['second_title']}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="w-100"></div>

                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label">Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css() ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('content', 'content', $post->content) ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js() ?>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">Why Herts?</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('why_herts', "data[why_herts]", $post->data['why_herts']) ?>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Videos</label>
                        </div>
                        <div class="form-group">
                            <input type="text" class='form-control' name="data[video1]" value='{{old('video1' , $post->data['video1'])}}' >
                        </div> <div class="form-group">
                            <input type="text" class='form-control' name="data[video2]" value='{{old('video2' , $post->data['video2'])}}' >
                        </div> <div class="form-group">
                            <input type="text" class='form-control' name="data[video3]" value='{{old('video3' , $post->data['video3'])}}' >
                        </div>
                    </div>
                    <div class="event-detials w-100">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="control-label">Partner Images</label>
                                    <?=
                                    Upload::images([
                                        'name' => 'data[images]',
                                        'value' => @$post->data['images'],
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save    "></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
