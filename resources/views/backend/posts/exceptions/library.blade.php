<div class="card">
    <div class="card-content">
        <div class="card-body">
          @success
          @errors
            <form action="backend/posts/store" method="post" id="bu-form">
                @csrf
                <input type="hidden" name="id" value="{{$post->id}}">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                          <label for="title">Title</label>
                          <input required placeholder="Post Title" type="text" name="title" value="{{old('title' , $post->title)}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="w-100"></div>
                    
					          <div class="col-6">
                        <div class="form-group">
                          <label for="title">Global Academic Foundation Library</label>
                          <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('library', 'data[library]', $post->data['library'])?>
                        </div>
                    </div>
					          <div class="col-6">
                        <div class="form-group">
                          <label for="title">The Library Policy</label>
                          <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('policy', 'data[policy]', $post->data['policy'])?>
                        </div>
                    </div>
					          <div class="col-6">
                        <div class="form-group">
                          <label for="title">Schedule of the Library</label>
                          <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('schedule', 'data[schedule]', $post->data['schedule'])?>
                        </div>
                    </div>
            
                    <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css()?>
                    <?=App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js()?>
                    
                </div>
                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
