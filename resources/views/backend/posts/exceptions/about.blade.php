<div class="col-12">
    <h2 class='pl-1 border-left-red border-left-3 mt-1'>Welcome Info</h2>
</div>
<input type="hidden" name="title" value="{{$model->title}}">
<div class="form-group col-md-6 ">
    <label>Main Image</label>
    {!!\HossamAldeen\Upload\Upload::image([
    'name'=>'image',
    'value'=>old("image" , @$model->image )
    ])!!}
    {!! $errors->first("image", '<p class="danger text-muted">:message</p>') !!}
</div>
<div class="form-group col-md-6">
    <label>Welcome Title</label>
    <input type='text' name='Post[data][welcome_title]' class="form-control" value="{{@$model->data['welcome_title']}}">
</div>
<div class="form-group col-md-6">
    <label>Welcome Quote</label>
    <input type='text' name='Post[data][welcome_quote]' class="form-control" value="{{@$model->data['welcome_quote']}}">
</div>
<div class="form-group col-md-6 ">
    <label>Welcome Section Image</label>
    {!!\HossamAldeen\Upload\Upload::image([
    'name'=>'Post[data][welcome_image]',
    'value'=>old("Post[data][welcome_image]" , @$model->data['welcome_image'] )
    ])!!}
    {!! $errors->first("Post[data][welcome_image]", '<p class="danger text-muted">:message</p>') !!}
</div>
<div class="form-group col-md-6">
    <label>Welcome Content</label>
    <textarea name='Post[data][welcome_content]' style='height:200px'
        class="form-control">{{@$model->data['welcome_content']}}</textarea></div>
</div>

{{-- why section start --}}
<div class="row">
    <div class="col-12">
        <h2 class='pl-1 border-left-red border-left-3 mt-1'>Why Choose us Section</h2>
    </div>
    <div class="form-group col-md-6">
        <label>Why Section Title</label>
        <input type='text' name='Post[data][why_title]' class="form-control" value="{{@$model->data['why_title']}}">
    </div>

    <div class="form-group col-md-6 ">
        <label>Why Section Main Image</label>
        {!!\HossamAldeen\Upload\Upload::image([
        'name'=>'Post[data][why_main_image]',
        'value'=>old("Post[data][why_main_image]" , @$model->data['why_main_image'] )
        ])!!}
        {!! $errors->first("Post[data][why_main_image]", '<p class="danger text-muted">:message</p>') !!}
    </div>
    <div class="col-12">
        <h2 class='pl-1 border-left-red border-left-3 mt-1'>Questions</h2>
    </div>
    <div class="form-group col-md-6">
        <label>Questions one Title</label>
        <input type='text' name='Post[data][q1_title]' class="form-control" value="{{@$model->data['q1_title']}}">
    </div>

    <div class="form-group col-md-6 ">
        <label>Questions one icon</label>
        {!!\HossamAldeen\Upload\Upload::image([
        'name'=>'Post[data][q1_image]',
        'value'=>old("Post[data][q1_image]" , @$model->data['q1_image'] )
        ])!!}
        {!! $errors->first("Post[data][q1_image]", '<p class="danger text-muted">:message</p>') !!}
    </div>

    <div class="form-group col-md-6">
        <label>Questions one brief</label>
        <textarea name='Post[data][q1_brief]' style='height:200px'
            class="form-control">{{@$model->data['q1_brief']}}</textarea>
    </div>
    {{-- two-section --}}
    <div class="form-group col-md-6">
        <label>Questions two Title</label>
        <input type='text' name='Post[data][q2_title]' class="form-control" value="{{@$model->data['q2_title']}}">
    </div>

    <div class="form-group col-md-6 ">
        <label>Questions two icon</label>
        {!!\HossamAldeen\Upload\Upload::image([
        'name'=>'Post[data][q2_image]',
        'value'=>old("Post[data][q2_image]" , @$model->data['q2_image'] )
        ])!!}
        {!! $errors->first("Post[data][q2_image]", '<p class="danger text-muted">:message</p>') !!}
    </div>

    <div class="form-group col-md-6">
        <label>Questions two brief</label>
        <textarea name='Post[data][q2_brief]' style='height:200px'
            class="form-control">{{@$model->data['q2_brief']}}</textarea>
    </div>
    {{-- three-section --}}
    <div class="form-group col-md-6">
        <label>Questions three Title</label>
        <input type='text' name='Post[data][q3_title]' class="form-control" value="{{@$model->data['q3_title']}}">
    </div>

    <div class="form-group col-md-6 ">
        <label>Questions three icon</label>
        {!!\HossamAldeen\Upload\Upload::image([
        'name'=>'Post[data][q3_image]',
        'value'=>old("Post[data][q3_image]" , @$model->data['q3_image'] )
        ])!!}
        {!! $errors->first("Post[data][q3_image]", '<p class="danger text-muted">:message</p>') !!}
    </div>

    <div class="form-group col-md-6">
        <label>Questions three brief</label>
        <textarea name='Post[data][q3_brief]' style='height:200px'
            class="form-control">{{@$model->data['q3_brief']}}</textarea>
    </div>
    {{-- four-section --}}
    <div class="form-group col-md-6">
        <label>Questions four Title</label>
        <input type='text' name='Post[data][q4_title]' class="form-control" value="{{@$model->data['q4_title']}}">
    </div>

    <div class="form-group col-md-6 ">
        <label>Questions four icon</label>
        {!!\HossamAldeen\Upload\Upload::image([
        'name'=>'Post[data][q4_image]',
        'value'=>old("Post[data][q4_image]" , @$model->data['q4_image'] )
        ])!!}
        {!! $errors->first("Post[data][q4_image]", '<p class="danger text-muted">:message</p>') !!}
    </div>

    <div class="form-group col-md-6">
        <label>Questions four brief</label>
        <textarea name='Post[data][q4_brief]' style='height:200px'
            class="form-control">{{@$model->data['q4_brief']}}</textarea>
    </div>

    {{-- five-section --}}
    <div class="form-group col-md-6">
        <label>Questions five Title</label>
        <input type='text' name='Post[data][q5_title]' class="form-control" value="{{@$model->data['q5_title']}}">
    </div>

    <div class="form-group col-md-6 ">
        <label>Questions five icon</label>
        {!!\HossamAldeen\Upload\Upload::image([
        'name'=>'Post[data][q5_image]',
        'value'=>old("Post[data][q5_image]" , @$model->data['q5_image'] )
        ])!!}
        {!! $errors->first("Post[data][q5_image]", '<p class="danger text-muted">:message</p>') !!}
    </div>

    <div class="form-group col-md-6">
        <label>Questions five brief</label>
        <textarea name='Post[data][q5_brief]' style='height:200px'
            class="form-control">{{@$model->data['q5_brief']}}</textarea>
    </div>

    {{-- six-section --}}
    <div class="form-group col-md-6">
        <label>Questions six Title</label>
        <input type='text' name='Post[data][q6_title]' class="form-control" value="{{@$model->data['q6_title']}}">
    </div>

    <div class="form-group col-md-6 ">
        <label>Questions six icon</label>
        {!!\HossamAldeen\Upload\Upload::image([
        'name'=>'Post[data][q6_image]',
        'value'=>old("Post[data][q6_image]" , @$model->data['q6_image'] )
        ])!!}
        {!! $errors->first("Post[data][q6_image]", '<p class="danger text-muted">:message</p>') !!}
    </div>

    <div class="form-group col-md-6">
        <label>Questions six brief</label>
        <textarea name='Post[data][q6_brief]' style='height:200px'
            class="form-control">{{@$model->data['q6_brief']}}</textarea>
    </div>

</div>
