<?php

use App\Http\Controllers\Helpers\Upload;
?>
<div class="card">
    <div class="card-content">
        <div class="card-body">
            @success
            @errors
            <form action="backend/posts/store" method="post" id="bu-form">
                @csrf
                <input type="hidden" name="id" value="{{$post->id}}">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input required placeholder="Title" type="text" name="title" value="{{$post->title}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">Main Title</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('main_title', "data[main_title]", $post->data['main_title']) ?>
                        </div>
                    </div>
                    <div class="w-100"></div>

                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Who We Are Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css() ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('who_we_are', 'data[who_we_are]', $post->data['who_we_are']) ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js() ?>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">Section Title</label>
                            <input required placeholder="Section Title" type="text" name="data[section_title]" value="{{$post->data['section_title']}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">Section Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('section_content', "data[section_content]", $post->data['section_content']) ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">School Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('school_content', "data[school_content]", $post->data['school_content']) ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">School of Humanities Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('school_of_humanitites_content', "data[school_of_humanitites_content]", $post->data['school_of_humanitites_content']) ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">School of Pharmaceutical Sciences Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('school_of_pharmaceutical_sciences_content', "data[school_of_pharmaceutical_sciences_content]", $post->data['school_of_pharmaceutical_sciences_content']) ?>
                        </div>
                    </div>

                </div>
                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save    "></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
