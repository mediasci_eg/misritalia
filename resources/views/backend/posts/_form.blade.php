<div class="card">
    <div class="card-content">
        <div class="card-body">
            @success
            @errors
            <form action="backend/posts/store" method="post" id="bu-form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$post->id}}">
                <div class="row">
                    {{ csrf_field() }}

                    @include('backend.posts.exceptions.about')


                    <div class='clearfix'></div>
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary'>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@push('script')
{{-- <script>
      $(document).ready(function(){

        $('#bu-form').validate({
          rules:{
            'name':{required:true},
            'email':{
              'required':true,
              'email':true
            },
             @if ($user->email == '')
            "password": {required: true,minlength:8},
            "password_confirmation": {required: true, equalTo:'#password'}
            @endif
          }
        })
      });
    </script> --}}
@endpush
