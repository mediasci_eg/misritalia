@extends(config('backend-users.backend_layout'))

@section(config('backend-users.layout_content_area'))
    <div class="content-header row">
          
        <div class="content-header-left col-md-6 col-12 mb-1">
        <h3 class="content-header-title">Developments Inquiries</h3>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./backend">Home</a>
                </li>
                <li class="breadcrumb-item active">Developments Inquiries
                </li>
            </ol>
        </div>
        </div>
    </div>

    <div class="content-body">
            @success
            @errors
        <section class='' id="search">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Find Inquiry <i class="fa fa-search" aria-hidden="true"></i></h3>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <form>
                                    <div class="row">
                                      
                                        <div class="col-2">
                                            <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" name="developments_inquiries[email]" value="{{request()->input('developments_inquiries.email')}}" id="email" class="form-control" placeholder="Email" aria-describedby="helpId">
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" name="developments_inquiries[name]" value="{{request()->input('developments_inquiries.name')}}" id="name" class="form-control" placeholder="Name" aria-describedby="helpId">
                                                </div>

                                        </div>

                                      

                                        <div class="col-1">
                                            <div class="form-group">
                                                <br>
                                                <input type="hidden" name="search" value="1">
                                                <button type="submit" title="search" class="btn btn-outline-primary btn-xs"><i class="fa fa-search" aria-hidden="true"></i></button>
                                            </div>
                                        </div>

                                      

                                    </div>
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </section>
        <section id="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Inquiries</h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
             
                                   <br>
                             <a href="backend/developments-inquiries/trash"  class="btn btn-outline-danger">Trached</a>
                    
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                {{-- <a href="backend/posts/create" class="btn btn-xs btn-outline-primary" title="new User"><i class="fa fa-plus" aria-hidden="true"></i> New</a> --}}
                                <br><br>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                
                                                <th>Project</th>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Message</th>
                                                <th>Migrated</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($developments_inquiries as $inquiry)
                                                <tr 
                                                 @if ($inquiry->is_read==0)
                                                 class="p-3 mb-2 bg-secondary text-white"
                                                  
                                                @endif 
                                                >
                                                    <td>{{ $inquiry->development->title }}</td>
                                                    <td>{{$inquiry->first_name.' '.$inquiry->last_name}}</td>
                                                    <td>{{$inquiry->phone}}</td>
                                                    <td>{{$inquiry->email}}</td>
                                                    <td>{{ str_limit($inquiry->message, 10) }}</td>
                                                    <td>{{ $inquiry->sent }}</td>
                                                    
                                                    <td>
                                                        {{-- <a href="backend/posts/update/{{$post->id}}" class="btn btn-sm btn-outline-info" title="Edit"><i class="fa fa-pen" aria-hidden="true"></i></a> --}}
                                                        <a href="backend/developments-inquiries/delete/{{ $inquiry->id }}" class="btn btn-sm btn-outline-danger delete" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                        <button data-metakey="{{  $inquiry->id }}" type="button" class="btn btn-sm btn-outline-info metakeybtn " data-toggle="modal" data-target="#exampleModal-{{ $inquiry->id }}">
                                                            View Message
                                                          </button>
                                            </td>
                                            <div class="modal fade" id="exampleModal-{{ $inquiry->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                      <div class="modal-content">
                                                        <div class="modal-header">
                                                          <h5 class="modal-title" id="exampleModalLabel"> Message</h5>
                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                          </button>
                                                        </div>
                                                        <div class="modal-body">
                                                         Message :
                                                         <br><br>
                                                         {{$inquiry->message}}
                                                         <hr>
                                                       
                                                      </div>
                                                    </div>
                                                  </div>
                                            </div>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{$developments_inquiries->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('script')
@deletejs


<script>
    $('.metakeybtn').on('click', function (e) {

    var id=$(this).data("metakey");
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    // console.log(id);

    $(this).parents('.text-white').attr("class",'');

    $.ajax({
                    /* the route pointing to the post function */
                    url: 'backend/developments-inquiries/postajax',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, 'key_id':id},
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) { 
                        $(".writeinfo").append(data.msg); 
                    }
                }); 
            
  });
    </script>

@endpush