@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update Framework structure #'.$model->id)	
@else
	@section('title' , 'Create Framework structure')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'  >
                <div class="row">
                    {{ csrf_field() }}		
                    <div class="form-group col-md-6">
                        <label for="FrameworkStructureTitle">Title</label>
                        <input type="text" name='FrameworkStructure[title]' class="form-control" id="FrameworkStructureTitle" placeholder="Enter Title" value='{{$model->title}}'>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="FrameworkStructureContent">Content</label>
						<?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css() ?>
						<?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('content', 'FrameworkStructure[content]', $model->content) ?>
						<?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js() ?>
                    </div>


                    <div class="form-group col-md-6">
                        <label for="FrameworkStructureShow_more">Show more</label>
                        <input type="text" name='FrameworkStructure[show_more]' class="form-control" id="FrameworkStructureShow_more" placeholder="Enter Show more" value='{{$model->show_more}}'>
                    </div>

                    <div class='clearfix' ></div>                    
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}	
    @endpush
@endif


@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/framework_structure')}}">Framework structures</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop