<table class='table table-striped'>
	<tr>
	  <th>Title</th>
	   <td>{{$model->title}}</td>
	</tr>
	<tr>
	  <th>Description</th>
	   <td>{{$model->description}}</td>
	</tr>
	<tr>
	  <th>Image</th>
	  <td><img src={{url('uploads/'.$model->image)}} style='width:100px' ></td>
	</tr>
	<tr>
	  <th>Sort</th>
	   <td>{{$model->sort}}</td>
	</tr>
</table>
  
  
