@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update Partner #'.$model->id)
@else
	@section('title' , 'Create Partner')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post' enctype="multipart/form-data" >
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6">
                        <label for="PartnerTitle">Title</label>
                        <input type="text" name='Partner[title]' class="form-control" id="PartnerTitle" placeholder="Enter Title" value='{{$model->title}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="PartnerDescription">Description</label>
                        <textarea style='height:200px' name='Partner[description]' class="form-control" id="PartnerDescription" placeholder="Enter Description">{!!$model->description!!}</textarea>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="PartnerImage">Image</label>
                        <span>(size is not fixed)</span>
                        <input type="file" name='Partner[image]' class="form-control" id="PartnerImage" placeholder="Enter Image" value='{{$model->image}}'>
                        @if($model->image !='' )
                            <img src={{url('uploads/'.$model->image)}} style='width:100px;margin-top: 10px;' >
                        @endif
                    </div>





                    <div class='clearfix' ></div>
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}
    @endpush
@endif


@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/partners')}}">Partners</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop
