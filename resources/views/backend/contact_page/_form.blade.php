<div class="row mb-4">
    <input type="hidden" name='ContactPage[{{$i}}][id]' value='{{$model->id}}'>

    <div class="form-group col-md-6">
        <label for="ContactPageTitle">Title</label>
        <input type="text" name='ContactPage[{{$i}}][title]' class="form-control"
            placeholder="Enter Title" value='{{$model->title}}'>
    </div>

    <div class="form-group col-md-6">
        <label for="ContactPageAddress">Address</label>
        <textarea name='ContactPage[{{$i}}][address]' class="form-control"
            placeholder="Enter Address">{!!$model->address!!}</textarea>
    </div>

    <div class="form-group col-md-6">
        <label for="ContactPageMap">Map</label>
        <input type="text" name='ContactPage[{{$i}}][map]' class="form-control"
            placeholder="Enter Map" value='{{$model->map}}'>
    </div>

    <div class="form-group col-md-6">
        <label for="ContactPageEmail">Email</label>
        <input type="text" name='ContactPage[{{$i}}][email]' class="form-control"
        placeholder="Enter Email" value='{{$model->email}}'>
    </div>
</div>


