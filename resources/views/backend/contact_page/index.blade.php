@extends('backend.layout.master')
@section('title' , 'Contact page')
@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'  >
                {{ csrf_field() }}
                @php 
					$i = 0 ; 
				@endphp
				
                <h2>SUPPORT</h2>
                @foreach($support as $model)
                    @include('backend.contact_page._form')
                    @php $i++ @endphp
                @endforeach
				<section style='width:100%' class='SHOWROOMS'>  
					<h2>SHOWROOMS  

					</h2>
					@foreach($showrooms as $model)
					<div class='section_content row pl-1'>
						@include('backend.contact_page._form')
						@php $i++ @endphp
						<div style='margin-top: 25px;' class='col-md-2'>
							@if($i ==2)
							<input type='button' class='add btn btn-success' value="+">
							@else
							<input type='button' class='remove btn btn-danger' value="X">
							@endif
						</div>
					</div>
					@endforeach
					
				</section>
				
                <div class="col-md-12">
                    <input type='submit' value='Save' name='save' class='btn btn-primary' >
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}
    @endpush
@endif


@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/contact_page')}}">Contact pages</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop



@push('footer')
<script>
    $(document).ready(function(){
        count = <?=$i?> ;
        $('.add').click(function(){
            var content = $(this).parents('.section_content')[0].outerHTML+'\n\
                                <div class="clearfix" ></div>';
            var $section = $(this).closest('section');
            count++;
            $section.append(content);
            var $lastItem = $section.find('.section_content').last();
            $lastItem.find('.preview-container').html('');
            $lastItem.find('input , select , textarea').each(function(){
                $(this).val('');
                if(typeof $(this).attr('data-name')!='undefined'){
                    var name = $(this).attr('data-name');
                    var newName = name.replace(/\d+/ ,count );
                    $(this).attr('data-name' , newName);
                }
                var name = $(this).attr('name');
                if(typeof name!='undefined'){
                    var newName = name.replace(/\d+/ ,count );
                    $(this).attr('name' , newName);
                }
            })

            var $lastItemBtn = $lastItem.find('.add');
            $lastItemBtn.val('X');
            $lastItemBtn.removeClass('add');
            $lastItemBtn.removeClass('btn-success');
            $lastItemBtn.addClass('btn-danger');
            $lastItemBtn.addClass('remove');
        })

        $(document).on('click','.remove' , function(){
            var $section = $(this).closest('section');
            count++;
            $(this).closest('.section_content').remove();
        })
    })
</script>
@endpush
