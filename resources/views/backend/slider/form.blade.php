<?php
    use App\Libs\Upload;
?>
@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update Slider #'.$model->id)
@else
	@section('title' , 'Create Slider')
@endif

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
    <h3 class="content-header-title">Slider</h3>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
    <div class="breadcrumb-wrapper col-12">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./backend">Home</a></li>
            <li class="breadcrumb-item active">Slider</li>
        </ol>
    </div>
    </div>
</div>

<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post' enctype="multipart/form-data" >
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6">
                        <label for="SliderImage">Image</label>
                        <?=
                            Upload::image([
                                'name' => 'Slider[image]',
                                'size' => '1350 * 625',
                                'value' => $model->image,
                            ])
                            ?>
                    </div>


                    <div class="form-group col-md-6">
                        <label for="SliderTitle">Title</label>
                        <input type="text" name='Slider[title]' class="form-control" id="SliderTitle" placeholder="Enter Title" value='{{$model->title}}'>
                    </div>

					<div class="form-group col-md-6">
                        <label for="SliderTitle">Sub Title</label>
                        <input type="text" name='Slider[subtitle]' class="form-control" id="SliderTitle" placeholder="Enter Sub Title" value='{{$model->subtitle}}'>
                    </div>

					<div class="form-group col-md-6">
                        <label for="SliderTitle">Details Title</label>
                        <input type="text" name='Slider[details_title]' class="form-control" id="SliderTitle" placeholder="Enter Details Title" value='{{$model->details_title}}'>
                    </div>

					<div class="form-group col-md-6">
                        <label for="SliderTitle">Details Content</label>
                        <textarea name='Slider[details_content]' class="form-control" placeholder="Enter Details Content">{{$model->details_content}}</textarea>
                    </div>


                    <div class='clearfix' ></div>
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/slider')}}">Sliders</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop
