<table class='table table-striped'>
	<tr>
	  <th>Image</th>
	  <td><img src={{url('uploads/'.$model->image)}} style='width:100px' ></td>
	</tr>
	<tr>
	  <th>Title</th>
	   <td>{{$model->title}}</td>
	</tr>
	<tr>
	  <th>Subtitle</th>
	   <td>{{$model->subtitle}}</td>
	</tr>
	<tr>
	  <th>Sort</th>
	   <td>{{$model->sort}}</td>
	</tr>
</table>
  
  
