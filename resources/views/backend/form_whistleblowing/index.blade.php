@extends('backend.layout.master')
@section('title' , 'Form whistleblowing')
@section('content')


<div class="card">
    <div class="card-header">
        <div class="btn-group float-md-right">
        </div>
    </div>
    <div class="card-content overflow-auto collapse show">
        <div class="card-body card-dashboard">
                <form method='get' class='search' >
                    <table class="table table-striped table-bordered zero-configuration">
                        <tr>
                            <th style="min-width: 65px">#</th>
                            <th>Subject</th>
                            <th style="width: 65px">Actions</th>
                        </tr>
                        <tr>
                            <th><input type='text' name='id' value='{{request('id')}}' class='form-control'></th>
                        <th><input type='text' name='subject' value='{{request('subject')}}' class='form-control'></th>
                            <th style="width: 65px">

                                <input type='submit' class='d-none' name='search'>
                            </th>

                        </tr>
                        @if($data->isNotEmpty())
                            @foreach($data as $row)
                                <tr>
                                    <td>{{$row->id}}</td>


                                    <td>{{$row->subject}}</td>
                                    <td>
                                            <a href="{{url('backend/form-whistleblowing/view/'.$row->id.'')}}" title='view'>
                                                <i class="fa fa-lg fa-eye"></i>
                                            </a>
                                            <a href="{{url('backend/form-whistleblowing/delete/'.$row->id.'')}}" title='Delete' onclick="return confirm('Are you sure you want to delete ?')" >
                                                <i class="fa fa-lg fa-remove"></i>
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class='text-center' colspan='100'>No Data Available</td>
                            </tr>
                        @endif
                    </table>
                </form>

            <!-- /.box-body -->
            <div class="box-footer clearfix">
                {{ $data->appends(request()->except('page'))->links() }}
            </div>
        </div>
    </div>
</div>
@stop

@push('footer')
    <script>
        $('.search input , .search select').change(function(){
            $('.search input[type=submit]').trigger('click');
        });
        $('.view').click(function(){
            var id=$(this).data().id;
            $.ajax({
                url:"{{url('backend/form_whistleblowing/view')}}/"+id,
                success:function(data){
                    $('#modal').find('.modal-title').html('View Form_whistleblowing #'+id);
                    $('#modal').find('.modal-body').html(data);
                    $('#modal').modal();
                }
            })
        })
    </script>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item active">Form whistleblowings</li>
@stop

