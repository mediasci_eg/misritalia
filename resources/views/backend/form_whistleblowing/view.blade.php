@extends('backend.layout.master')
@section('title' , 'Form whistleblowing')
@section('content')


<div class="card">
    <div class="card-header">
        <div class="btn-group float-md-right">
        </div>
    </div>
    <div class="card-content overflow-auto collapse show">
        <div class="card-body card-dashboard">
            <table class='table table-striped'>
                <tr>
                    <th>Subject</th>
                    <td>{{$model->subject}}</td>
                </tr>
                @if(is_array($model->data))
                    @foreach($model->data as $key=>$value)
                        <tr>
                            <th>{{$key}}</th>
                            <td>{{$value}}</td>
                        </tr>
                    @endforeach
                @endif

                @if($model->files != '')
                <tr>
                    <th>File</th>
                    <td><a download href='./uploads/pdf/{{$model->files}}'> Download </a></td>
                </tr>
                @endif
            </table>

        </div>
    </div>
</div>
@stop


@section('breadcrumbs')
    <li class="breadcrumb-item active">Form whistleblowings</li>
@stop
