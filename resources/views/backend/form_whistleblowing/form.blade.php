@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update Form whistleblowing #'.$model->id)	
@else
	@section('title' , 'Create Form whistleblowing')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'  >
                <div class="row">
                    {{ csrf_field() }}		
                    <div class="form-group col-md-6">
                        <label for="FormWhistleblowingData">Data</label>
                        <textarea name='FormWhistleblowing[data]' class="form-control" id="FormWhistleblowingData" placeholder="Enter Data">{!!$model->data!!}</textarea>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="FormWhistleblowingFiles">Files</label>
                        <textarea name='FormWhistleblowing[files]' class="form-control" id="FormWhistleblowingFiles" placeholder="Enter Files">{!!$model->files!!}</textarea>
                    </div>


                    <div class='clearfix' ></div>                    
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}	
    @endpush
@endif


@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/form_whistleblowing')}}">Form whistleblowings</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop