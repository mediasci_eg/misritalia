@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update About u #'.$model->id)	
@else
	@section('title' , 'Create About u')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'  >
                <div class="row">
                    {{ csrf_field() }}		
                    <div class="form-group col-md-6">
                        <label for="AboutUs[Founder_message_link">Founder message link</label>
                        <input type="text" name='AboutUs[founder_message_link]' class="form-control" id="AboutUs[Founder_message_link" placeholder="Enter Founder message link" value='{{$model->founder_message_link}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="AboutUs[Inspiration_link">Inspiration link</label>
                        <input type="text" name='AboutUs[inspiration_link]' class="form-control" id="AboutUs[Inspiration_link" placeholder="Enter Inspiration link" value='{{$model->inspiration_link}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="AboutUs[Board_of_directors_link">Board of directors link</label>
                        <input type="text" name='AboutUs[board_of_directors_link]' class="form-control" id="AboutUs[Board_of_directors_link" placeholder="Enter Board of directors link" value='{{$model->board_of_directors_link}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="AboutUs[Executive_management_link">Executive management link</label>
                        <input type="text" name='AboutUs[executive_management_link]' class="form-control" id="AboutUs[Executive_management_link" placeholder="Enter Executive management link" value='{{$model->executive_management_link}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="AboutUs[Management_team_link">Management team link</label>
                        <input type="text" name='AboutUs[management_team_link]' class="form-control" id="AboutUs[Management_team_link" placeholder="Enter Management team link" value='{{$model->management_team_link}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="AboutUs[Newsletter_description">Newsletter description</label>
                        <textarea name='AboutUs[newsletter_description]' class="form-control" id="AboutUs[Newsletter_description" placeholder="Enter Newsletter description">{!!$model->newsletter_description!!}</textarea>
                    </div>

                    @include('backend.about_us._files')


                    <div class='clearfix' ></div>                    
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}	
    @endpush
@endif


@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/about_us')}}">About uses</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop