<table class='table table-striped'>
	<tr>
	  <th>Founder message link</th>
	   <td>{{$model->founder_message_link}}</td>
	</tr>
	<tr>
	  <th>Inspiration link</th>
	   <td>{{$model->inspiration_link}}</td>
	</tr>
	<tr>
	  <th>Board of directors link</th>
	   <td>{{$model->board_of_directors_link}}</td>
	</tr>
	<tr>
	  <th>Executive management link</th>
	   <td>{{$model->executive_management_link}}</td>
	</tr>
	<tr>
	  <th>Management team link</th>
	   <td>{{$model->management_team_link}}</td>
	</tr>
	<tr>
	  <th>Newsletter description</th>
	   <td>{{$model->newsletter_description}}</td>
	</tr>
	<tr>
	  <th>Newsletter files</th>
	   <td>{{$model->newsletter_files}}</td>
	</tr>
</table>
  
  
