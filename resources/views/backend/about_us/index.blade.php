@extends('backend.layout.master')
@section('title' , 'About us')
@section('content')


<div class="card">
    <div class="card-header">
        <div class="btn-group float-md-right">
            <a href="{{url('backend/about_us/create')}}" class='btn btn-info' ><i class='fa fa-plus mr-1'></i>Create</a>
        </div>
    </div>
    <div class="card-content overflow-auto collapse show">
        <div class="card-body card-dashboard">
                <form method='get' class='search' >
                    <table class="table table-striped table-bordered zero-configuration">
                        <tr>
                            <th style="min-width: 65px">#</th>
                            <th>Founder message link</th>
                            <th>Inspiration link</th>
                            <th>Board of directors link</th>
                            <th>Executive management link</th>
                            <th>Management team link</th>
                            <th>Newsletter description</th>
                            <th>Newsletter files</th>
                            <th style="width: 65px">Actions</th>
                        </tr>
                        <tr>			
                            <th><input type='text' name='id' value='{{request('id')}}' class='form-control'></th>
                        <th><input type='text' name='founder_message_link' value='{{request('founder_message_link')}}' class='form-control'></th>
                        <th><input type='text' name='inspiration_link' value='{{request('inspiration_link')}}' class='form-control'></th>
                        <th><input type='text' name='board_of_directors_link' value='{{request('board_of_directors_link')}}' class='form-control'></th>
                        <th><input type='text' name='executive_management_link' value='{{request('executive_management_link')}}' class='form-control'></th>
                        <th><input type='text' name='management_team_link' value='{{request('management_team_link')}}' class='form-control'></th>

                            <th></th>
                        <th><input type='text' name='newsletter_files' value='{{request('newsletter_files')}}' class='form-control'></th>
                            <th style="width: 65px">
                                
                                <input type='submit' class='d-none' name='search'>
                            </th>
						
                        </tr>
                        @if($data->isNotEmpty())
                            @foreach($data as $row)
                                <tr>
                                    <td>{{$row->id}}</td>


                                    <td>{{$row->founder_message_link}}</td>


                                    <td>{{$row->inspiration_link}}</td>


                                    <td>{{$row->board_of_directors_link}}</td>


                                    <td>{{$row->executive_management_link}}</td>


                                    <td>{{$row->management_team_link}}</td>


                                    <td>{{$row->newsletter_description}}</td>


                                    <td>{{$row->newsletter_files}}</td>
                                    <td>
                                            <a href="{{url('backend/about_us/update/'.$row->id.'')}}" title='Edit'>
                                                <i class="fa fa-lg fa-edit"></i> 
                                            </a>
                                            <a href="{{url('backend/about_us/delete/'.$row->id.'')}}" title='Delete' onclick="return confirm('Are you sure you want to delete ?')" >
                                                <i class="fa fa-lg fa-remove"></i> 
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class='text-center' colspan='100'>No Data Available</td>
                            </tr> 
                        @endif
                    </table>
                </form>
            
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                {{ $data->appends(request()->except('page'))->links() }}			
            </div>
        </div>
    </div>
</div>
@stop

@push('footer')
    <script>
        $('.search input , .search select').change(function(){
            $('.search input[type=submit]').trigger('click');
        });
        $('.view').click(function(){
            var id=$(this).data().id;
            $.ajax({
                url:"{{url('backend/about_us/view')}}/"+id,
                success:function(data){
                    $('#modal').find('.modal-title').html('View About_u #'+id);
                    $('#modal').find('.modal-body').html(data);
                    $('#modal').modal();
                }
            })
        })
    </script>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item active">About uses</li>
@stop

