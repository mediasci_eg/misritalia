<?php
use App\Libs\Upload;
?>
<section style='width:100%' class='newsletter_files'>  
    <h4 class="card-title col-12">Development Brochures</h4>

    <?php 
        global $count;
        $count['newsletter_files'] = 0 ;
        if(!$model->exists || count($model->newsletter_files)==0)
            $newsletter_files = [['file'=>'','title'=>'']];
        else
            $newsletter_files = $model->newsletter_files;
    ?>
    @foreach($newsletter_files as $newsletter_file)
    <?php $count['newsletter_files']++ ;
	
	?>
    <div class='section_content row pl-1'>
        <div class='col-md-10 row'>


            <div class="form-group col-md-4">
                <label for="newsletter_fileFile">File</label>
                <?=Upload::pdf([
                    'name'=>'AboutUs[newsletter_files]['.$count['newsletter_files'].'][file]',
                    'value'=>@$newsletter_file['file'],
                ])?>
            </div>

            <div class="form-group col-md-4">
                <label for="newsletter_fileTitle">Title</label>
                <input type="text"
                    name='AboutUs[newsletter_files][{{$count['newsletter_files']}}][title]'
                    class="form-control" id="newsletter_fileTitle" placeholder="Enter Title"
                    value='{{@$newsletter_file['title']}}'>
            </div>

        </div>
        <div style='margin-top: 25px;' class='col-md-2'>
            @if($count['newsletter_files'] ==1)
            <input type='button' class='add btn btn-success' value="+">
            @else
            <input type='button' class='remove btn btn-danger' value="X">
            @endif
        </div>
    </div>
    <div class='clearfix'></div>
    @endforeach
</section>



@push('footer')
<script>
    $(document).ready(function(){
        count = <?=json_encode($count)?> ;    
        $('.add').click(function(){
            var content = $(this).parents('.section_content')[0].outerHTML+'\n\
                                <div class="clearfix" ></div>';
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $section.append(content);
            var $lastItem = $section.find('.section_content').last();
            $lastItem.find('.preview-container').html('');
            $lastItem.find('input , select , textarea').each(function(){
                $(this).val('');
                if(typeof $(this).attr('data-name')!='undefined'){
                    var name = $(this).attr('data-name');
                    var newName = name.replace(/\d+/ ,count[$section.attr('class')] );
                    $(this).attr('data-name' , newName);
                }
                var name = $(this).attr('name');
                if(typeof name!='undefined'){
                    var newName = name.replace(/\d+/ ,count[$section.attr('class')] );
                    $(this).attr('name' , newName);
                }
            })    
            
            var $lastItemBtn = $lastItem.find('.add');            
            $lastItemBtn.val('X');
            $lastItemBtn.removeClass('add');
            $lastItemBtn.removeClass('btn-success');
            $lastItemBtn.addClass('btn-danger');
            $lastItemBtn.addClass('remove');
        })
        
        $(document).on('click','.remove' , function(){
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $(this).closest('.section_content').remove();
        })
    })
</script>
@endpush
