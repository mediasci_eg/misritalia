<?php
use App\Http\Controllers\Helpers\Upload ;

if (!isset($model))
    $model = new \App\Models\Events;
?>
<div class="card">
    <div class="card-content">
        <div class="card-body">
            @success
            @errors
            <form action="backend/events/store" method="post" id="bu-form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$model->id}}">
                <div class="row">
                    <div class="col-5">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" value="{{old('title' , $model->title )}}" id="name"
                                class="form-control">
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="form-group">
                            <label for="title">Sub Title</label>
                            <input type="text" name="sub_title" value="{{old('title' , $model->sub_title )}}" id="name"
                                class="form-control">
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group">
                            <label for="date">Date</label>
                            <input type="date" name="date" value="{{old('date',$model->date)}}" id="name"
                                class="form-control">
                        </div>
                    </div>


                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label">Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css() ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('content', 'content', $model->content) ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js() ?>
                        </div>
                    </div>

                    <!-- Image-->
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label"> Image </label>
                            <span>(570 * 330)</span>
                            <input type="file" class='form-control' value="{{$model->image}}" name="image">
                            @if($model->image !='')
                            <img src='{{url('uploads/'.$model->image)}}' style='width:200px'>
                            @endif
                        </div>
                    </div>
					
                    <!-- Image-->
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label"> Cover Image </label>
                            <span>(1170 * 530)</span>
                            <input type="file" class='form-control' value="{{$model->cover_image}}" name="cover_image">
                            @if($model->image !='')
                            <img src='{{url('uploads/'.$model->cover_image)}}' style='width:200px'>
                            @endif
                        </div>
                    </div>




                    <div class="event-detials w-100 ">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="control-label">Event Images</label>
                                    <?= Upload::images([
                                        'name'=>'images[]' ,
                                        'size' => '840 * 520',
                                        'value'=>$model->images ,
                                    ]); ?>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('script')
{{-- <script>
    $(document).ready(function(){
    $('.type').change(function(){
        var val = $(this).val();
        $('.event-detials , .news-detials').addClass('');
        if(val == 'news')
            $('.news-detials').removeClass('');
        else
            $('.event-detials').removeClass('');
    });
    $('.type').trigger('change');
})
</script> --}}
@endpush
