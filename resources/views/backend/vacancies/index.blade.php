@extends('backend.layout.master')
@section('title' , 'Vacancies')
@section('content')


<div class="card">
    <div class="card-header">
        <div class="btn-group float-md-right">
            <a href="{{url('backend/vacancies/create')}}" class='btn btn-info' ><i class='fa fa-plus mr-1'></i>Create</a>
        </div>
    </div>
    <div class="card-content overflow-auto collapse show">
        <div class="card-body card-dashboard">
                <form method='get' class='search' >
                    <table class="table table-striped table-bordered zero-configuration">
                        <tr>
                            <th style="min-width: 65px">#</th>
                            <th>Title</th>
                            <th>About</th>
                            <th>Requirments</th>
                            <th>Is current</th>
                            <th style="width: 65px">Actions</th>
                        </tr>
                        <tr>			
                            <th><input type='text' name='id' value='{{request('id')}}' class='form-control'></th>
                        <th><input type='text' name='title' value='{{request('title')}}' class='form-control'></th>

                            <th></th>

                            <th></th>
                        <th>
                                <select name='is_current' class="form-control" >
                                    <option value='' >all</option>
                                    <option <?=selected( '1' , request('is_current') )?> value='1' >ON</option>
                                    <option <?=selected( '0' , request('is_current') )?> value='0' >OFF</option>
                                </select>
                            </th>
                            <th style="width: 65px">
                                
                                <input type='submit' class='d-none' name='search'>
                            </th>
						
                        </tr>
                        @if($data->isNotEmpty())
                            @foreach($data as $row)
                                <tr>
                                    <td>{{$row->id}}</td>


                                    <td>{{$row->title}}</td>


                                    <td>{{$row->about}}</td>


                                    <td>{{$row->requirments}}</td>

                                    <td>
                                        @if($row->is_current=='1')
                                            <i class="fa fa-check"></i>
                                        @else
                                            <i class="fa fa-close"></i>
                                        @endif
                                    </td>
                                    <td>
                                            <a href="{{url('backend/vacancies/update/'.$row->id.'')}}" title='Edit'>
                                                <i class="fa fa-lg fa-edit"></i> 
                                            </a>
                                            <a href="{{url('backend/vacancies/delete/'.$row->id.'')}}" title='Delete' onclick="return confirm('Are you sure you want to delete ?')" >
                                                <i class="fa fa-lg fa-remove"></i> 
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class='text-center' colspan='100'>No Data Available</td>
                            </tr> 
                        @endif
                    </table>
                </form>
            
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                {{ $data->appends(request()->except('page'))->links() }}			
            </div>
        </div>
    </div>
</div>
@stop

@push('footer')
    <script>
        $('.search input , .search select').change(function(){
            $('.search input[type=submit]').trigger('click');
        });
        $('.view').click(function(){
            var id=$(this).data().id;
            $.ajax({
                url:"{{url('backend/vacancies/view')}}/"+id,
                success:function(data){
                    $('#modal').find('.modal-title').html('View Vacancy #'+id);
                    $('#modal').find('.modal-body').html(data);
                    $('#modal').modal();
                }
            })
        })
    </script>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item active">Vacancies</li>
@stop

