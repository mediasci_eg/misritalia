@extends('backend.layout.master')
@if($model->exists)
@section('title' , 'Update Vacancy #'.$model->id)
@else
@section('title' , 'Create Vacancy')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'>
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6">
                        <label for="VacancyTitle">Title</label>
                        <input type="text" name='Vacancy[title]' class="form-control" id="VacancyTitle"
                            placeholder="Enter Title" value='{{$model->title}}'>
                    </div>


                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label">Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css() ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('about', 'about', $model->about) ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js() ?>
                        </div>
                    </div>


                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label">Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css() ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('requirments', 'requirments', $model->requirments) ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js() ?>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="VacancyIs_current">Is current</label>
                        <br />
                        <input type="hidden" name='Vacancy[is_current]' value='0'>
                        <input type="checkbox" name='Vacancy[is_current]' <?=checked('1' , $model->is_current)?>
                            id="VacancyIs_current" value='1'>
                    </div>


                    <div class='clearfix'></div>
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary'>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
@push('footer')
{!!$model->viewErrors()!!}
@endpush
@endif


@stop



@section('breadcrumbs')
<li class="breadcrumb-item"><a href="{{url('backend/vacancies')}}">Vacancies</a></li>
@if($model->exists)
<li class="breadcrumb-item active">Update</li>
@else
<li class="breadcrumb-item active">Create</li>
@endif
@stop
