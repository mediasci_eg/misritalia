<table class='table table-striped'>
	<tr>
	  <th>Title</th>
	   <td>{{$model->title}}</td>
	</tr>
	<tr>
	  <th>About</th>
	   <td>{{$model->about}}</td>
	</tr>
	<tr>
	  <th>Requirments</th>
	   <td>{{$model->requirments}}</td>
	</tr>
	<tr>
	  <th>Is current</th>
	  <td>
		@if($model->is_current=='1')
                    <i class="fa fa-check"></i>
		@else
                    <i class="fa fa-close"></i>
		@endif
	   </td>
	</tr>
</table>
  
  
