<?php
use App\Http\Controllers\Helpers\Upload ;
?>
@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update Message of hope #'.$model->id)
@else
	@section('title' , 'Create Message of hope')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'  >
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-12">
                        <label for="MessageOfHopeDescription">Description</label>
                        <textarea style='height:200px' name='MessageOfHope[description]' class="form-control" id="MessageOfHopeDescription" placeholder="Enter Description">{!!$model->description!!}</textarea>
                    </div>

                    <div class="col-md-12">
                        <h3>Videos</h3>
                    </div>
                    @if(!is_array($model->videos))
                        @php
                            $model->videos = ['',''] ;
                        @endphp
                    @endif

                    @foreach($model->videos as $video)
                    <div class="form-group col-md-6">
                        <input type='text' name='MessageOfHope[videos][]' class="form-control" value='{{$video}}' >
                    </div>
                    @endforeach



                    <div class='clearfix' ></div>
                    <section class='messageOfHopeTabs w-100'>
                        <div class="col-md-12">
                            <h3>Message of hope tabs</h3>
                        </div>
                        <?php
                            $count['messageOfHopeTabs'] = 0 ;
                            if(!$model->exists)
                                $messageOfHopeTabs = [new \App\Models\MessageOfHopeTab];
                            else
                                $messageOfHopeTabs = $model->tabs;
                        ?>
                        @foreach($messageOfHopeTabs as $messageOfHopeTab)
                        <?php $count['messageOfHopeTabs']++ ?>
                        <div class='section_content row pl-1'>
                    <div class='col-md-10 row' >


                    <div class="form-group col-md-6">
                        <label for="MessageOfHopeTabTitle">Title</label>
                        <input type="text" name='MessageOfHopeTab[{{$count['messageOfHopeTabs']}}][title]' class="form-control" id="MessageOfHopeTabTitle" placeholder="Enter Title" value='{{$messageOfHopeTab->title}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="MessageOfHopeTabDescription">Description</label>
                        <textarea name='MessageOfHopeTab[{{$count['messageOfHopeTabs']}}][description]' class="form-control" id="MessageOfHopeTabDescription" placeholder="Enter Description">{!!$messageOfHopeTab->description!!}</textarea>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="MessageOfHopeTabImages">Images</label>
                        <?= Upload::images([
                            'name'=>"MessageOfHopeTab[".$count['messageOfHopeTabs']."][images][]" ,
                            'size'=>'840 * 520',
                            'value'=>$messageOfHopeTab->images ,
                        ]); ?>
                    </div>



                    </div>
                    <div style='margin-top: 25px;' class='col-md-2' >
                        @if($count['messageOfHopeTabs'] ==1)
                        <input type='button' class='add btn btn-success' value="+">
                        @else
                        <input type='button' class='remove btn btn-danger' value="X">
                        @endif
                    </div>
                    </div>
                    <div class='clearfix' ></div>
                        @endforeach
                    </section>

                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}
    @endpush
@endif


@stop


@push('footer')
<script>
    $(document).ready(function(){
        count = <?=json_encode($count)?> ;
        $('.add').click(function(){
            var content = $(this).parents('.section_content')[0].outerHTML+'\n\
                                <div class="clearfix" ></div>';
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $section.append(content);
            var $lastItem = $section.find('.section_content').last();
            $lastItem.find('input , select , textarea').each(function(){
                $(this).val('');
                var name = $(this).attr('name');
                if(typeof name!='undefined'){
                    var newName = name.replace(/\d+/ ,count[$section.attr('class')] );
                    $(this).attr('name' , newName);
                }
            })

            var $lastItemBtn = $lastItem.find('.add');
            $lastItemBtn.val('X');
            $lastItemBtn.removeClass('add');
            $lastItemBtn.removeClass('btn-success');
            $lastItemBtn.addClass('btn-danger');
            $lastItemBtn.addClass('remove');
        })

        $(document).on('click','.remove' , function(){
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $(this).closest('.section_content').remove();
        })
    })

</script>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/message_of_hope')}}">Message of hopes</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop
