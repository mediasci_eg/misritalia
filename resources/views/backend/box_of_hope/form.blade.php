<?php
use App\Http\Controllers\Helpers\Upload ;
?>
@extends('backend.layout.master')
@if($model->exists)
@section('title' , 'Update Box of hope #'.$model->id)
@else
@section('title' , 'Create Box of hope')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'>
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6">
                        <label for="BoxOfHopeDescription">Description</label>
                        <textarea name='BoxOfHope[description]' class="form-control" id="BoxOfHopeDescription"
                            placeholder="Enter Description">{!!$model->description!!}</textarea>
                    </div>


                    <div class='clearfix'></div>
                    <section class='boxOfHopeVolums' style='width:100%'>
                        <div class="col-md-12">
                            <h3>Box of hope volums</h3>
                        </div>
                        <?php
                            $count['boxOfHopeVolums'] = 0 ;
                            if(!$model->exists)
                                $boxOfHopeVolums = [new \App\Models\BoxOfHopeVolum];
                            else
                                $boxOfHopeVolums = $model->volumes;
                        ?>
                        @foreach($boxOfHopeVolums as $boxOfHopeVolum)
                        <?php $count['boxOfHopeVolums']++ ?>
                        <div class='section_content row pl-1'>
                            <div class='col-md-10 row'>


                                <div class="form-group col-md-6">
                                    <label for="BoxOfHopeVolumTitle">Title</label>
                                    <input type="text" name='BoxOfHopeVolum[{{$count['boxOfHopeVolums']}}][title]'
                                        class="form-control" id="BoxOfHopeVolumTitle" placeholder="Enter Title"
                                        value='{{$boxOfHopeVolum->title}}'>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="BoxOfHopeVolumDescription">Description</label>
                                    <textarea name='BoxOfHopeVolum[{{$count['boxOfHopeVolums']}}][description]'
                                        class="form-control" id="BoxOfHopeVolumDescription"
                                        style='height:200px'
                                        placeholder="Enter Description">{!!$boxOfHopeVolum->description!!}</textarea>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="BoxOfHopeVolumDate">Date</label>
                                    <input type="date" name='BoxOfHopeVolum[{{$count['boxOfHopeVolums']}}][date]'
                                        class="form-control" id="BoxOfHopeVolumDate" placeholder="Enter Date"
                                        value='{{$boxOfHopeVolum->date}}'>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="BoxOfHopeVolumImages">Images</label>
                                    <?= Upload::images([
                                        'name'=>"BoxOfHopeVolum[".$count['boxOfHopeVolums']."][images][]" ,
                                        'size'=>'840 * 520',
                                        'value'=>$boxOfHopeVolum->images ,
                                    ]); ?>
                                </div>



                            </div>
                            <div style='margin-top: 25px;' class='col-md-2'>
                                @if($count['boxOfHopeVolums'] ==1)
                                <input type='button' class='add btn btn-success' value="+">
                                @else
                                <input type='button' class='remove btn btn-danger' value="X">
                                @endif
                            </div>
                        </div>
                        <div class='clearfix'></div>
                        @endforeach
                    </section>

                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary'>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@if(isset($errors))
@push('footer')
{!!$model->viewErrors()!!}
@endpush
@endif


@stop


@push('footer')
<script>
    $(document).ready(function(){
        count = <?=json_encode($count)?> ;
        $('.add').click(function(){
            var content = $(this).parents('.section_content')[0].outerHTML+'\n\
                                <div class="clearfix" ></div>';
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $section.append(content);
            var $lastItem = $section.find('.section_content').last();
            $lastItem.find('input , select , textarea').each(function(){
                $(this).val('');
                var name = $(this).attr('name');
                if(typeof name!='undefined'){
                    var newName = name.replace(/\d+/ ,count[$section.attr('class')] );
                    $(this).attr('name' , newName);
                }
            })

            var $lastItemBtn = $lastItem.find('.add');
            $lastItemBtn.val('X');
            $lastItemBtn.removeClass('add');
            $lastItemBtn.removeClass('btn-success');
            $lastItemBtn.addClass('btn-danger');
            $lastItemBtn.addClass('remove');
        })

        $(document).on('click','.remove' , function(){
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $(this).closest('.section_content').remove();
        })
    })

</script>
@endpush

@section('breadcrumbs')
<li class="breadcrumb-item"><a href="{{url('backend/box_of_hope')}}">Box of hopes</a></li>
@if($model->exists)
<li class="breadcrumb-item active">Update</li>
@else
<li class="breadcrumb-item active">Create</li>
@endif
@stop
