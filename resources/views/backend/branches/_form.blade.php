<div class="card">
        <div class="card-content">
            <div class="card-body">
                @success @errors
                <form action="backend/branches/store" method="post" id="bu-form" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{$branch->id}}">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input required type="text" name="name" value="{{old('name' , $branch->name )}}" id="name" class="form-control">
                            </div>
                        </div>
    
                        <div class="col-6">
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input required type="text" name="address" value="{{old('address' , $branch->address )}}" id="address" class="form-control">
                            </div>
                        </div>
    
                        <div class="col-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input  type="email" name="email" value="{{old('email' , $branch->email )}}" id="email" class="form-control">
                            </div>
                        </div>
    
                        <div class="col-6">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input required type="text" name="phone" value="{{old('phone' , $branch->phone )}}" id="phone" class="form-control">
                            </div>
                        </div>
    
                        <div class="hr-line-dashed"></div>
                        <div class="col-12">
                            <div class="form-group">
                                <input name="lat" type="hidden" id="maplat" value="{{old('lat' , $branch->lat )}}">
                                <input name="lng" type="hidden" id="maplng" value="{{old('lng' , $branch->lng )}}">
                                <label id="location-error" style="display:none;color: #cc5965;margin-left: 5px;">The branch location is required.</label> @include('map')
                            </div>
    
                        </div>
    
    
                    </div>
                    <hr>
                    <div class="row">
                        <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @push('script') @endpush