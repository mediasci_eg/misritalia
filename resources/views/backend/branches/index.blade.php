@extends(config('backend-users.backend_layout'))

@section(config('backend-users.layout_content_area'))
    <div class="content-header row">
          
        <div class="content-header-left col-md-6 col-12 mb-1">
        <h3 class="content-header-title">Branches Data</h3>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./backend">Home</a>
                </li>
                <li class="breadcrumb-item active">Branches
                </li>
            </ol>
        </div>
        </div>
    </div>

    <div class="content-body">
            
      
        <section id="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Branches</h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <a href="backend/branches/create" class="btn btn-xs btn-outline-primary" title="new User"><i class="fa fa-plus" aria-hidden="true"></i> New</a>
                                <br><br>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                
                                                <th>name</th>
                                                <th>Address</th>
                                                <th>phone</th>
                                                <th>Email</th>
                                                <th>Actions</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($branches as $branch)
                                                <tr>
                                         <td>{{ $branch->name }}</td>
                                         <td>{{ $branch->address }}</td>
                                         <td>{{ $branch->phone }}</td>
                                         <td>{{ $branch->email }}</td>
                                         <td>
                                            <a href="backend/branches/update/{{$branch->id}}" class="btn btn-sm btn-outline-info" title="Edit"><i class="fa fa-pen" aria-hidden="true"></i></a>
                                            <a href="backend/branches/delete/{{ $branch->id }}" class="btn btn-sm btn-outline-danger delete" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('script')
@deletejs

@endpush