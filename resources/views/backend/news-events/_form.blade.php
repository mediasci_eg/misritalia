<?php
use App\Http\Controllers\Helpers\Upload ;

if (!isset($model))
    $model = new \App\Models\NewsEvents;
?>
<div class="card">
    <div class="card-content">
        <div class="card-body">
            @success
            @errors
            <form action="backend/news-events/store" method="post" id="bu-form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$model->id}}">
                <div class="row">
                    <div class="col-5">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" value="{{old('title' , $model->title )}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label for="type">Type</label>
                            <select name="type" class="type form-control ">
                                <option  @if($model->type==null) selected  @endif value="" selected disabled hidden>{{old('type',$model->type)}}</option>
                                <option  @if($model->type=="News") selected  @endif value="News">News</option>
                                <option @if($model->type=="Event") selected  @endif value="Event">Event</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="date">Date</label>
                            <input type="date" name="date" value="{{old('date',$model->date)}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <input type="checkbox" name="is_featured" @if($model->is_featured == 1) checked @endif>Featured
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label">Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css() ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('content', 'content', $model->content) ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js() ?>
                        </div>
                    </div>

                    <!--Square Image-->
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Square Image </label>
                            <input type="file" class='form-control' value="{{$model->square_image}}" name="square_image">
                            @if($model->square_image !='')
                            <img src='{{url('uploads/'.$model->square_image)}}' style='width:200px'>
                            @endif
                        </div>
                    </div>

                    <!--Rectangle Image-->
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Rectangle Image </label>
                            <input type="file" class='form-control' name="rectangle_image">
                            @if($model->rectangle_image !='')
                            <img src='{{url('uploads/'.$model->rectangle_image)}}' style='width:200px'>
                            @endif
                        </div>
                    </div>
                    <div class="col-12">
                        <h2>Details Page</h2>
                    </div>
                    <!--Detail Image-->
                    <div class="news-detials d-none">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="control-label">Detail Image </label>
                                <input type="file" class='form-control' name="detail_image">
                                @if($model->detail_image !='')
                                <img src='{{url('uploads/'.$model->detail_image)}}' style='width:200px'>
                                @endif
                            </div>
                        </div>
                    </div>
                    
                    <div class="event-detials w-100 d-none">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="control-label">Event Images</label>
                                    <?= Upload::images([
                                        'name'=>'images[]' ,
                                        'value'=>$model->images ,
                                    ]); ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="control-label">Video</label>
                                    <input type="text" class='form-control' name="video" value='{{old('video' , $model->video)}}' >
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('script')
<script>
$(document).ready(function(){
    $('.type').change(function(){
        var val = $(this).val();
        $('.event-detials , .news-detials').addClass('d-none');
        if(val == 'news')
            $('.news-detials').removeClass('d-none');
        else
            $('.event-detials').removeClass('d-none');        
    });
    $('.type').trigger('change');
})
</script> 
    @endpush