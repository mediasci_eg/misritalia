<?php
use App\Libs\Upload;
?>
<section style='width:100%' class='sub'>
    <h4 class="card-title col-12">Sub Content</h4>

    <?php
        global $count;
        $count['sub'] = 0 ;
        if(count($model->children)==0)
            $children = [new \App\Models\Csr];
        else
            $children = $model->children;
    ?>
    @foreach($children as $sub)
    <?php $count['sub']++ ?>
    <div class='section_content row pl-1'>
        <div class='col-md-10 row'>
            <div class="form-group col-md-4">
                <label for="SubTitle">Title</label>
                <input type="text"
                    name='Sub[{{$count['sub']}}][title]'
                    class="form-control" id="SubTitle" placeholder="Enter Title"
                    value='{{$sub->title}}'>
            </div>
            <div class="form-group col-md-4">
                <label for="CsrDescription">Description</label>
                <textarea
                    name='Sub[{{$count['sub']}}][description]'
                    class="form-control" id="CsrDescription" placeholder="Enter Description">{!!$model->description!!}</textarea>
            </div>

            <div class="form-group col-md-4">
                <label for="SubFile">Image</label>
                <?=Upload::image([
                    'name'=>'Sub['.$count['sub'].'][image]',
					'size'=>'150 * 150',
                    'value'=>$sub->image,
                ])?>
            </div>


        </div>
        <div style='margin-top: 25px;' class='col-md-2'>
            @if($count['sub'] ==1)
            <input type='button' class='add btn btn-success' value="+">
            @else
            <input type='button' class='remove btn btn-danger' value="X">
            @endif
        </div>
    </div>
    <div class='clearfix'></div>
    @endforeach
</section>


@push('footer')
<script>
    $(document).ready(function(){
        count = <?=json_encode($count)?> ;
        $('.add').click(function(){
            var content = $(this).parents('.section_content')[0].outerHTML+'\n\
                                <div class="clearfix" ></div>';
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $section.append(content);
            var $lastItem = $section.find('.section_content').last();
            $lastItem.find('.preview-container').html('');
            $lastItem.find('input , select , textarea').each(function(){
                $(this).val('');
                if(typeof $(this).attr('data-name')!='undefined'){
                    var name = $(this).attr('data-name');
                    var newName = name.replace(/\d+/ ,count[$section.attr('class')] );
                    $(this).attr('data-name' , newName);
                }
                var name = $(this).attr('name');
                if(typeof name!='undefined'){
                    var newName = name.replace(/\d+/ ,count[$section.attr('class')] );
                    $(this).attr('name' , newName);
                }
            })

            var $lastItemBtn = $lastItem.find('.add');
            $lastItemBtn.val('X');
            $lastItemBtn.removeClass('add');
            $lastItemBtn.removeClass('btn-success');
            $lastItemBtn.addClass('btn-danger');
            $lastItemBtn.addClass('remove');
        })

        $(document).on('click','.remove' , function(){
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $(this).closest('.section_content').remove();
        })
    })
</script>
@endpush
