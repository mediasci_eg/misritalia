<table class='table table-striped'>
	<tr>
	  <th>Title</th>
	   <td>{{$model->title}}</td>
	</tr>
	<tr>
	  <th>Description</th>
	   <td>{{$model->description}}</td>
	</tr>
	<tr>
	  <th>Image</th>
	  <td><img src={{url('uploads/'.$model->image)}} style='width:100px' ></td>
	</tr>
	<tr>
	  <th>Title2</th>
	   <td>{{$model->title2}}</td>
	</tr>
	<tr>
	  <th>Description2</th>
	   <td>{{$model->description2}}</td>
	</tr>
	<tr>
	  <th>Parent</th>
	   <td>{{$model->parent_id}}</td>
	</tr>
	<tr>
	  <th>Has sub</th>
	  <td>
		@if($model->has_sub=='1')
                    <i class="fa fa-check"></i>
		@else
                    <i class="fa fa-close"></i>
		@endif
	   </td>
	</tr>
	<tr>
	  <th>Has 2 titles</th>
	  <td>
		@if($model->has_2_titles=='1')
                    <i class="fa fa-check"></i>
		@else
                    <i class="fa fa-close"></i>
		@endif
	   </td>
	</tr>
</table>
  
  
