
<?php
use App\Libs\Upload;
?>
@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update Csr #'.$model->id)
@else
	@section('title' , 'Create Csr')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post' enctype="multipart/form-data" >
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6">
                        <label for="CsrTitle">Title</label>
                        <input type="text" name='Csr[title]' class="form-control" id="CsrTitle" placeholder="Enter Title" value='{{$model->title}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="CsrDescription">Description</label>
                        <textarea style='height:200px' name='Csr[description]' class="form-control" id="CsrDescription" placeholder="Enter Description">{!!$model->description!!}</textarea>
                    </div>

                    @if($model->has_2_titles==1)
                        <div class="form-group col-md-6">
                            <label for="CsrTitle2">Title2</label>
                            <input type="text" name='Csr[title2]' class="form-control" id="CsrTitle2" placeholder="Enter Title2" value='{{$model->title2}}'>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="CsrDescription2">Description2</label>
                            <textarea style='height:200px' name='Csr[description2]' class="form-control" id="CsrDescription2" placeholder="Enter Description2">{!!$model->description2!!}</textarea>
                        </div>
                    @endif


                    <div class="form-group col-md-6">
                        <label for="CsrImage">Image</label>
                            <?=Upload::image([
                                'name'=>'Csr[image]',
								'size'=>'size is not fixed',
                                'value'=>$model->image,
                            ])?>
                    </div>

                    @if($model->has_sub==1)
                        @include('backend.csr._sub')
                    @endif
                    <div class='clearfix' ></div>
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}
    @endpush
@endif


@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/csr')}}">Csrs</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop
