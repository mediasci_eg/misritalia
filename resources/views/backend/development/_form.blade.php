<?php

use App\Libs\Upload;

if (!isset($model))
    $model = new \App\Models\Development;
?>
<div class="card">
    <div class="card-content">
        <div class="card-body">
            @success @errors
            <form action="backend/developments/store" method="post" id="bu-form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$model->id}}">
                <div class="row">
                    <h4 class="card-title col-12">Main Section</h4>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input required type="text" name="title" value="{{old('title' , $model->title )}}" id="name"
                                class="form-control">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="name">Slug</label>
                            <input required type="text" name="slug" value="{{old('slug' , $model->slug )}}"
                                class="form-control">
                        </div>
                    </div>

                    <!-- Image-->
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label"> Image </label>
                            <?=
                            Upload::image([
                                'name' => 'image',
                                'size' => 'size is not fixed' ,
                                'value' => $model->image,
                            ])
                            ?>
                        </div>
                    </div>

                    <!-- Logo-->
                    <div class="col-6">
                        <div class="logo_container form-group">
                            <label class="control-label"> Logo </label>
                            <?=
                            Upload::image([
                                'name' => 'logo',
                                'size' => 'size is not fixed' ,
                                'value' => $model->logo,
                            ])
                            ?>
                            <style>
                                .logo_container img {
                                    background: grey;
                                }
                            </style>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label"> Cover Image </label>
                            <?=
                            Upload::image([
                                'name' => 'cover_image',
                                'size' => '1350 * 625' ,
                                'value' => $model->cover_image,
                            ])
                            ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label"> Home Image </label>
                            <?=
                            Upload::image([
                                'name' => 'home_image',
                                'size' => '1350 * 465' ,
                                'value' => $model->home_image,
                            ])
                            ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label"> Masterplan </label>
                            <?=
                            Upload::image([
                                'name' => 'masterplan',
                                'size' => 'size is not fixed' ,
                                'value' => $model->masterplan,
                            ])
                            ?>
                        </div>
                    </div>
                    <hr class='w-100 border-blue' >
                    <!--Overview Section-->
                    <h4 class="card-title col-12">Concept Section</h4>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label"> Image </label>
                            <?=Upload::image([
                                'name'=>'overview_image',
                                'size' => '570 * 440' ,
                                'value'=>$model->overview_image,
                            ])?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="overview_content">Description </label>
                            <textarea required type="text" name="overview_content" id="address"
                                class="form-control">{{old('overview_content' , $model->overview_content )}}</textarea>
                        </div>
                    </div>
					<div class="col-6">
                        <div class="form-group">
                            <label for="overview_content">Or Video </label>
							<input type="text" name="overview_video" value="{{old('overview_video' , $model->overview_video )}}"
                                class="form-control">
                        </div>
                    </div>
                    <hr class='w-100 border-blue' >
                    <!--Location Section-->
                    <h4 class="card-title col-12">Location Section</h4>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label"> Image </label>
                            <?=Upload::image([
                                'name'=>'location_image',
                                'size' => 'size is not fixed' ,
                                'value'=>$model->location_image,
                            ])?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="location_content">Description </label>
                            <textarea required type="text" name="location_content" id="address"
                                class="form-control">{{old('location_content' , $model->location_content )}}</textarea>
                        </div>
                    </div>

                    <!--video section-->
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Video</label>
                            <input type="text" class='form-control' name="video"
                                value='{{old('video' , $model->video)}}'>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Location Link</label>
                            <input type="text" class='form-control' name="location_link"
                                value='{{old('location_link' , $model->location_link)}}'>
                        </div>
                    </div>
                    <hr class='w-100 border-blue' >
                    <!--Design Section-->
                    <h4 class="card-title col-12">Design Section</h4>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label"> Image </label>
                            <?=Upload::image([
                                'name'=>'design_image',
                                'size' => '570 * 370' ,
                                'value'=>$model->design_image,
                            ])?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="design_content">Description </label>
                            <textarea required type="text" name="design_content" id="address"
                                class="form-control">{{old('design_content' , $model->design_content )}}</textarea>
                        </div>
                    </div>
                    <hr class='w-100 border-blue' >
                    <!--Unit Types Section-->
                    <h4 class="card-title col-12">Unit Types Section</h4>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label"> Image </label>
                            <?=Upload::image([
                                'name'=>'unit_types_image',
                                'size' => '570 * 370' ,
                                'value'=>$model->unit_types_image,
                            ])?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="unit_types_content">Description </label>
                            <textarea required type="text" name="unit_types_content" id="address"
                                class="form-control">{{old('unit_types_content' , $model->unit_types_content )}}</textarea>
                        </div>
                    </div>
                    <?php
                        global $count  ;
                    ?>

                    @include('backend.development.partials.unit-types')
                    <hr class='w-100 border-blue' >
                    @include('backend.development.partials.brochures')
                    <hr class='w-100 border-blue' >
                    <!--Slider Section-->
                    <h4 class="card-title col-12">Galllery Images</h4>
                    <div class="col-6 form-group " id="slider-container">
                        <label class="control-label">Images </label>
                        <?php
                            $slider = $model->slider->pluck('image') ;
                        ?>
                        <?=Upload::images([
                                'name'=>'images[]',
                                'size' => '1170 * 450' ,
                                'value'=>$slider,
                            ])?>
                    </div>


                </div>
                <hr class='w-100 border-blue' >
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('footer')
<script>
    $(document).ready(function(){
        count = <?=json_encode($count)?> ;
        $('.add').click(function(){
            var content = $(this).parents('.section_content')[0].outerHTML+'\n\
                                <div class="clearfix" ></div>';
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $section.append(content);
            var $lastItem = $section.find('.section_content').last();
            $lastItem.find('.preview-container').html('');
            $lastItem.find('input , select , textarea').each(function(){
                $(this).val('');
                if(typeof $(this).attr('data-name')!='undefined'){
                    var name = $(this).attr('data-name');
                    var newName = name.replace(/\d+/ ,count[$section.attr('class')] );
                    $(this).attr('data-name' , newName);
                }
                var name = $(this).attr('name');
                if(typeof name!='undefined'){
                    var newName = name.replace(/\d+/ ,count[$section.attr('class')] );
                    $(this).attr('name' , newName);
                }
            })

            var $lastItemBtn = $lastItem.find('.add');
            $lastItemBtn.val('X');
            $lastItemBtn.removeClass('add');
            $lastItemBtn.removeClass('btn-success');
            $lastItemBtn.addClass('btn-danger');
            $lastItemBtn.addClass('remove');
        })

        $(document).on('click','.remove' , function(){
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $(this).closest('.section_content').remove();
        })
    })
</script>
@endpush

<style>
.card .card-title {
    text-align: center;
    margin: 20px 0;
    font-size: 20px;
}
</style>
