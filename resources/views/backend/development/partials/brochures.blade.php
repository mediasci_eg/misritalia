<?php
use App\Libs\Upload;
?>
<section style='width:100%' class='developmentBrochures'>  
    <h4 class="card-title col-12">Development Brochures</h4>

    <?php 
        global $count;
        $count['developmentBrochures'] = 0 ;
        if(!$model->exists || count($model->brochures)==0)
            $developmentBrochures = [new \App\Models\DevelopmentBrochure];
        else
            $developmentBrochures = $model->brochures;
    ?>
    @foreach($developmentBrochures as $developmentBrochure)
    <?php $count['developmentBrochures']++ ?>
    <div class='section_content row pl-1'>
        <div class='col-md-10 row'>


            <div class="form-group col-md-4">
                <label for="DevelopmentBrochureFile">File</label>
                <?=Upload::pdf([
                    'name'=>'DevelopmentBrochure['.$count['developmentBrochures'].'][brochure]',                    
                    'value'=>$developmentBrochure->brochure,
                ])?>
            </div>

            <div class="form-group col-md-4">
                <label for="DevelopmentBrochureTitle">Title</label>
                <input type="text"
                    name='DevelopmentBrochure[{{$count['developmentBrochures']}}][title]'
                    class="form-control" id="DevelopmentBrochureTitle" placeholder="Enter Title"
                    value='{{$developmentBrochure->title}}'>
            </div>

            <div class="form-group col-md-4">
                <label for="DevelopmentBrochureType">Type</label>
                <select name='DevelopmentBrochure[{{$count['developmentBrochures']}}][type]' class="form-control" id="DevelopmentBrochureType">
                    <option value='' >Choose Type</option>
                    <option @if($developmentBrochure->type=='general') selected @endif value='general' >Community</option>
                    <option @if($developmentBrochure->type=='apartment') selected @endif value='apartment' >Apartment</option>
                    <option @if($developmentBrochure->type=='chalets') selected @endif value='chalets' >Chalets</option>
                    <option @if($developmentBrochure->type=='villa') selected @endif value='villa' >Villa</option>
                </select>
            </div>
        </div>
        <div style='margin-top: 25px;' class='col-md-2'>
            @if($count['developmentBrochures'] ==1)
            <input type='button' class='add btn btn-success' value="+">
            @else
            <input type='button' class='remove btn btn-danger' value="X">
            @endif
        </div>
    </div>
    <div class='clearfix'></div>
    @endforeach
</section>
