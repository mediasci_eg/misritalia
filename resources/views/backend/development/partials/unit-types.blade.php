<?php
use App\Libs\Upload;
?>
<section style='width:100%' class='developmentUnitTypes'>
    <div class="col-md-12">
        <h4>Images</h4>
    </div>
    <?php
        global $count  ;
        $count['developmentUnitTypes'] = 0 ;
        if(!$model->exists ||  count($model->unitTypes)==0)
            $developmentUnitTypes = [new \App\Models\DevelopmentUnitType];
        else
            $developmentUnitTypes = $model->unitTypes;
    ?>
    @foreach($developmentUnitTypes as $developmentUnitType)
    <?php $count['developmentUnitTypes']++ ?>
    <div class='section_content row pl-1'>
        <div class='col-md-10 row'>
			<div class="form-group col-md-2">
			    <label>Sort</label>
				<input type="text" class='form-control' name="DevelopmentUnitType[{{$count['developmentUnitTypes']}}][sort]"
                                value='{{$developmentUnitType->sort}}'>
			</div>
            <div class="form-group col-md-5">
                <label for="DevelopmentUnitTypeImage">Image</label>

                <?=
                    Upload::image([
                        'name' => 'DevelopmentUnitType['.$count['developmentUnitTypes'].'][image]',
                        'size' => '560 * 380' ,
                        'value' => $developmentUnitType->image,
                    ])
                ?>
            </div>

            <div class="form-group col-md-5">
                <label for="DevelopmentUnitTypeTitle">Description</label>
                <textarea
                    name='DevelopmentUnitType[{{$count['developmentUnitTypes']}}][title]'
                    class="form-control" id="DevelopmentUnitTypeTitle" placeholder="Enter Title"
                    >{{$developmentUnitType->title}}</textarea>
            </div>



        </div>
        <div style='margin-top: 25px;' class='col-md-2'>
            @if($count['developmentUnitTypes'] ==1)
            <input type='button' class='add btn btn-success' value="+">
            @else
            <input type='button' class='remove btn btn-danger' value="X">
            @endif
        </div>
    </div>
    <div class='clearfix'></div>
    @endforeach
</section>
