@extends('backend.layout.master')
@if($model->exists)
	@section('title' , 'Update Construction #'.$model->id)	
@else
	@section('title' , 'Create Construction')
@endif

@section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post'  >
                <div class="row">
                    {{ csrf_field() }}		
                    <div class="form-group col-md-6">
                        <?php $developments=\App\Models\Development::all() ?>                        
                        <label for="ConstructionDevelopment_id">Development</label>
                        <select name='Construction[development_id]' class="form-control" id="ConstructionDevelopment_id">
                            <option value='' >Choose Development</option>
                            @foreach($developments as $development)
                                <option <?=selected($development->id , $model->development_id)?> value='{{$development->id}}' >{{$development->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="ConstructionDescription">Description</label>
                        <textarea name='Construction[description]' class="form-control" id="ConstructionDescription" placeholder="Enter Description">{!!$model->description!!}</textarea>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="ConstructionVideo">Video</label>
                        <input type="text" name='Construction[video]' class="form-control" id="ConstructionVideo" placeholder="Enter Video" value='{{$model->video}}'>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="ConstructionDate">Date</label>
                        <input type="date" name='Construction[date]' class="form-control" id="ConstructionDate" placeholder="Enter Date" value='{{$model->date}}'>
                    </div>


                    <div class='clearfix' ></div>                    
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if(isset($errors))
    @push('footer')
        {!!$model->viewErrors()!!}	
    @endpush
@endif


@stop



@section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="{{url('backend/constructions')}}">Constructions</a></li>
    @if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    @else
        <li class="breadcrumb-item active">Create</li>
    @endif
@stop