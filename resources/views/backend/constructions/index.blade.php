@extends('backend.layout.master')
@section('title' , 'Constructions')
@section('content')


<div class="card">
    <div class="card-header">
        <div class="btn-group float-md-right">
            <a href="{{url('backend/constructions/create')}}" class='btn btn-info' ><i class='fa fa-plus mr-1'></i>Create</a>
        </div>
    </div>
    <div class="card-content overflow-auto collapse show">
        <div class="card-body card-dashboard">
                <form method='get' class='search' >
                    <table class="table table-striped table-bordered zero-configuration">
                        <tr>
                            <th style="min-width: 65px">#</th>
                            <th>Development</th>
                            <th>Date</th>
                            <th style="width: 65px">Actions</th>
                        </tr>
                        <tr>			
                            <th><input type='text' name='id' value='{{request('id')}}' class='form-control'></th>
                        <?php $developments=\App\Models\Development::all() ?>						
                            <th>
                                <select name='development_id' class="form-control" >
                                    <option value='' >all</option>
                                    @foreach($developments as $development)
                                        <option <?=selected($development->id , request('development_id') )?> value='{{$development->id}}' >{{$development->title}}</option>
                                    @endforeach                                            
                                </select>
                            </th>                        <th><input type='date' name='date' value='{{request('date')}}' class='form-control'></th>                            <th style="width: 65px">
                                
                                <input type='submit' class='d-none' name='search'>
                            </th>
						
                        </tr>
                        @if($data->isNotEmpty())
                            @foreach($data as $row)
                                <tr>
                                    <td>{{$row->id}}</td>

                                    <td>{{$row->development->title}}</td>

                                    <td>{{$row->date}}</td>
                                    <td>
                                            <a href="{{url('backend/constructions/update/'.$row->id.'')}}" title='Edit'>
                                                <i class="fa fa-lg fa-edit"></i> 
                                            </a>
                                            <a href="{{url('backend/constructions/delete/'.$row->id.'')}}" title='Delete' onclick="return confirm('Are you sure you want to delete ?')" >
                                                <i class="fa fa-lg fa-remove"></i> 
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class='text-center' colspan='100'>No Data Available</td>
                            </tr> 
                        @endif
                    </table>
                </form>
            
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                {{ $data->appends(request()->except('page'))->links() }}			
            </div>
        </div>
    </div>
</div>
@stop

@push('footer')
    <script>
        $('.search input , .search select').change(function(){
            $('.search input[type=submit]').trigger('click');
        });
        $('.view').click(function(){
            var id=$(this).data().id;
            $.ajax({
                url:"{{url('backend/constructions/view')}}/"+id,
                success:function(data){
                    $('#modal').find('.modal-title').html('View Construction #'+id);
                    $('#modal').find('.modal-body').html(data);
                    $('#modal').modal();
                }
            })
        })
    </script>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item active">Constructions</li>
@stop

