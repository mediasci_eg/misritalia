<div style='clear:both' ></div>

<input type="text" placeholder="Search ..."  id="pac-input" class="form-control " >
<div id="map" style="width:100%;height:300px;"></div>

<script>
    function initMap() {
		if($('#maplat').val() != '' )
		{
			var lat = parseFloat($('#maplat').val()) ;
			var lng = parseFloat($('#maplng').val()) ;
			var center = {lat: lat , lng: lng};
			var zoom = 13 ;
			var markerPoint =new google.maps.LatLng(lat , lng) ;
		}
		else{
			var center = {lat: 26.8206, lng: 30.8025};			
			var zoom = 5 ;
		}
		var marker ;
		var map = new google.maps.Map(document.getElementById('map'), {
		zoom: zoom,
		center: center
		});
		
		var input = document.getElementById('pac-input');
		var searchBox = new google.maps.places.SearchBox(input);
        
		// Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
		
		searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();
          if (places.length == 0) {
            return;
          }

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            
			addMarker(place.geometry.location) ;

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
   
		
		if(typeof markerPoint!='undefined'){
			addMarker(markerPoint) ;
		}
			
		
        map.addListener('click', function(e) {
            var latLng = e.latLng;
			console.log(e.latLng);
            if(map.zoom<9)return;
			addMarker(latLng) ;
        });
		
		
		function addMarker(latLng){
            if(typeof marker =='undefined'){
                marker = new google.maps.Marker({
                  position: latLng,
                  map: map,
                  draggable:true
                });
				marker.addListener('dragend', function(e){
					addMarker(e.latLng) ;
				});
            }else{
                marker.setPosition(latLng);
            }
            $('#maplat').val(latLng.lat());
            $('#maplat').removeAttr('disabled');
            
            $('#maplng').val(latLng.lng());
            $('#maplng').removeAttr('disabled');
			
		}
    }
	$('#pac-input').change(function(){
		var link = $(this).val().search("google");
		if(link!=-1){
			var regex = new RegExp('@(.*),(.*),');
            var lng_lat_match = $(this).val().match(regex);
            var lat = lng_lat_match[1];
            var lng = lng_lat_match[2];
			//alert(1);
            $('#maplat').val(lat);
			$('#maplng').val(lng);	
			initMap();
		}
			
	})
	$(document).keypress(function (e) {
	 if(e.which == 13)  // the enter key code
		return false;  
	}); 
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDRt4t1aVecHVflkaMyDd8Xk3xagSXM8Bg&callback=initMap">
</script>


