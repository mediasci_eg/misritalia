<div>
@isset($size)
<span>({{$size}})</span>
@endisset
<input type='file'  data-name='{{$name}}' class='form-control images' >
<progress style='display: none;' max="100" value="0" data-label=""></progress>

<ul class='preview-container ' data-hasOne=''>
    @if($value !='')
        <li class='image_container' draggable='true' ondragenter='dragenter(event)' ondragstart='dragstart(event)'>
            <a href='#' class='delete_image'>✖</a>
            <input type='hidden' name='{{$name}}' value='{{$value}}' />
            <img style='width:100px;margin:0 3px' src='{{url('uploads/'.$value )}}'>
        </li>
    @endif
</ul>
</div>

@section('footer')
    @include('upload.js')
@stop

