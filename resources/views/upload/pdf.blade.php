<div>
    <input type='file' data-name='{{$name}}' class='form-control pdfs'>
    <input type='hidden' class='folder' value='{{@$folder}}'>

    <progress style='display: none;' max="100" value="0" data-label=""></progress>
    <ul class='preview-container ' data-hasOne=''>
        @if($value !='')
        <li class='image_container' draggable='true' ondragenter='dragenter(event)' ondragstart='dragstart(event)'>
            <a href='#' class='delete_image'>✖</a>
            <input type='hidden' name='{{$name}}' value='{{$value}}' />
            <a target="_blank" href='{{url('brochures/'.$value )}}'>{{$value}}</a>
        </li>
        @endif
    </ul>
</div>
@section('footer')
@include('upload.js')
@stop
