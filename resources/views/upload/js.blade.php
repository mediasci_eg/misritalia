<style>
    progress {
    text-align: center;
    height: 2px;
    width: 100%;
    -webkit-appearance: none;
    border: none;
    
    /* Set the progressbar to relative */
    position:relative;
    }
    progress:before {
    content: attr(data-label);
    font-size: 0.8em;
    vertical-align: 0;
    
    /*Position text over the progress bar */
    position:absolute;
    left:0;
    right:0;
    }
    progress::-webkit-progress-bar {
    background-color: #c9c9c9;
    }
    progress::-webkit-progress-value {
    background-color: #7cc4ff;
    }
    progress::-moz-progress-bar {
    background-color: #7cc4ff;
    }
</style>
<script>
        function uploadImage(selector){
            upload(selector , 'image') ;
        }
        function uploadPdf(selector){
            upload(selector , 'pdf') ;
        }
        function upload(selector , type) {
            if(type == 'image')
                var url = "{{url('backend/upload-image')}}";
            else if(type == 'pdf')
                var url = "{{url('backend/upload-pdf')}}";
            var file_data = selector.prop("files")[0];
            selector.val('');
            var form_data = new FormData();
            form_data.append("file", file_data);
            if(selector.parent().find('.folder').length>0){
                form_data.append("folder", selector.parent().find('.folder').val());
            }
            
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        var percentVal = percentComplete;
 
                        var progress = selector.parent().parent().find("progress");
                        progress.show();
                        progress.val(percentVal);
                        progress.attr('data-label' , percentVal+'%');                        

                        if (percentComplete === 100) {

                        }

                    }
                    }, false);

                    return xhr;
                },
                type: 'post',
                url: url,
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                beforeSend: function () {
                    var progress = selector.parent().parent().find("progress");
                    progress.show();
                    progress.val('0');
                    progress.attr('data-label' , '');

                    $('input[type=submit]').hide();
                },
                complete: function (result) {
                    var progress = selector.parent().parent().find("progress");
                    progress.hide();
                    progress.val('0');
                    var content = $.parseJSON(result.responseText);
                    $('input[type=submit]').show();

                    if (content.status == 0)
                    {
                        alert("Error,can't upload the image");
                        return;
                    }
    
                    var element = selector.parent().find('.preview-container');
    
                    if (typeof selector.attr('data-name') != 'undefined')
                        name = selector.attr('data-name');
                    else
                        name = 'images[]';
                    if(type == 'image')
                        var text = '\n\
                        <li class="image_container" >\n\
                                <a href="#" class="delete_image" >✖</a>\n\
                                <img style="width:100px;margin:0 3px" id=""  src="{{url('uploads')}}/' + content.file + '" />\n\
                                <input type="hidden" name="' + name + '"  value="' + content.file + '" />\n\
                        </li> \n\
                        ';
                    else if(type == 'pdf')
                        var text = '\n\
                        <li class="image_container" >\n\
                                <a href="#" class="delete_image" >✖</a>\n\ \n\
                                <a target="_blank" id=""  href="{{url('brochures')}}/' + content.file + '" >'+ content.file +'</a>\n\
                                <input type="hidden" name="' + name + '"  value="' + content.file + '" />\n\
                        </li> \n\
                        ';

                
                    if (typeof element.attr('data-hasOne') == 'undefined')
                        element.append(text).show();
                    else
                        element.html(text).show();

                        $('#upload-loading').hide();
                        $('input[type=submit]').show();
                }
            });
        }
        $(document).ready(function () {
            //--------------------------
            $(document).on("change", "input:file.images", function () {
                uploadImage($(this));
            });
            $(document).on("change", "input:file.pdfs", function () {
                uploadPdf($(this));
            });
            $(document).on('click', '.delete_image', function (e) {
                e.preventDefault();
                var id = $(this).parent().find('img').attr('id');
                if (id > 0)
                {
                    $('form').append("<input type='hidden' name='deleted_images[]' value='" + id + "' >");
                }
                $(this).parent().remove();
            });
        });
            //--------------------------
    </script>

@push('header')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
    ul.preview-container{
        padding: 0;
    }
    li.image_container img {
        width: 100px !important ;
        height: 66px;
        margin: 10px 2px !important;
        border-radius: 10px;
    }
    li.image_container {
        display: inline-block;
        position: relative;
    }
    li.image_container .delete_image {
        position: absolute;
        bottom: -17px;
        left: 48%;
    }
    .sortable .image_container img {
        cursor: grab;
    }
    </style>
@endpush
    

@push('footer')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script>
    $( function() {
        $( ".sortable" ).sortable();
    } );
    </script>
@endpush



