<div>
@isset($size)
<span>({{$size}})</span>
@endisset
<input type='file'  data-name='{{$name}}' class='form-control images'>
<progress style='display: none;' max="100" value="0" data-label=""></progress>
<ul class='preview-container sortable' >
    @if($value !='')
		@foreach($value as $item)
        <li class='image_container' draggable='true' ondragenter='dragenter(event)' ondragstart='dragstart(event)'>
            <a href='#' class='delete_image'>✖</a>
            <input type='hidden' name='{{$name}}' value='{{$item}}' />
            <img style='width:100px;margin:0 3px' src='{{url('uploads/'.$item )}}'>
        </li>
		@endforeach
    @endif
</ul>
</div>

<style>
	ul.preview-container{
		padding-left:0
	}
	li.image_container{
		display: inline-block;
		list-style: none;
		position: relative;
		padding-right: 5px;
		cursor: move;
	}
	li.image_container .delete_image {
		position: absolute;
		bottom: -20px;
		right: 50%;
	}
	li.image_container img {
		border-radius: 5px;
	}
</style>
@section('footer')
    @include('upload.js')
@stop

