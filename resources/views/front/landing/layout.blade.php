<!DOCTYPE html>
<html lang="en">

<head>
    <base href="{{url('./')}}">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- owl.carousel for this template -->
    <link href="assets/css/owl.carousel.min.css" rel="stylesheet" />
    <link href="assets/css/owl.theme.default.min.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!-- wow animation stylesheet -->
    <!-- Animate CSS -->
    <link rel="stylesheet" href="assets/css/animate.min.css" />

    <!-- library styleSheet -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- custome css  -->
    <link rel="stylesheet" href="assets/stylesheets/misritalia.css" />
 <script src='//code.jquery.com/jquery-3.4.1.min.js'></script>
    <title>Misritalia</title>
    <style>
		form .form-control {
            margin-bottom: 24px;
        }
		form .form-control.error {
            margin-bottom: 0px;
        }

        form .form-group {
            margin-bottom: 10px;
        }

        form .error {
            color: red;
			margin-bottom: 0px;
			font-size: 12px;
        }
        form .btn:hover {
            color: #fff;
        }
		header .nav-item {
		  margin-left: 23px !important;
		}
		.project-inquiry .inquiry-form .back {
		  margin-bottom: 15px !important;
		  margin-top: 15px !important;
		}
		input.form-control {
		  padding: 23px 10px;
		}
		form .form-control {
		  border-radius: 8px;
		  text-transform: uppercase;
		}
		.inquiry-logo {
		  max-width: 100% !important;
		}
		.button-style{
			width: 185px !important;
			height: 50px !important;
			text-align: center;
			line-height: 50px;
			border-radius: 25px;
			text-decoration: none;
			font-size: 14px;
			display: inline-block;
			font-family: bold;
			text-transform: uppercase;
			text-decoration: none!important;
		}
        .button-style:hover {
            border: 2px solid #1c296a !important;
            transition: 0.2s all !important;
            background-color: #fff !important;
            color: #1c296a !important;
        }
	.project-inquiry .inquiry-form, .project-inquiry .img-view{
		height: 100vh;
	}
	.blue_logo{
		display:none ;
	}
	.logo_container{
		position: absolute;
		left: 5%;
	}
	
    @media screen and (max-width: 768px){
        .project-inquiry .img-view {
            height: 650px;
        }
		.project-inquiry .inquiry-form, .project-inquiry .img-view{
			height: 740px;
		}
		.logo_container{
			position:unset;
		}
		.blue_logo{
			display:block;
			margin:auto;
		}
		.white_logo{
			display:none;
		}
    }
	@media screen and (max-width: 480px){		
		.project-inquiry .inquiry-form, .project-inquiry .img-view {
			height: 780px;
		}
	}
	
	@media screen and (max-width: 380px){			
		.project-inquiry .inquiry-form, .project-inquiry .img-view {
			height: 780px;
		}
	}
    </style>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '642131839659960');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=642131839659960&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Global site tag (gtag.js) - Google Ads: 676490036 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-676490036"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'AW-676490036');
    </script>

    @stack('header')
</head>

<body>

    <header class="wow fadeInDown">
        <div class="logo_container" style="">
            <nav class="navbar navbar-expand-lg navbar-light projectQuiry">
				@if(!isset($left_side2))
					<img src="assets/images/Misr%20Italia_Logos%202.png" class='white_logo' style='width:213px' alt />
					<img src="assets/images/blue-logo.png" class='blue_logo' style='width:213px' alt />
				@endif

            </nav>
        </div>
    </header>

    <div class="project-inquiry">
        <div class="row m-0 aligm-items-center">
	
		<div class="col-md-6 p-0 order-1 order-md-2">
			@yield('content')
		</div>
		<div class="col-md-6 p-0 order-2 order-md-1">
			@if(isset($left_side2))
				@include('front.landing.leftside2')
			@else
				@include('front.landing.leftside')
			@endif		

		</div>

		
        </div>
    </div>
    <script src='//code.jquery.com/jquery-3.4.1.min.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js'></script>
    <script>
    jQuery.validator.addMethod("customemail", function(value, element) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return this.optional(element) || re.test(String(value).toLowerCase());
    }, "Please enter a valid email address.");

    jQuery.validator.addMethod("mobile", function(value, element) {
        var re = /01[0-9]{9}/;
        return this.optional(element) || re.test(String(value).toLowerCase());
    }, "Please enter a valid phone number.");


    $("form").validate({
        rules: {
            first_name : 'required',
            email: {
                required: true,
                email: true,
                customemail:true,
            },
            phone: {
                required: true,
                mobile:true,
            },


        }
    });

    </script>
</body>

</html>
