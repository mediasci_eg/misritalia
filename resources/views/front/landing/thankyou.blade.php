@extends('front.landing.layout')
@section('content')
<div class="submitted">
    <div class="inquiry-form d-flex justify-content-center flex-column text-center">
		@if(request()->has('source') && request('source')=='website' )
			<a href="{{$back}}" style='position: absolute;top: 30px;' class="back d-block text-decoration-none text-reset">
				<i class="fas fa-arrow-left mr-3"></i>BACK 
			</a>
		@endif
        <h1>Thank you</h1>
        <p>Your information has been received. We will contact you shortly.</p>
        
    </div>
</div>
@stop

@push('header')
<script>
    fbq('track', 'Contact');
</script>

<!-- Event snippet for Conversions conversion page -->
<script>
    gtag('event', 'conversion', {'send_to': 'AW-676490036/AKLqCM_WpboBELTWycIC'});
</script>

<!--
Event snippet for Conversion code on : Please do not remove.
Place this snippet on pages with events you’re tracking. 
Creation date: 01/15/2020
-->
<script>
    gtag('event', 'conversion', {
      'allow_custom_scripts': true,
      'send_to': 'DC-9864371/invmedia/conve0+unique'
    });
</script>
<noscript>
<img src="https://ad.doubleclick.net/ddm/activity/src=9864371;type=invmedia;cat=conve0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=1;num=1?" width="1" height="1" alt=""/>
</noscript>
<!-- End of event snippet: Please do not remove -->

<!-- 
Start of global snippet: Please do not remove
Place this snippet between the <head> and </head> tags on every page of your site.
-->
<!-- Global site tag (gtag.js) - Google Marketing Platform -->
<script async src="https://www.googletagmanager.com/gtag/js?id=DC-9864371"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'DC-9864371');
</script>
<!-- End of global snippet: Please do not remove -->
@endpush