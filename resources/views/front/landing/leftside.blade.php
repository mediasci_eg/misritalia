<div style='position:relative;background: url("{{url('uploads/'.$development->inquiry_image)}}") 100% 0% no-repeat;background-size:cover' class="img-view d-flex align-items-end">
<div class="over-layer"></div>
    <p class="text-left pb-5  pr-5"  style=' font-size:17px;
'>
        <img class="dev-logo mb-3" style='@if($development->id == '3' ) max-width:350px @endif' src='uploads/{{$development->logo}}'><br>
        {{$development->inquiry_content}}
    </p>
</div>

<style>
.dev-logo{
	max-width: 300px;margin-right: auto;display: block;
}
.over-layer {
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background-color: rgba(14,15,15,.26);
    position: absolute;
}
.project-inquiry .img-view p {
    font-family: "regular";
    position: absolute;
    z-index: 2;
}
@media screen and (max-width: 768px){
	.dev-logo{
		margin: 20% auto;	
	}
}
</style>