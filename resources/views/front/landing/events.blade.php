@extends('front.landing.layout')
@section('content')
  <div class="inquiry-form d-flex justify-content-center flex-column"> 
	@if(request()->has('source') && request('source')=='website' )
		<a href="{{$back}}" class="back d-block text-decoration-none text-reset">
			<i class="fas fa-arrow-left mr-3"></i>BACK 
		</a>
	@endif
    <h1 class="text-center text-md-left">  
	EVENTS
    </h1>
    <form method='POST' action='landing/save' id='inquiry_form'>
      {{ csrf_field() }}
		@if(request()->has('source'))
		  <input type="hidden" value='{{request('source')}}'  name="source" />
		@endif
      
      <input  style='height:
    ' type="hidden" value='{{request('utm_term')}}' name="utm_term" />
      <input  type="hidden" value='{{request('utm_source')}}' name="utm_source" />
      <input type="hidden" value='{{request('utm_campaign')}}' name="utm_campaign" />
      <div class="row p-0">
	  
        <div class="form-group col-lg-6 col-md-12 col-sm-6 col-12">
          <input type="text" class="form-control" placeholder="NAME * " required name="name" />
        </div>
        
        <div class="form-group col-lg-6 col-md-12 col-sm-6 col-12">
          <input type="text" class="form-control" required placeholder="PHONE * " name="phone"
            pattern="01(\d){9}" />
        </div>
		<div class="form-group col-lg-6 col-md-12 col-12">
          <input type="text" class="form-control" id='fullemail' name="email" required placeholder="EMAIL * " />
        </div>
		<div class="form-group col-lg-6 col-md-12 col-sm-6 col-12">
          <input type="text" class="form-control" placeholder="FACEBOOK PROFILE * " required name="facebook_profile" />
        </div>
        
        <div class="col-12">
          <button type="submit" id='submit_button' class="button-style btn d-block ml-auto" style="line-height:3px">SUBMIT</button>
        </div>
      </div>
    </form>
  </div>

<script>
$(document).ready(function(){
	$('#inquiry_form').on('submit',function(){
		if($(this).valid()) 
			$('#submit_button').attr("disabled", true);
	});
});
</script>

@endsection
