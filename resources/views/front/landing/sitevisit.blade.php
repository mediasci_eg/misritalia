
<!DOCTYPE html>
<html lang="en">

<head>
    <base href="https://sitevisit.misritaliaproperties.com/.">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- owl.carousel for this template -->
    <link href="assets/css/owl.carousel.min.css" rel="stylesheet" />
    <link href="assets/css/owl.theme.default.min.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!-- wow animation stylesheet -->
    <!-- Animate CSS -->
    <link rel="stylesheet" href="assets/css/animate.min.css" />

    <!-- library styleSheet -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- custome css  -->
    <link rel="stylesheet" href="assets/stylesheets/misritalia.css" />
 <script src='//code.jquery.com/jquery-3.4.1.min.js'></script>
    <title>Misritalia</title>
    <style>
		form .form-control {
            margin-bottom: 24px;
        }
		form .form-control.error {
            margin-bottom: 0px;
        }

        form .form-group {
            margin-bottom: 10px;
        }

        form .error {
            color: red;
			margin-bottom: 0px;
			font-size: 12px;
        }
        form .btn:hover {
            color: #fff;
        }
		header .nav-item {
		  margin-left: 23px !important;
		}
		.project-inquiry .inquiry-form .back {
		  margin-bottom: 15px !important;
		  margin-top: 15px !important;
		}
		input.form-control {
		  padding: 23px 10px;
		}
		form .form-control {
		  border-radius: 8px;
		  text-transform: uppercase;
		}
		.inquiry-logo {
		  max-width: 100% !important;
		}
		.button-style{
			width: 185px !important;
			height: 50px !important;
			text-align: center;
			line-height: 50px;
			border-radius: 25px;
			text-decoration: none;
			font-size: 14px;
			display: inline-block;
			font-family: bold;
			text-transform: uppercase;
			text-decoration: none!important;
		}
        .button-style:hover {
            border: 2px solid #1c296a !important;
            transition: 0.2s all !important;
            background-color: #fff !important;
            color: #1c296a !important;
        }
	.project-inquiry .inquiry-form, .project-inquiry .img-view{
		height: 100vh;
	}
	.blue_logo{
		display:none ;
	}
	.logo_container{
		position: absolute;
		left: 5%;
    }
    .dev-logo-container{
        height: 135px;
    }
	.dev-logo{
        max-width: 90%;
        display: block;
    }
    .over-layer {
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        background-color: rgba(14,15,15,.26);
        position: absolute;
    }
    .project-inquiry .img-view p {
        font-family: "regular";
        position: absolute;
        z-index: 2;
    }

    @media  screen and (max-width: 768px){
        /* .dev-logo{
                margin: 20% auto;	
        } */
        .project-inquiry .img-view {
            height: 650px;
        }
		.project-inquiry .inquiry-form, .project-inquiry .img-view{
			height: 740px;
		}
		.logo_container{
			position:unset;
		}
		.blue_logo{
			display:block;
			margin:auto;
		}
		.white_logo{
			display:none;
		}
    }
	@media  screen and (max-width: 480px){		
		.project-inquiry .inquiry-form, .project-inquiry .img-view {
			height: 780px;
		}
	}
	
	@media  screen and (max-width: 380px){			
		.project-inquiry .inquiry-form, .project-inquiry .img-view {
			height: 780px;
		}
	}
    </style>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '642131839659960');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=642131839659960&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Global site tag (gtag.js) - Google Ads: 676490036 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-676490036"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'AW-676490036');
    </script>

    </head>

<body>

    <header class="wow fadeInDown">
        <div class="logo_container" style="">
            <nav class="navbar navbar-expand-lg navbar-light projectQuiry">
                <img src="assets/images/Misr%20Italia_Logos%202.png" class='white_logo' style='width:213px' alt />
                <img src="assets/images/blue-logo.png" class='blue_logo' style='width:213px' alt />
            </nav>
        </div>
    </header>

    <div class="project-inquiry">
        <div class="row m-0 aligm-items-center">
            <div class="col-lg-6 p-0 mb-3 mb-lg-0">
                <div style='position:relative; background: #1c296a; padding:10% 5% 5%;' class="h-100 d-flex flex-column justify-content-center ">
                    <div class="py-3 bg-adanger row">
						@foreach($projects as $development)
							<div class="col-sm-4 col-6  ">
								<div class="dev-logo-container mb-3 d-flex flex-column justify-content-around">
									<img class="dev-logo " style='' src='uploads/{{$development->logo}}'>
								</div>
							</div>
						@endforeach
                        
                    </div>
                    <p class="text-center text-lg-left pr-5"  style=' font-size:17px;'> Let the Site Speak! IL BOSCO New Capital is delivering 2020, a year ahead of schedule, as construction progresses impressively. A revolutionary concept built on the philosophy of encapsulating nature within city life, IL BOSCO creates a true ecological experience. The lifestyle allows for a free force of organic living spread upon the urban topography giving variable aspects to be integrated within the community in form of energy.
                    </p>
                </div>
            </div>
            <div class="col-lg-6 p-0">
                <div class="inquiry-form d-flex justify-content-center flex-column">
                    <h1 class="text-center text-md-left">SITE
                        <br class="d-none d-md-inline" />VISIT
                    </h1>
                    <form method='POST' action='landing/save' id='inquiry_form'>
                        {{ csrf_field() }}
                        <input type="hidden" value="{{request('utm_term')}}" name="utm_term" />
                        <input type="hidden" value="{{request('utm_source')}}" name="utm_source" />
                        <input type="hidden" value="{{request('utm_campaign')}}" name="utm_campaign" />
                        <div class="row p-0">
                            <div class="form-group col-lg-6 col-md-12 col-sm-6 col-12">
                                <input type="text" class="form-control" placeholder="FIRST NAME * " required name="first_name" />
                            </div>
                            <div class="form-group col-lg-6 col-md-12 col-sm-6 col-12">
                                <input type="text" class="form-control" name="last_name" required placeholder="LAST NAME * " />
                            </div>
                            <div class="form-group col-lg-6 col-md-12 col-sm-6 col-12">
                                <input type="text" class="form-control" id='fullemail' name="email" required placeholder="EMAIL * " />
                            </div>
                            <div class="form-group col-lg-6 col-md-12 col-sm-6 col-12">
                                <input type="text" class="form-control" required placeholder="PHONE * " name="phone"
                                    pattern="01(\d){9}" />
                            </div>
                            <div class="form-group col-lg-6 col-md-12 col-sm-6 col-12">
                                <label for="cars">Choose Project:</label>
                                <select name='development_id' class="form-control">
                                    <option value="">All Projects</option>
                                    
                                    <option value="1">VINCI</option>
                                    
                                    <option value="2">IL BOSCO</option>
                                    
                                    <option value="3">IL BOSCO CITY</option>
                                    
                                    <option value="4">CAIRO BUSINESS PARK</option>
                                    
                                    <option value="5">GARDEN 8</option>
                                    
                                    <option value="6">KAI SOKHNA</option>
                                    
                                    <option value="7">VINCI STREET</option>
                                    
                                    <option value="8">LA NUOVA VISTA</option>
                                    
                                    <option value="12">VERTICAL FOREST</option>
                                    
                                </select>
                            </div>
                            <div class="col-12 form-group ">
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="message"
                                    placeholder="INTERESTED IN"></textarea>
                            </div>
                            <div class="col-12">
                                <button type="submit" id='submit_button' class="button-style btn d-block ml-auto"
                                    style="line-height:3px">SUBMIT</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

<script>
    $(document).ready(function(){
	$('#inquiry_form').on('submit',function(){
		if($(this).valid())
			$('#submit_button').attr("disabled", true);
	});
});
</script>
    <script src='//code.jquery.com/jquery-3.4.1.min.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js'></script>
    <script>
    jQuery.validator.addMethod("customemail", function(value, element) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return this.optional(element) || re.test(String(value).toLowerCase());
    }, "Please enter a valid email address.");

    jQuery.validator.addMethod("mobile", function(value, element) {
        var re = /01[0-9]{9}/;
        return this.optional(element) || re.test(String(value).toLowerCase());
    }, "Please enter a valid phone number.");


    $("form").validate({
        rules: {
            first_name : 'required',
            email: {
                required: true,
                email: true,
                customemail:true,
            },
            phone: {
                required: true,
                mobile:true,
            },


        }
    });

    </script>
</body>

</html>