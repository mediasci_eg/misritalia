-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2019 at 03:27 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `misritalia`
--

-- --------------------------------------------------------

--
-- Table structure for table `development_brochures`
--

CREATE TABLE `development_brochures` (
  `id` int(11) NOT NULL,
  `development_id` int(11) NOT NULL,
  `brochure` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `development_brochures`
--

INSERT INTO `development_brochures` (`id`, `development_id`, `brochure`, `thumbnail`, `created_at`, `updated_at`) VALUES
(16, 1, '410cf9859883b1a5966ee58be3105724.pdf', NULL, '2019-12-24 09:26:20', '2019-12-24 09:26:20'),
(17, 5, 'c9e4e238f1ad79733a4e65dea7d89408.pdf', NULL, '2019-12-24 10:31:27', '2019-12-24 10:31:27'),
(18, 6, '7d93bc2a0dabd378076cd67c80b5351f.pdf', NULL, '2019-12-24 10:36:46', '2019-12-24 10:36:46'),
(19, 7, 'dcb949232ea7513201efb5fc5a855646.pdf', NULL, '2019-12-24 10:41:40', '2019-12-24 10:41:40'),
(20, 7, '9f6dcf490d1efa099e6b31fdc1cb35c6.pdf', NULL, '2019-12-24 10:41:40', '2019-12-24 10:41:40'),
(21, 7, 'bfa78b77eeac9a190f30c27a2c527c05.pdf', NULL, '2019-12-24 10:41:40', '2019-12-24 10:41:40'),
(22, 7, '8563462fa4ea1a4ee188e8e930e2c254.pdf', NULL, '2019-12-24 10:41:40', '2019-12-24 10:41:40'),
(23, 7, 'd8c144ea379dad5438f6c6277642de81.pdf', NULL, '2019-12-24 10:41:40', '2019-12-24 10:41:40'),
(24, 7, '7eb4aab4d7446bd63badc1140ae3a53a.pdf', NULL, '2019-12-24 10:41:40', '2019-12-24 10:41:40'),
(25, 7, 'f43a1eaf8f250f702da0c03ca6c89dbc.pdf', NULL, '2019-12-24 10:41:40', '2019-12-24 10:41:40'),
(26, 8, '073a2b34b29a32086bf04ced790b6907.pdf', NULL, '2019-12-24 10:47:09', '2019-12-24 10:47:09'),
(27, 8, '514a7beb2b11c5914914a830df8d60c4.pdf', NULL, '2019-12-24 10:47:09', '2019-12-24 10:47:09'),
(28, 9, 'dfe5d2624adf22ef6dcbee37edfddef0.pdf', NULL, '2019-12-24 10:52:12', '2019-12-24 10:52:12'),
(29, 9, '3fb827f5133334584ecbfaf0c4985d13.pdf', NULL, '2019-12-24 10:52:12', '2019-12-24 10:52:12'),
(30, 9, '316b7691f91196d8969eb772a2054a71.pdf', NULL, '2019-12-24 10:52:12', '2019-12-24 10:52:12'),
(31, 9, 'f80d5fef160a067add72d5a3321b3155.pdf', NULL, '2019-12-24 10:52:12', '2019-12-24 10:52:12'),
(32, 11, 'a076d37ea4e2593ddb703f4693e0bb63.pdf', NULL, '2019-12-24 11:06:17', '2019-12-24 11:06:17'),
(33, 11, 'e54f11e6761d3805da7c9a8a7e4e9d55.pdf', NULL, '2019-12-24 11:06:17', '2019-12-24 11:06:17'),
(34, 11, '3e94f7243a8b5b5750897f8b673d4143.pdf', NULL, '2019-12-24 11:06:17', '2019-12-24 11:06:17'),
(35, 11, '6ff1fbd6c8a81fd1126edc7baf6469be.pdf', NULL, '2019-12-24 11:06:17', '2019-12-24 11:06:17'),
(36, 11, '1bbca9f84fd384bf8aebcc3ccaf6ce63.pdf', NULL, '2019-12-24 11:06:17', '2019-12-24 11:06:17'),
(37, 11, 'cab27064ab5ef94309f248aca05d1fd5.pdf', NULL, '2019-12-24 11:06:17', '2019-12-24 11:06:17'),
(38, 11, '393e5b59ca089141f807cb8e6d48adee.pdf', NULL, '2019-12-24 11:06:17', '2019-12-24 11:06:17'),
(39, 11, 'a7ec69cf4ef4691a6a20aa50cfd5db81.pdf', NULL, '2019-12-24 11:06:17', '2019-12-24 11:06:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `development_brochures`
--
ALTER TABLE `development_brochures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `development_brochures_development_id_foreign` (`development_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `development_brochures`
--
ALTER TABLE `development_brochures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `development_brochures`
--
ALTER TABLE `development_brochures`
  ADD CONSTRAINT `development_brochures_development_id_foreign` FOREIGN KEY (`development_id`) REFERENCES `developments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
