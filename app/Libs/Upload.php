<?php

namespace App\Libs;

class Upload {

    static function image($config) {
        return view('upload.image', $config)->render();
    }

    static function images($config) {
        return view('upload.images', $config)->render();
    }

    static function pdf($config) {
        return view('upload.pdf', $config)->render();
    }

    static function pdfs($config) {
        return view('upload.pdfs', $config)->render();
    }

}
