<?php
namespace App\Libs;
class CountryIP {

    static function get_IP() {
        $ip = $_SERVER['REMOTE_ADDR'];
        $ip = '156.223.51.10';

        $countryInfo = self::getContent('http://api.ipstack.com/'.$ip.'?access_key=1fd73f7c359d498b988a45479e686ece');
        if ($countryInfo) {
            return $countryInfo['ip'];
        } else {
            $countryInfo = self::getContent('http://www.geoplugin.net/php.gp?ip=' . $ip);
            if ($countryInfo) {
                $countryInfo['ip'] = $countryInfo['geoplugin_countryCode'];
                return $countryInfo['ip'];
            }
        }
    }

    static function get_Symbol() {
        $ip = $_SERVER['REMOTE_ADDR'];
        //$ip = '5.1.47.0'; //jordon
        //$ip = '5.11.40.0'; //palastine
        // $ip="34.248.100.250"; //irelandland
        //  $ip="163.121.103.122"; //egypt
		//$ip="212.138.92.10"; //KSA
        // $ip="83.110.250.231"; //UAE
        $countryInfo = self::getContent('http://api.ipstack.com/'.$ip.'?access_key=1fd73f7c359d498b988a45479e686ece');
        if ($countryInfo) {
            return $countryInfo['country_code'];
        } else {
            $data = self::getContent('http://www.geoplugin.net/php.gp?ip=' . $ip);
            if ($data) {
                $countryInfo['country_code'] = $countryInfo['geoplugin_countryCode'];
                return $countryInfo['country_code'];
            } else {
                $countryInfo['country_code'] = "EG";
                return $countryInfo['country_code'];
            }
        }
    }

    static function get_Name() {
        $ip = $_SERVER['REMOTE_ADDR'];
        $countryInfo = self::getContent('http://api.ipstack.com/'.$ip.'?access_key=1fd73f7c359d498b988a45479e686ece');
        if ($countryInfo) {
            return $countryInfo['country_name'];
        } else {
            $data = self::getContent('http://www.geoplugin.net/php.gp?ip=' . $ip);
            if ($data) {
                $countryInfo['country_name'] = $countryInfo['geoplugin_countryName'];
                return $countryInfo['country_name'];
            } else {
                $countryInfo['country_name'] = "EGYPT";
                return $countryInfo['country_name'];
            }
        }
    }

    static function getContent($url) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 2); //timeout in seconds
        $data = curl_exec($curl);
        curl_close($curl);
        if ($data != '')
            return json_decode($data, true);
        else
            return false;
    }

}
