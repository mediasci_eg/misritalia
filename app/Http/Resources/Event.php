<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Event extends JsonResource
{

    public function toArray($request)
    {
        $imagesList = $this->images;

        foreach ($imagesList as $image) {
            $list[] = $image = url('/uploads/' . $image);
        }
        return [
            'id' => $this->id,
            'title' => $this->title,
            'sub_title' => $this->sub_title,
            'content' => $this->content,
            'date' => $this->date,
            'images' => $list,
            'image' => url('/uploads/' . $this->image),
            'cover_image' => url('/uploads/' . $this->cover_image),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
