<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BoxOfHope;

class BoxOfHopeController extends Controller
{
    public function index()
    {
		$data = BoxOfHope::where('id' ,1)->with('volumes')->first();
        $data->volumes = $data->volumes->map(function($q){
            $images = [] ;
            if(is_array($q->images)){
                foreach($q->images as $image){
                    $images[] = url('uploads/' .$image ) ;
                }
            }
            $q->images =  $images ;
            return $q->only(['id' , 'title' , 'description' , 'date' , 'images']) ;
        });
        return response()->json($data);
    }
}
