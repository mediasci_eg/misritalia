<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\News as NewsResource;
use App\Models\Base\VacanciesApply;
use App\Models\News;
use App\Models\Vacancy;
use Illuminate\Support\Facades\Validator;

class VacancyController extends Controller
{
    public function index()
    {
        $vacancies = Vacancy::where('is_current', 1)->get();
        return  response()->json($vacancies);
    }
    public function details($id)
    {
        $vacancy = Vacancy::where('id', $id)->first();
        return  response()->json($vacancy);
    }
    public function apply(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'phone' => 'required|min:9',
            'email' => 'required|email',
            'cv' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['msg', $validator->errors()]);
        }
        $vacancy = new VacanciesApply;
        $vacancy->fill(request()->all());
        if (request()->hasFile('cv') && request()->cv != null) {
            $pdf = $this->imageUploadbrochure(request('cv'));
            if ($pdf)
                $vacancy->cv = $pdf;
            else
                return response()->json(['msg', 'this file type is not supported']);
        }
        $vacancy->save();
        return  response()->json($vacancy);
    }
}