<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helpers\Youtube;
use App\Models\MessageOfHope;

class MessageOfHopeController extends Controller
{
    public function index()
    {
        $data = MessageOfHope::where('id' ,1)->with('tabs')->first();
        $videos = [] ;
        if(is_array($data->videos)){
            foreach($data->videos as $video){
                $videos[] = Youtube::getId($video) ;
            }
        }

        $data->videos = $videos ;
        $data->tabs = $data->tabs->map(function($q){
            $images = [] ;
            if(is_array($q->images)){
                foreach($q->images as $image){
                    $images[] = url('uploads/' .$image ) ;
                }
            }
            $q->images =  $images ;
            return $q->only(['id' , 'title' , 'description' , 'date' , 'images']) ;
        });
        return response()->json($data);
    }
}
