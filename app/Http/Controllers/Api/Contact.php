<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ContactMail;
use App\Mail\Whistleblowing;
use App\Models\ContactUs ;
use App\Models\FormWhistleblowing;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class Contact extends Controller {

    public function index(Request $request) {

		 $rules = [
            'name' => 'required',
			'phone' => 'required',
            'development_id' => 'required',
            //'request_type' => 'required',
            'inquiry_type' => 'required',
            'is_homeowner' => 'required',
            'email' => 'required',
        ];

		$v = \Validator::make($request->all(), $rules);

		if ($v->fails() || !empty($this->errors)) {
            $errors = $v->errors()->toArray();
			$data['status'] = 'failed' ;
			$data['error'] = end($errors)[0] ;
			return response()->json($data);
            return false;
        }

		$contact = New ContactUs ;
		$contact->fill(request()->all());
		$contact->save();
		$data['status'] = 'success' ;
        $data['id'] = $contact->id ;


        Mail::to('customer.care@misritaliaproperties.com')
        ->cc('misritaliaproperties@gmail.com')
        ->cc('hossam.ahmed@media-sci.com')
        ->cc('ahmed.darwish@media-sci.com')
        ->send(new ContactMail($contact));

		return response()->json($data);

    }

}
