<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Development;

class Developments extends Controller
{

    public function all()
    {
        $developments = Development::select('id', 'title', 'logo', 'slug', 'image')
            ->orderBy('sort')
            ->get()->map(function ($item) {
                $item->image = url('uploads/' . $item->image);
                if ($item->logo == '')
                    $item->logo = url('uploads/place-logo.png');
                else
                    $item->logo = url('uploads/' . $item->logo);
                return $item->toArray();
            });
        return response()->json($developments);
    }
    public function index()
    {
        $developments = Development::select('id', 'title', 'logo', 'slug', 'image')
            ->orderBy('sort')
            ->get()->chunk(3)->map(function ($sub) {

                $sub->map(function ($item) {
                    $item->image = url('uploads/' . $item->image);
                    if ($item->logo == '')
                        $item->logo = url('uploads/place-logo.png');
                    else
                        $item->logo = url('uploads/' . $item->logo);
                });

                return array_values($sub->toArray());
            });

        return response()->json($developments);
    }

    public function home()
    {
        $developments = Development::select('id', 'title', 'home_image', 'slug', 'overview_content')
            ->where('home_image', '!=', '')
            ->orderBy('sort')->get()->map(function ($item) {
                $item->home_image = url('uploads/' . $item->home_image);
                $item->title_capitalized = \Str::title($item->title);
                return $item;
            });
        return response()->json($developments);
    }
}
