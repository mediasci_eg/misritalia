<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helpers\Youtube;
use App\Models\Development;
use App\Models\DevelopmentSlider;
use App\Models\DevelopmentBrochure;

class DevelopmentsDetails extends Controller {

    private $images = [
        'logo' ,
        'cover_image' ,
        'home_image' ,
        'overview_image' ,
        'location_image' ,
        'design_image' ,
        'masterplan' ,
    ] ;
    public function index($id) {
        $developments = Development::with('slider');
		if(is_numeric($id))
			$developments = $developments->where('id', $id) ;
		else
			$developments = $developments->where('slug', $id) ;
		$developments = $developments->first();

        foreach($this->images as $image){
            if($developments->{$image} !='')
                $developments->{$image} = url('uploads/'.$developments->{$image});
			else
				$developments->{$image} = '' ;
        }

        $developments->video_id = ($developments->video != '')?Youtube::getId($developments->video) : '' ;
		$developments->overview_video = ($developments->overview_video != '')?Youtube::getId($developments->overview_video) : '' ;
        $developments->link = 'http://'.$developments->slug.'.misritaliaproperties.com?source=website' ;

        foreach($developments->slider as $k=>$slider){
            $developments->slider[$k]->image = url('uploads/' .$developments->slider[$k]->image);
        }
        $this->handleNext($developments) ;
        $this->handleBrochured($developments) ;
        $this->handleUnitTypes($developments) ;

        //dd($developments);
        return response()->json($developments->toArray());
    }

    function handleBrochured(&$developments){
        $result = [
            'general'=>[],
            'villa'=>[],
            'apartment'=>[],
            'chalets'=>[],
        ];
        $developments->brochures_active = '' ;
        foreach($developments->brochures as $k=>$brochure){
            $developments->brochures[$k]->brochure = str_replace('ilbosco' , $developments->slug ,url('brochures/'.$developments->brochures[$k]->brochure)) .'#zoom=50' ;
            //$developments->brochures[$k]->brochure = url('brochures/'.$developments->brochures[$k]->brochure) ;
        }
        foreach($developments->brochures as $brochure){
            $result[$brochure->type][] = $brochure ;
        }
        foreach($result as $k=>$item){
            if(!empty($item)){
                $developments->brochures_active = $k ;
                break ;
            }
        }
        unset($developments->brochures) ;
        $developments->brochures =  $result ;

    }

    function handleUnitTypes(&$developments){
        foreach($developments->unitTypes as $k=>$type){
            $developments->unitTypes[$k]->image = url('uploads/'.$developments->unitTypes[$k]->image) ;
        }
    }

    function handleNext(&$developments){
        $next = Development::select('id' ,'slug', 'cover_image','title' , 'image')
            ->where('sort', '>', $developments->sort)
			->orderBy('sort')
			->first();

        if(!$next){
            $next = Development::select('id' , 'slug' ,'cover_image' , 'title' , 'image')->orderBy('sort')->first();
        }
        $next->image = url('uploads/'.$next->image);
        $next->cover_image = url('uploads/'.$next->cover_image);
        $developments->next = $next ;
    }


}
