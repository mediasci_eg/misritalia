<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LetsGrow;

class letsGrowController extends Controller
{
    public function index()
    {
        $data = LetsGrow::where('id', 1)->with('tabs', 'tabs.tabs')->first();
        foreach ($data->tabs as $tab) {

            foreach ($tab->tabs as $subtab) {
                $images = [] ;
                if(is_array($subtab->images))
                foreach ($subtab->images as $image) {
                    $images[] = url('uploads/' . $image);
                }
                $subtab->images =  $images;
            }
        }

        return response()->json($data);
    }
}
