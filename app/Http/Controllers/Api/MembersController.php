<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Member;

class MembersController  extends Controller
{
    public function index($team)
    {
        $data['members'] = Member::where('team' , $team)
        ->orderBy('sort')
        ->get()->map(function($q){
			$q->image = url('uploads/'.$q->image);
			$q->position = nl2br($q->position);
			return $q ;
		});
        return  response()->json($data);
    }
}
