<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BoxOfHope;
use App\Models\BusinessTransformation;

class BusinessTransformationController extends Controller
{
    public function index()
    {
        $data = BusinessTransformation::where('id' ,1)->with('tabs')->get()->map(function($q){
            $q->tabs = $q->tabs->map(function($q){
                if(is_array($q->files)){
                    $files = [] ;
                    foreach($q->files as $file){
                        $files[] = url('brochures/'.$file) ;
                    }
                    $q->files = $files ;
                }
				$q->content = str_replace('./uploads' , url('uploads/') , $q->content) ;

                $q =  $q->only('title' ,'content' , 'files');

                return $q ;
            });
            return $q->only('description' , 'tabs');
        });
        return response()->json($data);
    }
}
