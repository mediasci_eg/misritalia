<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Inspiration;

class InspirationController extends Controller
{
    public function index()
    {
        $data = Inspiration::where('id', 1)->first();
		$data->brochure = url('brochures/' . $data->brochure); ;
        return response()->json($data);
    }
}
