<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Development;
use App\Models\Slider;

class Homepage extends Controller {

    public function index() {
        $developments = Development::where('is_in_homepage', 1)->select('id','slug', 'title', 'image')->get();
        return response()->json($developments);
    }
    
    public function slider() {
        $slider = Slider::orderBy('sort')->get();
        foreach($slider as $image){
            $image->image = url('uploads/'.$image->image) ;
        }
        return response()->json($slider);
    }

}
