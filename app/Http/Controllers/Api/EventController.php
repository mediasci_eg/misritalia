<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Event as EventResource;
use App\Models\Event;

class EventController extends Controller
{
    public function index()
    {
        return EventResource::collection(Event::all());
    }
    public function details($id)
    {
        $data['event'] = new EventResource(Event::find($id));
        $data['related_events']  = Event::where('id', '!=', $id)->get()->map(function($q){
			$q->image = url('uploads/'.$q->image);
			$q->cover_image = url('uploads/'.$q->cover_image);
			return $q ;
		});
        return  response()->json($data);
    }
}
