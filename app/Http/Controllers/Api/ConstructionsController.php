<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helpers\Youtube;
use App\Models\Construction;

class ConstructionsController extends Controller
{
    public function index()
    {
		$data = Construction::all()->map(function ($item) {
                $item->video_id = Youtube::getId($item->video) ;
                $item->title = $item->development->title;
				unset($item->development);
                return $item ;
            });
        return response()->json($data);
    }
}
