<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FrameworkStructure;
use Illuminate\Support\Facades\Validator;

class FrameworkStructureController extends Controller
{
    public function index(Request $request)
    {
        $data = FrameworkStructure::all()->map(function($q){
			$q->content = str_replace('./uploads' , url('uploads/') , $q->content) ;
            return $q->only('id' , 'title' , 'content' , 'show_more');
        });
        return  response()->json($data);
    }
}
