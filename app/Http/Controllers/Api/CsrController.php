<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Csr;
use App\Models\CsrPartner;

class CsrController extends Controller
{

    public function index()
    {
        $csr = Csr::whereNull('parent_id')->with('children')
            ->get()->map(function ($item) {
                $item->image = url('uploads/' . $item->image);
                if($item->children->isNotEmpty()){
                    $item->children = $item->children->map(function($item){
                        $item->image = url('uploads/' . $item->image);

                        return $item;
                    });
                }
                return $item;
            });
        return response()->json($csr);
    }


    public function partners(){
        $partners = CsrPartner::all()->map(function($item){
            $item->logo = url('uploads/' . $item->logo);
            return $item ;
        });
        return response()->json($partners);
    }

}
