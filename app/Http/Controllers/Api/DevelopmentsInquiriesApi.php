<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DevelopmentsInquiries;

class DevelopmentsInquiriesApi extends Controller
{

    public function index(Request $request)
    {
        //         $developments_inquiries->fill($request->get('params'));

        $developments_inquiries = new DevelopmentsInquiries;

        $developments_inquiries->fill($request->get('params'));
        
        if (!$developments_inquiries->validate()) {
            // session()->flash('errors', $developments_inquiries->errors());
            return response()->json($developments_inquiries->errors());
        }
        $developments_inquiries->save();

        return response()->json(['succses' => 'succses']);
    }
}
