<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CorporateGovernance;

class CorporateGovernanceController extends Controller
{
    public function index()
    {
        $data = CorporateGovernance::where('id', 1)->first();
		$files = [] ;
		$i=0;
		foreach($data->crisis_management_files as $row) {
			$files[$i] = $row;
			$files[$i]['file'] = url('brochures/' . @$row['file']);
			$i++;
		}
		$data->crisis_management_files = $files ;
        return response()->json($data);
    }
}
