<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\News as NewsResource;
use App\Models\News;

class NewsController extends Controller
{
    public function index()
    {
        $data['news'] = $news = News::all()->map(function($q){
			$q->image = url('uploads/'.$q->image);
			return $q ;
		});
        $news[0]->is_main = 1;
        return  response()->json($data);
        // return NewsResource::collection(News::all());
    }
    public function details($id)
    {
        $data['news'] = News::where('id', $id)->first();
		$data['news']->image = url('uploads/'.$data['news']->image);
		$data['news']->content = str_replace('./uploads' , url('uploads/') , $data['news']->content) ;
        $data['related_news']  = News::where('id', '!=', $id)->get()->map(function($q){
			$q->image = url('uploads/'.$q->image);
			return $q ;
		});;
        return  response()->json($data);
        // return new NewsResource(News::find($id));
    }
}
