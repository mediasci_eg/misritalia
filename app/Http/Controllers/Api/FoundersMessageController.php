<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FoundersMessage;

class FoundersMessageController extends Controller
{
    public function index()
    {
        $data = FoundersMessage::where('id' ,1)->first();
        //$data = $data->only('content') ;
        $data->content = nl2br($data->content) ;

        return response()->json($data);
    }
}
