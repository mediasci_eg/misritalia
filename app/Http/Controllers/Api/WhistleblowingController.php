<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FormWhistleblowing;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\Whistleblowing ;

class WhistleblowingController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subject' => 'required',
            'data' => 'required|json',
        ]);
        if ($validator->fails()) {
            return response()->json(['msg', $validator->errors()]);
        }
        $formWhistleblowing = new FormWhistleblowing;
        $formWhistleblowing->subject = $request->subject ;
        $formWhistleblowing->data = json_decode($request->data , true) ;

        if (request()->hasFile('file')) {
            $pdf = $this->imageUploadbrochure(request('file'));
            if ($pdf)
                $formWhistleblowing->files = $pdf;
            else
                return response()->json(['msg', 'this file type is not supported']);
        }else{
			    $formWhistleblowing->files = '';
		}
        $formWhistleblowing->save();
        $data['status'] = 'success';

        Mail::to('speakout@misritaliaproperties.com')
        ->cc('misritaliaproperties@gmail.com')
		->cc('hossam.ahmed@media-sci.com')
        ->cc('ahmed.darwish@media-sci.com')
        ->send(new Whistleblowing($formWhistleblowing));

        return  response()->json($data);
    }
}
