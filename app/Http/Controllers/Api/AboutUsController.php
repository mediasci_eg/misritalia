<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AboutUs;

class AboutUsController extends Controller
{
    public function index()
    {
        $data = AboutUs::where('id', 1)->first();
		$files = [] ;
		$i=0;
		foreach($data->newsletter_files as $row) {
			$files[$i] = $row;
			$files[$i]['file'] = url('brochures/' . @$row['file']);
			$i++;
		}
		$data->newsletter_files = $files ;
        return response()->json($data);
    }
}
