<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ContactPage;
use App\Models\CorporateGovernance;

class ContactPageController extends Controller
{
    public function index()
    {
        $data['showrooms'] =  ContactPage::where('type' , 'showrooms')->get();
        $data['support'] =  ContactPage::where('type' , 'support')->get();

        return response()->json($data);
    }
}
