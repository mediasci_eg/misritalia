<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Partner;

class PartnersController extends Controller
{
    public function index()
    {
		$data = Partner::orderBy('sort')->get()->map(function($q){
            $q->image = url('uploads/' .$q->image ) ;
            return $q ;
        });
        return response()->json($data);
    }
}
