 
<textarea name="{name}" id="{editor_id}" style="height: 500px">{html}</textarea>


<script>
    $(function(){
      $('#{editor_id}').froalaEditor({
       // imageUploadURL: '{upload_url}',
        //toolbarSticky: false,
        height:400,
        //imageManagerLoadURL:'./admin/ajax/browse',

//        imageUploadParams: {
//          id: 'my_editor'
//        }
      })
      .on('froalaEditor.image.beforeUpload', function (e, editor, files) {
          if (files.length) {
            var reader = new FileReader();
            reader.onload = function (e) {
              var result = e.target.result;

              editor.image.insert(result, null, null, editor.image.get());
            };

            reader.readAsDataURL(files[0]);
          }

          return false;
        });
      /*
        .on('froalaEditor.image.removed', function (e, editor, $img) {
            $.ajax({
              // Request method.
              method: "POST",

              // Request URL.
              url: "{remove_url}",

              // Request params.
              data: {
                src: $img.attr('src')
              }
            })
            .done (function (data) {
              console.log ('image was deleted');
            })
            .fail (function () {
              console.log ('image delete problem');
            })
      });
      */
        $('#{editor_id}').on('froalaEditor.contentChanged', function (e, editor) {
            // Do something here.
            $(window).trigger('resize');
            //$(window).scrollTop( $(window).height()+10);
        });
      
      
    });
  </script>
  

