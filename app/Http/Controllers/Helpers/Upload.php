<?php

namespace App\Http\Controllers\Helpers ;

class Upload 
{
    static function image($config){
        return view('upload.image' , $config )->render();
    }
    
    static function images($config){
        return view('upload.images' , $config )->render();
    }
}

