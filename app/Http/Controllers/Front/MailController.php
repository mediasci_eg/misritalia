<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Development;
use App\Models\DevelopmentsInquiries;
use Illuminate\Http\Request;

class MailController extends Controller {
    
	public function index() {
		if(!request()->has('mail')){
			?>
			<form>
				<input type='text' name='mail'>
				<input type='submit' value='Send'>
			</form>
			
			<?php
			die ;
		}
        $transport = (new \Swift_SmtpTransport('localhost', 25));

		
		//You could alternatively use a different transport such as Sendmail:

		// Sendmail
		//$transport = new \Swift_SendmailTransport('/usr/sbin/sendmail -bs');
		

		// Create the Mailer using your created Transport
		$mailer = new \Swift_Mailer($transport);

		// Create a message
		$message = (new \Swift_Message('Test Mail Subject'))
		  ->setFrom(['noreply@misritaliaproperties.com' => 'misritaliaproperties'])
		  ->setTo([request('mail')])
		  ->setBody('Here is the message itself')
		  ;

		// Send the message
		$result = $mailer->send($message);
		dd($result);
    }
    
}
