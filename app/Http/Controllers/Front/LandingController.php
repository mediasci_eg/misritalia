<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Development;
use App\Models\DevelopmentsInquiries;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    private $sources = [
        'google' => '14',
        'youtube' => '18',
        'gdn' => '17',
        'programmatic' => '19',
        'facebook' => '16'
    ];
    public function index($slug)
    {
        $data['development'] = $development = Development::whereSlug($slug)->first();
        if (!$development) {
            echo $slug;
            die;
        }
        $data['back'] = 'https://misritaliaproperties.com/developments/' . $development->slug;
        return view('front.landing.index', $data);
    }
    public function sitevisit()
    {
        $slug = 'ilbosco';
		

        $development = Development::whereSlug($slug)->first();
        $development->inquiry_content =  'Let the Site Speak! IL BOSCO New Capital is delivering 2020, a year ahead of schedule, as construction progresses impressively. A revolutionary concept built on the philosophy of encapsulating nature within city life, IL BOSCO creates a true ecological experience. The lifestyle allows for a free force of organic living spread upon the urban topography giving variable aspects to be integrated within the community in form of energy.';
        $development->inquiry_image =  'ed781851afa60c2aae7367ed993436f1.jpeg';

        $data['development'] = $development;
        $data['projects'] = Development::orderBy('sort')->get();
        // dd($data['projects']);
        return view('front.landing.sitevisit', $data);
    }
	
	public function inquiry()
    {
        $slug = 'ilbosco';
		

        $development = Development::whereSlug($slug)->first();
        $development->inquiry_content =  'Let the Site Speak! IL BOSCO New Capital is delivering 2020, a year ahead of schedule, as construction progresses impressively. A revolutionary concept built on the philosophy of encapsulating nature within city life, IL BOSCO creates a true ecological experience. The lifestyle allows for a free force of organic living spread upon the urban topography giving variable aspects to be integrated within the community in form of energy.';
        $development->inquiry_image =  'ed781851afa60c2aae7367ed993436f1.jpeg';

        $data['development'] = $development;
        $data['projects'] = Development::orderBy('sort')->get();
        // dd($data['projects']);
        return view('front.landing.inquiry', $data);
    }

    public function events()
    {
        $data['left_side2'] = true;
        return view('front.landing.events', $data);
    }

    public function save(Request $request)
    {
		
        $development = Development::findOrFail($request->development_id);
        $developments_inquiries = new DevelopmentsInquiries();
        $developments_inquiries->fill($request->all());

        if (!$developments_inquiries->validate()) {
            return response()->json($developments_inquiries->errors());
        }
        $developments_inquiries->save();

        $developments_inquiries->sent = $this->integrate($developments_inquiries, $development);
        $developments_inquiries->save();
        $page = 'thank-you/'.$development->slug;
        if (request()->has('source'))
            $page .= '?source=' . request('source');

        return redirect($page);
    }
    function thankYou($slug='')
    {
        $data['development'] =  $development = Development::whereSlug($slug)->first();
		if($development)
			$data['back'] = 'https://test.misritaliaproperties.com/developments/' . $development->slug;
        return view('front.landing.thankyou', $data);
    }

    function integrate($lead = null, $development = null)
    {
        $url = 'https://gap.e-phunk.com/public/api/v1/add_lead';
        if (request()->has('utm_source') && in_array(strtolower(request('utm_source')), array_keys($this->sources)))
            $source = $this->sources[strtolower(request('utm_source'))];
        else
            $source = 4;
        $source = (int) $source;

        if (request()->filled('utm_campaign'))
            $campaign = request('utm_campaign');
        else
            $campaign = '';


        if ($lead == null) {
            $post = [
                'name' => 'hossam',
                "phone" => '01003603410',
                "email" => 'hossam.ahmed@media-sci.com',
                "source_id" => $source,
                "campaign_name" => $campaign,
                "adset_name" => request('utm_term'),
                "comment" => '',
                "project_id" => 15
            ];
        } else {
            $post = [
                'name' => $lead->first_name . ' ' . $lead->last_name,
                "phone" => $lead->phone,
                "email" => $lead->email,
                "source_id" => $source,
                "campaign_name" => $campaign,
                "adset_name" => (request('utm_term') == null) ? '' : request('utm_term'),
                "comment" => ($lead->message == null) ? '' : $lead->message,
                "project_id" => (int) $development->inquiry_project_id
            ];
            //dd($post);
        }
        $data_string = json_encode($post);
		/*$data_string2 = '{"name":"test test","phone":"01234567891","email":"admin@admin.com","source_id":4,"campaign_name":"no campaign","adset_name":"no adset name","comment":"","project_id":16}';
		echo $data_string ;
		echo '<br>';
		echo $data_string2 ;
		die ;*/
        $ch = curl_init();
		//$data_string = '{"name":"test test","phone":"01234567891","email":"admin@admin.com","source_id":4,"campaign_name":"no campaign","adset_name":"no adset name","comment":"","project_id":16}';
		curl_setopt($ch, CURLOPT_URL, $url );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

		$headers = array();
		$headers[] = 'Accept: */*';
		$headers[] = 'X-Api-Key: R1nsYlB3XeI2DI5V8XObO6PUN19bm3';
		$headers[] = 'Content-Type: application/json';
		$headers[] = curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
		$headers[] = curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
			
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);
// echo exec('whoami');
//		echo $result ;die ;
        $result = json_decode($result, 1);

        if (is_array($result) && isset($result['success']) && $result['success'] == true)
            return 1;
        else
            return 0;
    }
}
