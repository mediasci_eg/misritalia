<?php

namespace App\Http\Controllers\Backend;

use App\Models\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Elsayednofal\MediaManager\Controllers\MediaController;
use DB;

class EventsController extends Controller
{
    function index(Request $request)
    {
        $events = Event::orderBy('id', 'DESC');
        if ($request->title) {
            $events = Event::where('title', 'LIKE', $request->title . '%');
            //            dd($news_events);
            //            $news_events = $this->search($request, $news_events);
        }
        $data['result'] = $events->paginate(20);
        return view('backend.events.index', $data);
    }

    function create(Request $request)
    {
        $data['model'] = $model = new Event;
        return view('backend.events.create', $data);
    }

    function update(Request $request, $id)
    {
        $data['model'] = $model = Event::find($id);
        return view('backend.events.update', $data);
    }

    function store(Request $request)
    {
        // dd(request()->all());
        DB::beginTransaction();
        try {

            $model = Event::findOrNew($request->id);
            $model->fill($request->all());

            // Image
            if ($request->hasFile('image') && $request->image != null) {
                $image = $this->imageUpload($request, 'image');
                if ($image)
                    $model->image = $image;
                else
                    return redirect()->back()->withErrors(' Image Type is not Supported');
            }





            if (!$model->validate()) {
                session()->flash('errors', $model->errors());
                return back();
            }
            $model->save();
            DB::commit();
            return redirect()->back()->with('success', 'Event Saved Successfully');
        } catch (\Exception $exception) {
            throw $exception;
            DB::rollBack();
            return back();
        }
    }

    function delete(Request $request, $id)
    {
        $event = Event::where('id', $id)->first();
        $event->delete();

        return back();;
    }
}