<?php
namespace App\Http\Controllers\Backend;

use App\Models\BoxOfHope ;
use App\Models\BoxOfHopeVolum ;

class BoxOfHopeController extends BaseController
{
    public function index(BoxOfHope $model)
    {
        $model = $this->search($model);

        $data['data'] = $model->paginate(20);
        return view('backend.box_of_hope.index',$data);
    }


    public function update($id)
    {

        $model = BoxOfHope::find($id) ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                //return back();
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.box_of_hope.form',$data);
    }


    private function store($model){
        \DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('BoxOfHope'));

            if($model->validate())
                $model->save() ;
            else
                $valid = false;
            BoxOfHopeVolum::where('box_of_hope_id' , $model->id)->delete();
            foreach(request('BoxOfHopeVolum') as $elements){
                $BoxOfHopeVolum = new BoxOfHopeVolum ;
                $BoxOfHopeVolum->fill($elements) ;
                $BoxOfHopeVolum->box_of_hope_id = $model->id ;
                if($BoxOfHopeVolum->validate())
                    $BoxOfHopeVolum->save() ;
                else
                    $valid = false;
              }

            if(!$valid)
				throw new \Exception('Not Saved');
            \DB::commit();
            return true ;
        } catch (\Exception $e) {
            throw $e;
            //\DB::rollback();
            return false;
        }
    }


    public function delete($id){
		try {
        $model = BoxOfHope::destroy($id);
        if($model)
            session()->flash('msg', 'Deleted Successfully');
		} catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }


    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
        }
		$model = $model->orderByDesc('id');
        return $model ;
    }



}
