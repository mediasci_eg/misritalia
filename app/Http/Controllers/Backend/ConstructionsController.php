<?php 
namespace App\Http\Controllers\Backend;

use App\Models\Construction ;

class ConstructionsController extends BaseController
{
    public function index(Construction $model)
    {
        $model = $this->search($model);

        $data['data'] = $model->paginate(20);
        return view('backend.constructions.index',$data);
    }

    public function create()
    {
        $model = new Construction ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Created Successfully');
                return redirect('backend/constructions');				
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.constructions.form',$data);
    }

    public function update($id)
    {
        $model = Construction::find($id) ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                return redirect('backend/constructions');				
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.constructions.form',$data);
    }

 
    private function store($model){
        //\DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('Construction'));

            if($model->validate())
                $model->save() ;
            else
                $valid = false;
            if(!$valid)
				throw new \Exception('Not Saved');	
            //\DB::commit();
            return true ;			
        } catch (\Exception $e) {
            //throw $e;	
            //\DB::rollback();
            return false;
        }
    }

	
    public function delete($id){
		try {
        $model = Construction::destroy($id);
        if($model)
            session()->flash('msg', 'Deleted Successfully');
		} catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }
	
	
    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
            if(request('development_id')!='')
                $model = $model->where('development_id' , request('development_id'));
            if(request('date')!='')
                $model = $model->where('date' , request('date'));
        }
		$model = $model->orderByDesc('id');
        return $model ;
    }
	
    

}