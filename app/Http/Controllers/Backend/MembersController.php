<?php 
namespace App\Http\Controllers\Backend;

use App\Models\Member ;

class MembersController extends BaseController
{
    public function index(Member $model)
    {
        $model = $this->search($model);
        $model = $model->orderBy('sort');

        $data['data'] = $model->paginate(20);
        return view('backend.members.index',$data);
    }

    public function create()
    {
        $model = new Member ;
        if (request()->has('save')){
            $model->sort = 1 ;
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Created Successfully');
                return redirect('backend/members');				
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.members.form',$data);
    }

    public function update($id)
    {
        $model = Member::find($id) ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                return redirect('backend/members');				
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.members.form',$data);
    }

 
    private function store($model){
        //\DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('Member'));

            if(request()->hasFile('Member.image')) 
                $model->image = $model->uploadImage('image');

            if($model->validate())
                $model->save() ;
            else
                $valid = false;
            if(!$valid)
				throw new \Exception('Not Saved');	
            //\DB::commit();
            return true ;			
        } catch (\Exception $e) {
            //throw $e;	
            //\DB::rollback();
            return false;
        }
    }

	
    public function delete($id){
		try {
        $model = Member::destroy($id);
        if($model)
            session()->flash('msg', 'Deleted Successfully');
		} catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }
	
    public function sort(){
        $i=1 ;
        foreach(request()->get('id') as $id){
            $model = Member::find($id);
            $model->sort = $i;
            $model->save();
            $i++ ;	
        }
        return back();
    }
	
    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
            if(request('name')!='')	
                $model = $model->where('name' ,'like' , '%'.request('name').'%');
            if(request('position')!='')	
                $model = $model->where('position' ,'like' , '%'.request('position').'%');
            if(request('team')!='')	
                $model = $model->where('team' ,'like' , '%'.request('team').'%');
            if(request('sort')!='')
                $model = $model->where('sort' , request('sort'));
        }
        return $model ;
    }
	
    

}