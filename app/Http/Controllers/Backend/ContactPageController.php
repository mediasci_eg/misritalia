<?php
namespace App\Http\Controllers\Backend;

use App\Models\ContactPage ;

class ContactPageController extends BaseController
{
    public function index(ContactPage $model)
    {
        $data['showrooms'] =  $model->where('type' , 'showrooms')->get();
        $data['support'] =  $model->where('type' , 'support')->get();
        if (request()->has('save')){
            $this->store();
            return back();
        }
        return view('backend.contact_page.index',$data);
    }

    private function store(){
        foreach(request('ContactPage') as $contactPage){
			if($contactPage['id'] ==''){
				$model = new ContactPage ;	
				$model->type = 'showrooms' ;
			}else{
				$model = ContactPage::find($contactPage['id']);	
			}
				
			
            $model->fill($contactPage);
            $model->save() ;
			$ids[] = $model->id ;
        }
		ContactPage::whereNotIn('id' , $ids)->delete();
		
        session()->flash('msg', 'Updated Successfully');

        return true ;
    }

}
