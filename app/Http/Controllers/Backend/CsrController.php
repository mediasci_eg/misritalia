<?php
namespace App\Http\Controllers\Backend;

use App\Models\Csr ;

class CsrController extends BaseController
{
    public function index(Csr $model)
    {
        $model = $this->search($model);
        $model = $model->whereNull('parent_id');

        $data['data'] = $model->paginate(20);
        return view('backend.csr.index',$data);
    }

    public function create()
    {
        $model = new Csr ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Created Successfully');
                return redirect('backend/csr');
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.csr.form',$data);
    }

    public function update($id)
    {
        $model = Csr::find($id) ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                return redirect('backend/csr');
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.csr.form',$data);
    }


    private function store($model){
        //\DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('Csr'));
            if($model->validate())
                $model->save() ;
            else
                $valid = false;

            if(request()->has('Sub')){
                Csr::where('parent_id' , $model->id)->delete();
                foreach(request('Sub') as $sub){
                    $csr = new Csr ;
                    $csr->fill($sub);
                    $csr->parent_id = $model->id ;
                    $csr->save();
                }
            }
            if(!$valid)
				throw new \Exception('Not Saved');
            //\DB::commit();
            return true ;
        } catch (\Exception $e) {
            throw $e;
            //\DB::rollback();
            return false;
        }
    }


    public function delete($id){
		try {
        $model = Csr::destroy($id);
        if($model)
            session()->flash('msg', 'Deleted Successfully');
		} catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }


    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
            if(request('title')!='')
                $model = $model->where('title' ,'like' , '%'.request('title').'%');
        }
		$model = $model->orderByDesc('id');
        return $model ;
    }



}
