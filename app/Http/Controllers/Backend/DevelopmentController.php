<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Development;
use DB;
use App\Models\DevelopmentSlider;
use App\Models\DevelopmentBrochure;
use App\Models\DevelopmentUnitType;

class DevelopmentController extends Controller {

    public function index() {
        $data['developments'] = Development::orderBy('id', 'ASC')->get();

        return view('backend.development.index', $data);
    }

    public function create() {


        $data['model'] = new Development;

        return view('backend.development.create', $data);
    }

    public function store(Request $request) {
//         dd($request->all());

        DB::beginTransaction();
        try {
            $development = Development::findOrNew($request->id);
            $development->fill($request->all());

            if (!$development->validate()) {
                return back()->withErrors($development->errors())->withInput();
            }

            $development->save();
            
//            Slider Images
            if ($request->has('images')) {
                DevelopmentSlider::where('development_id', $development->id)->delete();
                foreach ($request->images as $image) {
                    $new_image = new DevelopmentSlider;
                    $new_image->development_id = $development->id;
                    $new_image->image = $image;
                    $new_image->save();
                }
            }

            // Brochures
            DevelopmentBrochure::where('development_id' , $development->id)->delete();
            foreach(request('DevelopmentBrochure') as $elements){
                $DevelopmentBrochure = new DevelopmentBrochure ;
                $DevelopmentBrochure->fill($elements) ;
                $DevelopmentBrochure->development_id = $development->id ;
                if($DevelopmentBrochure->validate())
                    $DevelopmentBrochure->save() ;
            }

            DevelopmentUnitType::where('development_id' , $development->id)->delete();
            foreach(request('DevelopmentUnitType') as $elements){
                if(!isset($elements['image']) || $elements['image']=='')continue ;
                $DevelopmentUnitType = new DevelopmentUnitType ;
                $DevelopmentUnitType->fill($elements) ;
                $DevelopmentUnitType->development_id = $development->id ;
                if($DevelopmentUnitType->validate())
                    $DevelopmentUnitType->save() ;
            
            }

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;
            DB::rollBack();
            return back();
        }


        return redirect()->back()->with('success', 'Data Saved Successfully');
    }

    public function update(Request $request, $id) {
        $data['model'] = Development::where('id', $id)->first();
        return view('backend.development.edit', $data);
    }

    function delete(Request $request, Development $development) {

        $development->delete();
        return response()->json(['message' => 'branch deleted successfully']);
    }

}
