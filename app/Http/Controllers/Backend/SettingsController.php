<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Setting;

class SettingsController extends Controller
{

    public function anyIndex()
    {
        $data['settings'] = Setting::Orderby('id', 'ASC')->get();
        return view('backend.settings.index', $data);
    }
    public function anyUpdate()
    {
        if(isset($_POST['save']))
        {
            foreach($_POST['value'] as $key => $row)
            {
                $setting = Setting::find($key);
                if($setting)
                {
                    if(is_array($row))
                    {
                        foreach($row as $key2 => $row2)
                        {
                            $setting = Setting::find($key);
                            if($setting)
                            {
                                $setting->value = $row2;
                                $setting->save();
                            }
                        }
                    }
                    else
                    {
                        $setting->value = $row;
                        $setting->save();
                    }
                }
            }

//            session()->put('success', 'Update successfully');
//            return redirect(\URL::Current());
            return redirect()->back()->with('success', 'Setting Saved Successfully');
        }
        $data['settings'] = Setting::Orderby('id', 'ASC')->get();
        return view('backend.settings.index', $data);
    }

}
