<?php
namespace App\Http\Controllers\Backend;

use App\Models\FormWhistleblowing ;

class FormWhistleblowingController extends BaseController
{
    public function index(FormWhistleblowing $model)
    {
        $model = $this->search($model);

        $data['data'] = $model->paginate(20);
        return view('backend.form_whistleblowing.index',$data);
    }





    public function view($id){
        $data['model'] = FormWhistleblowing::find($id) ;
        return view('backend.form_whistleblowing.view',$data);
    }


    public function delete($id){
		try {
        $model = FormWhistleblowing::destroy($id);
        if($model)
            session()->flash('msg', 'Deleted Successfully');
		} catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }


    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
            if(request('subject')!='')
                $model = $model->where('subject' ,'like' , '%'.request('subject').'%');
        }
		$model = $model->orderByDesc('id');
        return $model ;
    }



}
