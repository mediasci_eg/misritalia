<?php 
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\Models\Slider ;

class SliderController extends Controller
{
    public function index(Slider $model)
    {
        $model = $this->search($model);
        $model = $model->orderBy('sort');

        $data['data'] = $model->paginate(20);
        return view('backend.slider.index',$data);
    }

    public function create()
    {
        $model = new Slider ;
        if (request()->has('save')){
            $model->sort = 1 ;
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Created Successfully');
                return redirect('backend/slider');				
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.slider.form',$data);
    }

    public function update($id)
    {
        $model = Slider::find($id) ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                return redirect('backend/slider');				
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.slider.form',$data);
    }

 
    private function store($model){
        //\DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('Slider'));

            if($model->validate())
                $model->save() ;
            else
                $valid = false;
            if(!$valid)
				throw new \Exception('Not Saved');	
            //\DB::commit();
            return true ;			
        } catch (\Exception $e) {
            throw $e;	
            //\DB::rollback();
            return false;
        }
    }

	
    public function delete($id){
		try {
        $model = Slider::destroy($id);
        if($model)
            session()->flash('msg', 'Deleted Successfully');
		} catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }
	
    public function sort(){
        $i=1 ;
        foreach(request()->get('id') as $id){
            $model = Slider::find($id);
            $model->sort = $i;
            $model->save();
            $i++ ;	
        }
        return back();
    }
	
    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
            if(request('title')!='')	
                $model = $model->where('title' ,'like' , '%'.request('title').'%');
            if(request('subtitle')!='')	
                $model = $model->where('subtitle' ,'like' , '%'.request('subtitle').'%');
            if(request('sort')!='')
                $model = $model->where('sort' , request('sort'));
        }
        return $model ;
    }
	
    

}