<?php
namespace App\Http\Controllers\Backend;

use App\Models\FoundersMessage ;

class FoundersMessageController extends BaseController
{
    public function index(FoundersMessage $model)
    {
        $model = $this->search($model);

        $data['data'] = $model->paginate(20);
        return view('backend.founders_message.index',$data);
    }

    public function create()
    {
        $model = new FoundersMessage ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Created Successfully');
                return redirect('backend/founders_message');
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.founders_message.form',$data);
    }

    public function update($id)
    {
        $model = FoundersMessage::find($id) ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                return back();
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.founders_message.form',$data);
    }


    private function store($model){
        //\DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('FoundersMessage'));

            if($model->validate())
                $model->save() ;
            else
                $valid = false;
            if(!$valid)
				throw new \Exception('Not Saved');
            //\DB::commit();
            return true ;
        } catch (\Exception $e) {
            //throw $e;
            //\DB::rollback();
            return false;
        }
    }


    public function delete($id){
		try {
        $model = FoundersMessage::destroy($id);
        if($model)
            session()->flash('msg', 'Deleted Successfully');
		} catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }


    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
        }
		$model = $model->orderByDesc('id');
        return $model ;
    }



}
