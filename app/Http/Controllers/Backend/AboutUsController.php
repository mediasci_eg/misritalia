<?php 
namespace App\Http\Controllers\Backend;

use App\Models\AboutUs ;

class AboutUsController extends BaseController
{
    public function index(AboutUs $model)
    {
        $model = $this->search($model);

        $data['data'] = $model->paginate(20);
        return view('backend.about_us.index',$data);
    }

    public function create()
    {
        $model = new AboutUs ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Created Successfully');
                return back();				
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.about_us.form',$data);
    }

    public function update($id)
    {
        $model = AboutUs::find($id) ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                return back();				
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.about_us.form',$data);
    }

 
    private function store($model){
        //\DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('AboutUs'));

            if($model->validate())
                $model->save() ;
            else
                $valid = false;
            if(!$valid)
				throw new \Exception('Not Saved');	
            //\DB::commit();
            return true ;			
        } catch (\Exception $e) {
            //throw $e;	
            //\DB::rollback();
            return false;
        }
    }

	
    public function delete($id){
		try {
        $model = AboutUs::destroy($id);
        if($model)
            session()->flash('msg', 'Deleted Successfully');
		} catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }
	
	
    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
            if(request('founder_message_link')!='')	
                $model = $model->where('founder_message_link' ,'like' , '%'.request('founder_message_link').'%');
            if(request('inspiration_link')!='')	
                $model = $model->where('inspiration_link' ,'like' , '%'.request('inspiration_link').'%');
            if(request('board_of_directors_link')!='')	
                $model = $model->where('board_of_directors_link' ,'like' , '%'.request('board_of_directors_link').'%');
            if(request('executive_management_link')!='')	
                $model = $model->where('executive_management_link' ,'like' , '%'.request('executive_management_link').'%');
            if(request('management_team_link')!='')	
                $model = $model->where('management_team_link' ,'like' , '%'.request('management_team_link').'%');
            if(request('newsletter_files')!='')	
                $model = $model->where('newsletter_files' ,'like' , '%'.request('newsletter_files').'%');
        }
		$model = $model->orderByDesc('id');
        return $model ;
    }
	
    

}