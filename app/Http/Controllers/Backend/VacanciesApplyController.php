<?php 
namespace App\Http\Controllers\Backend;

use App\Models\VacanciesApply ;

class VacanciesApplyController extends BaseController
{
    public function index(VacanciesApply $model)
    {
        $model = $this->search($model);

        $data['data'] = $model->paginate(20);
        return view('backend.vacancies_apply.index',$data);
    }


    public function update($id)
    {
        $model = VacanciesApply::find($id) ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                return redirect('backend/vacancies_apply');				
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.vacancies_apply.form',$data);
    }

 
    private function store($model){
        //\DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('VacanciesApply'));

            if($model->validate())
                $model->save() ;
            else
                $valid = false;
            if(!$valid)
				throw new \Exception('Not Saved');	
            //\DB::commit();
            return true ;			
        } catch (\Exception $e) {
            //throw $e;	
            //\DB::rollback();
            return false;
        }
    }

	
    public function delete($id){
		try {
        $model = VacanciesApply::destroy($id);
        if($model)
            session()->flash('msg', 'Deleted Successfully');
		} catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }
	
	
    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
            if(request('vacancy_id')!='')
                $model = $model->where('vacancy_id' , request('vacancy_id'));
            if(request('first_name')!='')	
                $model = $model->where('first_name' ,'like' , '%'.request('first_name').'%');
            if(request('last_name')!='')	
                $model = $model->where('last_name' ,'like' , '%'.request('last_name').'%');
            if(request('email')!='')	
                $model = $model->where('email' ,'like' , '%'.request('email').'%');
            if(request('phone')!='')	
                $model = $model->where('phone' ,'like' , '%'.request('phone').'%');
            if(request('cv')!='')	
                $model = $model->where('cv' ,'like' , '%'.request('cv').'%');
        }
		$model = $model->orderByDesc('id');
        return $model ;
    }
	
    

}