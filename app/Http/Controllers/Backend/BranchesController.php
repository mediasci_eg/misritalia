<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Branches;


class BranchesController extends Controller {

   

    public function index() {
        $data['branches'] = Branches::orderBy('id', 'ASC')->get();
        
        return view('backend.branches.index', $data);
    }

    public function create() {


        $data['branch'] = new Branches;

        return view('backend.branches.create', $data);
    }

   
    public function store(Request $request) {

        // dd($request->all());
        $branch=Branches::findOrNew($request->id);
        $branch->fill($request->all());
        if (!$request->validate([
            'name' => "required",
            'address' => "required",
            'phone' => "required",
            'email' => "required",
            'lat' => "required",
            'lng' => "required"
        ],
        [
            'lat.required' => 'Location is required!',
            'lng.required' => 'Location is required!'
        ])) {
            session()->flash('errors', $branch->errors());
            return back();
        }
        $branch->save();
        return redirect()->back()->with('success','Data Saved Successfully');
    }

    public function update(Request $request, $id) {
        $data['branch'] = Branches::where('id',$id)->first();

        return view('backend.branches.edit', $data);
    }

    function delete(Request $request,Branches $branch){
        // dd()
        $branch->delete();
        return response()->json(['message'=>'branch deleted successfully']);
    }

}
