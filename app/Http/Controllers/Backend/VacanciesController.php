<?php

namespace App\Http\Controllers\Backend;

use App\Models\Vacancy;

class VacanciesController extends BaseController
{
    public function index(Vacancy $model)
    {
        $model = $this->search($model);

        $data['data'] = $model->paginate(20);
        return view('backend.vacancies.index', $data);
    }

    public function create()
    {
        $model = new Vacancy;
        if (request()->has('save')) {
            $store = $this->store($model);
            if ($store === true) {
                session()->flash('msg', 'Created Successfully');
                return redirect('backend/vacancies');
            } else {
                $data['errors'] = true;
            }
        }

        $data['model'] = $model;
        return view('backend.vacancies.form', $data);
    }

    public function update($id)
    {
        $model = Vacancy::find($id);
        if (request()->has('save')) {
            $store = $this->store($model);
            if ($store === true) {
                session()->flash('msg', 'Updated Successfully');
                return redirect('backend/vacancies');
            } else {
                $data['errors'] = true;
            }
        }

        $data['model'] = $model;
        return view('backend.vacancies.form', $data);
    }


    private function store($model)
    {
        //\DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('Vacancy'));
            $model->requirments = request('requirments');
            $model->about = request('about');

            if ($model->validate())
                $model->save();
            else
                $valid = false;
            if (!$valid)
                throw new \Exception('Not Saved');
            //\DB::commit();
            return true;
        } catch (\Exception $e) {
            //throw $e;
            //\DB::rollback();
            return false;
        }
    }


    public function delete($id)
    {
        try {
            $model = Vacancy::destroy($id);
            if ($model)
                session()->flash('msg', 'Deleted Successfully');
        } catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }


    private function search($model)
    {
        if (request()->has('search')) {
            if (request('id') != '')
                $model = $model->where('id', request('id'));
            if (request('title') != '')
                $model = $model->where('title', 'like', '%' . request('title') . '%');
            if (request('is_current') != '')
                $model = $model->where('is_current', request('is_current'));
        }
        $model = $model->orderByDesc('id');
        return $model;
    }
}