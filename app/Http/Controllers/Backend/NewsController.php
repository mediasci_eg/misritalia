<?php

namespace App\Http\Controllers\Backend;

use App\Models\News;

class NewsController extends BaseController
{
    public function index(News $model)
    {
        $model = $this->search($model);

        $data['data'] = $model->paginate(20);
        return view('backend.news.index', $data);
    }

    public function create()
    {
        $model = new News;
        if (request()->has('save')) {
            $store = $this->store($model);
            if ($store === true) {
                session()->flash('msg', 'Created Successfully');
                return redirect('backend/news');
            } else {
                $data['errors'] = true;
            }
        }

        $data['model'] = $model;
        return view('backend.news.form', $data);
    }

    public function update($id)
    {
        $model = News::find($id);
        if (request()->has('save')) {
            $store = $this->store($model);
            if ($store === true) {
                session()->flash('msg', 'Updated Successfully');
                return redirect('backend/news');
            } else {
                $data['errors'] = true;
            }
        }

        $data['model'] = $model;
        return view('backend.news.form', $data);
    }


    private function store($model)
    {

        //\DB::beginTransaction();
        // dd(request('content'));
        try {
            $valid = true;
            $model->fill(request('News'));
            $model->content = request('News')['first_paragraph'];

            if (request()->hasFile('image') && request()->image != null) {
                $image = $this->imageUpload(request(), 'image');
                if ($image)
                    $model->image = $image;
                else
                    return redirect()->back()->withErrors('this file Type is not Supported');
            }

            if (request()->hasFile('inner_image') && request()->inner_image != null) {
                $image = $this->imageUpload(request(), 'inner_image');

                if ($image)
                    $model->inner_image = $image;
                else
                    return redirect()->back()->withErrors('this file Type is not Supported');
            }

            if ($model->validate())
                $model->save();
            else
                $valid = false;
            if (!$valid)
                throw new \Exception('Not Saved');
            //\DB::commit();
            return true;
        } catch (\Exception $e) {
            //throw $e;
            //\DB::rollback();
            return false;
        }
    }


    public function delete($id)
    {
        try {
            $model = News::destroy($id);
            if ($model)
                session()->flash('msg', 'Deleted Successfully');
        } catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }


    private function search($model)
    {
        if (request()->has('search')) {
            if (request('id') != '')
                $model = $model->where('id', request('id'));
            if (request('title') != '')
                $model = $model->where('title', 'like', '%' . request('title') . '%');
        }
        $model = $model->orderByDesc('id');
        return $model;
    }
}
