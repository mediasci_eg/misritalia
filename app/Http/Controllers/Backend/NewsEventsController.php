<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Elsayednofal\MediaManager\Controllers\MediaController;
use App\Models\NewsEvents;
use DB;

class NewsEventsController extends Controller
{

    function index(Request $request)
    {
        $news_events = NewsEvents::orderBy('id', 'DESC');
        if ($request->title) {
            $news_events = NewsEvents::where('title', 'LIKE', $request->title . '%');
            //            dd($news_events);
            //            $news_events = $this->search($request, $news_events);
        }
        $data['result'] = $news_events->paginate(20);
        return view('backend.news-events.index', $data);
    }

    function create(Request $request)
    {
        $data['model'] = $model = new NewsEvents;
        return view('backend.news-events.create', $data);
    }

    function update(Request $request, $id)
    {
        $data['model'] = $model = NewsEvents::find($id);
        return view('backend.news-events.update', $data);
    }

    function store(Request $request)
    {
        dd(request()->all());
        DB::beginTransaction();
        try {

            $model = NewsEvents::findOrNew($request->id);
            $model->fill($request->all());

            //Square Image
            if ($request->hasFile('square_image') && $request->square_image != null) {
                $square = $this->imageUpload($request, 'square_image');
                if ($square)
                    $model->square_image = $square;
                else
                    return redirect()->back()->withErrors('Square Image Type is not Supported');
            }

            //Rectangle Image
            if ($request->hasFile('rectangle_image') && $request->rectangle_image != null) {
                $rectangle = $this->imageUpload($request, 'rectangle_image');
                if ($rectangle)
                    $model->rectangle_image = $rectangle;
                else
                    return redirect()->back()->withErrors('Rectangle Image Type is not Supported');
            }

            //Detail Image
            if ($request->hasFile('detail_image') && $request->detail_image != null) {
                $detail = $this->imageUpload($request, 'detail_image');
                if ($detail)
                    $model->detail_image = $detail;
                else
                    return redirect()->back()->withErrors('Detail Image Type is not Supported');
            }

            $model->slug = str_slug($request->title);
            if ($request->get('is_featured'))
                $model->is_featured = 1;
            else
                $model->is_featured = 0;
            if (!$model->validate()) {
                session()->flash('errors', $model->errors());
                return back();
            }
            $model->save();
            DB::commit();
            return redirect()->back()->with('success', 'News&Events Saved Successfully');
        } catch (\Exception $exception) {
            throw $exception;
            DB::rollBack();
            return back();
        }
    }

    function delete(Request $request, NewsEvents $news_events)
    {
        $news_events->delete();
        return response()->json(['message' => 'News&Events deleted successfully']);
    }
}
