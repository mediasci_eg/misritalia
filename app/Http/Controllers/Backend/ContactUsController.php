<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use Elsayednofal\MediaManager\Controllers\MediaController;
use App\Models\ContactUs;

class ContactUsController extends Controller
{

    function index(Request $request)
    {
        // dd($request->all());
        $contacts = ContactUs::orderBy('id', 'DESC');
        if ($request->search) {
            $contacts = $this->search($request, $contacts);
        }
        if ($request->trashed) {
            $contacts = $contacts->onlyTrashed();
        }
        
        $data['contacts'] = $contacts->paginate(20);
        return view('backend.contact-us.index', $data);
    }
    function indexTrashed(Request $request)
    {
        
        $contacts = ContactUs::orderBy('id', 'DESC')->onlyTrashed();
        if ($request->search) {
            $contacts = $this->search($request, $contacts);
        }
    
        $data['contacts'] = $contacts->paginate(20);
        return view('backend.contact-us.index', $data);
    }

    public function ajview(Request $request){
        $contacts = ContactUs::where('id',$request->key_id)->first();
        $contacts->is_read=1;
        $contacts->save();
        $response = array(
            'status' => 'success',
            'msg' => $request->message,
        );
        return response()->json($response); 
     }

    function delete(Request $request,ContactUs $contacts){
        $contacts->delete();
        return response()->json(['message'=>'user deleted successfully']);
    }

    function search(Request $request, $contacts)
    {
        foreach ($request->contacts as $key => $value) {
            if ($key == 'page' || $key == 'search' || $value == '') continue;

            if (is_numeric($value)) {
                $contacts = $contacts->where($key, $value);
            } else {
                $contacts = $contacts->where($key, 'like', $value . '%');
            }
        }
        return $contacts;
    }

    // function index(Request $request)
    // {
    //     $data['contact']=$contact= ContactUs::where('id',1)->first();
    //     if ($request->save) {
    //         $contact = $this->store($request, $contact);
    //     }

    //     return view('backend.contact-us.index', $data);
        
    // }

    // function store(Request $request,$contact)
    // {
    //     $request->validate([
    //         'address' => 'required',
    //         'phone' => 'required',
    //         'mail' => 'required|mail',
    //     ]);

    //     $contact->address = $request->address;
    //     $contact->phone = $request->phone;
    //     $contact->mail = $request->mail;
    //     $contact->save();
       
    //     return redirect()->back()->with('success', 'Data Saved Successfully');
    // }



}
