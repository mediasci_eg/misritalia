<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UploadPdfController extends Controller
{
    public function index(Request $request)
    {

        $pdf = request()->file('file');
        $input['file'] = $pdf;
        $v = \Validator::make($input, [
            'file' => 'required|mimes:pdf',
        ]);

        if (!in_array($pdf->extension(), ['pdf']))
            return false;

        if ($v->fails()) {
            $errors = $v->errors()->toArray();
            $return = ['status' => '0', 'msg' => $errors['file']];
            echo json_encode($return);
            die;
        }
        //$name = md5(microtime()) . '.' . $image->getClientOriginalExtension();
        $name = $pdf->getClientOriginalName();
        $folder = public_path('brochures');
        if (request()->has('folder')) {
            if (!is_dir(public_path('brochures/' . request('folder'))))
                mkdir(public_path('brochures/' . request('folder')));
            $folder = public_path('brochures/' . request('folder'));
        }

        $pdf->move($folder, $name);

        $return = ['status' => '1', 'file' => $name];
        echo json_encode($return);
        die;
    }
}