<?php
namespace App\Http\Controllers\Backend;

use App\Models\MessageOfHope ;
use App\Models\MessageOfHopeTab ;

class MessageOfHopeController extends BaseController
{
    public function index(MessageOfHope $model)
    {
        $model = $this->search($model);

        $data['data'] = $model->paginate(20);
        return view('backend.message_of_hope.index',$data);
    }

    public function create()
    {
        $model = new MessageOfHope ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Created Successfully');
                return redirect('backend/message_of_hope');
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.message_of_hope.form',$data);
    }

    public function update($id)
    {
        $model = MessageOfHope::find($id) ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                return back();
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.message_of_hope.form',$data);
    }


    private function store($model){
        //\DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('MessageOfHope'));

            if($model->validate())
                $model->save() ;
            else
                $valid = false;
            MessageOfHopeTab::where('message_of_hope_id' , $model->id)->delete();
            foreach(request('MessageOfHopeTab') as $elements){
                $MessageOfHopeTab = new MessageOfHopeTab ;
                $MessageOfHopeTab->fill($elements) ;
                $MessageOfHopeTab->message_of_hope_id = $model->id ;
                if($MessageOfHopeTab->validate())
                    $MessageOfHopeTab->save() ;
                else
                    $valid = false;
            }

            if(!$valid)
				throw new \Exception('Not Saved');
            //\DB::commit();
            return true ;
        } catch (\Exception $e) {
            throw $e;
            //\DB::rollback();
            return false;
        }
    }


    public function delete($id){
		try {
        $model = MessageOfHope::destroy($id);
        if($model)
            session()->flash('msg', 'Deleted Successfully');
		} catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }


    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
        }
		$model = $model->orderByDesc('id');
        return $model ;
    }



}
