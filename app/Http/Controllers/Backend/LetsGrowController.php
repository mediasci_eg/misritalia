<?php
namespace App\Http\Controllers\Backend;

use App\Models\LetsGrow ;
use App\Models\LetsGrowSubTab;
use App\Models\LetsGrowTab ;

class LetsGrowController extends BaseController
{
    public function index(LetsGrow $model)
    {
        $model = $this->search($model);

        $data['data'] = $model->paginate(20);
        return view('backend.lets_grow.index',$data);
    }


    public function update($id)
    {
        $model = LetsGrow::find($id) ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                return back();
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.lets_grow.form',$data);
    }


    private function store($model){
        //\DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('LetsGrow'));

            if($model->validate())
                $model->save() ;
            else
                $valid = false;
            if(request()->has('LetsGrowTab')){
                foreach(request('LetsGrowTab') as $elements){
                    $LetsGrowTab = LetsGrowTab::find($elements['id']) ;
                    $LetsGrowTab->fill($elements) ;
                    if($LetsGrowTab->validate())
                        $LetsGrowTab->save() ;
                    else
                        $valid = false;
                }
            }

            if(request()->has('letsGrowSubTabs')){
                foreach(request('letsGrowSubTabs') as $elements){
                    $LetsGrowTab = LetsGrowSubTab::find($elements['id']) ;
                    $LetsGrowTab->fill($elements) ;
                    if(!isset($elements['images']))
                        $LetsGrowTab->images = [] ;

                    if($LetsGrowTab->validate())
                        $LetsGrowTab->save() ;
                    else
                        $valid = false;
                }

            }

            if(!$valid)
				throw new \Exception('Not Saved');
            //\DB::commit();
            return true ;
        } catch (\Exception $e) {
            //throw $e;
            //\DB::rollback();
            return false;
        }
    }




    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
            if(request('video')!='')
                $model = $model->where('video' ,'like' , '%'.request('video').'%');
        }
		$model = $model->orderByDesc('id');
        return $model ;
    }



}
