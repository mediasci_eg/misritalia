<?php
namespace App\Http\Controllers\Backend;

use App\Models\BusinessTransformation ;
use App\Models\BusinessTransformationTab ;

class BusinessTransformationController extends BaseController
{
    public function index(BusinessTransformation $model)
    {
        $model = $this->search($model);

        $data['data'] = $model->paginate(20);
        return view('backend.business_transformation.index',$data);
    }

    public function create()
    {
        $model = new BusinessTransformation ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Created Successfully');
                return redirect('backend/business_transformation');
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.business_transformation.form',$data);
    }

    public function update($id)
    {
        $model = BusinessTransformation::find($id) ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                return back();
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.business_transformation.form',$data);
    }


    private function store($model){
        //\DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('BusinessTransformation'));

            if($model->validate())
                $model->save() ;
            else
                $valid = false;
            foreach(request('BusinessTransformationTab') as $elements){
                $BusinessTransformationTab = BusinessTransformationTab::find($elements['id']) ;
                $BusinessTransformationTab->fill($elements) ;
                $BusinessTransformationTab->table_id = $model->id ;
                if($BusinessTransformationTab->validate())
                    $BusinessTransformationTab->save() ;
                else
                    $valid = false;
            }

            if(!$valid)
				throw new \Exception('Not Saved');
            //\DB::commit();
            return true ;
        } catch (\Exception $e) {
            throw $e;
            //\DB::rollback();
            return false;
        }
    }


    public function delete($id){
		try {
        $model = BusinessTransformation::destroy($id);
        if($model)
            session()->flash('msg', 'Deleted Successfully');
		} catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }


    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
        }
		$model = $model->orderByDesc('id');
        return $model ;
    }



}
