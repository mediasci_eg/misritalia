<?php
namespace App\Http\Controllers\Backend;

use App\Models\CsrPartner ;

class CsrPartnersController extends BaseController
{
    public function index(CsrPartner $model)
    {
        $model = $this->search($model);

        $data['data'] = $model->paginate(20);
        return view('backend.csr_partners.index',$data);
    }

    public function create()
    {
        $model = new CsrPartner ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Created Successfully');
                return redirect('backend/csr_partners');
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.csr_partners.form',$data);
    }

    public function update($id)
    {
        $model = CsrPartner::find($id) ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                return redirect('backend/csr_partners');
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.csr_partners.form',$data);
    }


    private function store($model){
        //\DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('CsrPartner'));

            if(request()->hasFile('CsrPartner.logo'))
                $model->logo = $model->uploadImage('logo');

            if($model->validate())
                $model->save() ;
            else
                $valid = false;
            if(!$valid)
				throw new \Exception('Not Saved');
            //\DB::commit();
            return true ;
        } catch (\Exception $e) {
            throw $e;
            //\DB::rollback();
            return false;
        }
    }


    public function delete($id){
		try {
        $model = CsrPartner::destroy($id);
        if($model)
            session()->flash('msg', 'Deleted Successfully');
		} catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }


    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
            if(request('name')!='')
                $model = $model->where('name' ,'like' , '%'.request('name').'%');
        }
		$model = $model->orderByDesc('id');
        return $model ;
    }



}
