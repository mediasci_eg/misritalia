<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DevelopmentsInquiries;

class DevelopmentsInquiriesController extends Controller
{

    function index(Request $request)
    {
        // dd($request->all());
        $developments_inquiries = DevelopmentsInquiries::orderBy('id', 'DESC');
        if ($request->search) {
            $developments_inquiries = $this->search($request, $developments_inquiries);
        }
        if ($request->trashed) {
            $developments_inquiries = $developments_inquiries->onlyTrashed();
        }
        
        $data['developments_inquiries'] = $developments_inquiries->paginate(20);
        return view('backend.developments_inquiries.index', $data);
    }
    function indexTrashed(Request $request)
    {
        
        $developments_inquiries = DevelopmentsInquiries::orderBy('id', 'DESC')->onlyTrashed();
        if ($request->search) {
            $developments_inquiries = $this->search($request, $developments_inquiries);
        }
    
        $data['developments_inquiries'] = $developments_inquiries->paginate(20);
        return view('backend.developments_inquiries.index', $data);
    }

    public function ajview(Request $request){
        $developments_inquiries = DevelopmentsInquiries::where('id',$request->key_id)->first();
        $developments_inquiries->is_read=1;
        $developments_inquiries->save();
        $response = array(
            'status' => 'success',
            'msg' => $request->message,
        );
        return response()->json($response); 
     }

    function delete(Request $request,DevelopmentsInquiries $developments_inquiries){
        $developments_inquiries->delete();
        return response()->json(['message'=>'user deleted successfully']);
    }

    function search(Request $request, $developments_inquiries)
    {
        foreach ($request->developments_inquiries as $key => $value) {
            if ($key == 'page' || $key == 'search' || $value == '') continue;

            if (is_numeric($value)) {
                $developments_inquiries = $developments_inquiries->where($key, $value);
            } else {
                $developments_inquiries = $developments_inquiries->where($key, 'like', $value . '%');
            }
        }
        return $developments_inquiries;
    }

}
