<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Post;
 
class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    function __construct() {
        $data['posts'] = Post::all();
        view()->share($data);
    }

    public function imageUpload($request, $name) {
        $this->validate($request, [
            $name => 'required|image|mimes:jpeg,png,jpg,gif',
        ]);
        $mimes = ['jpeg', 'JPEG', 'png', 'PNG', 'jpg', 'JPG', 'gif', 'GIF'];
        $image = $request->file($name);
        if (!in_array($image->getClientOriginalExtension(), $mimes))
            return false;

        $name = $name . '_' . time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads');
        $image->move($destinationPath, $name);
        return $name;
    }

    public function imageUploadslider($image) {

        $mimes = ['jpeg', 'JPEG', 'png', 'PNG', 'jpg', 'JPG', 'gif', 'GIF'];
        // $image = $request->file($name);
        if (!in_array($image->getClientOriginalExtension(), $mimes))
            return false;

        $name = rand(10,100) . '_' . time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads');
        $image->move($destinationPath, $name);
        return $name;
    }
    public function imageUploadbrochure($my_file) {

        $mimes = ['pdf','docx' , 'doc' , 'txt' , 'xlsx' , 'csv'];
        // $image = $request->file($name);
        if (!in_array($my_file->getClientOriginalExtension(), $mimes))
            return false;

        $name = rand(10,100) . '_' . time() . '.' . $my_file->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/pdf');
        $my_file->move($destinationPath, $name);
        return $name;
    }
}
