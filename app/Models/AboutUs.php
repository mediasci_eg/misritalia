<?php 
namespace App\Models ;

class AboutUs extends Base\AboutUs
{
	protected $casts = [
		'newsletter_files' => 'array'
	] ;
}