<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DevelopmentSlider extends Model {

    protected $fillable = ['image', 'development_id'];
    //=========Rules===============
    public $rules = [
        'image' => "required",
        'development_id' => "required",
    ];

}
