<?php
namespace App\Models ;

class MessageOfHopeTab extends Base\MessageOfHopeTab
{
	protected $casts = [
        'images' => 'array'
    ] ;
}
