<?php

namespace App\Models;

use App\Models\BaseModel;

class DevelopmentUnitType extends BaseModel {
    protected $table = 'development_unit_types';
    protected $fillable = ['title','image','sort', 'type', 'development_id'];

}
