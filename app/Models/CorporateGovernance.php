<?php
namespace App\Models ;

class CorporateGovernance extends Base\CorporateGovernance
{
	protected $casts = [
        'crisis_management_files' => 'array'
    ];
}
