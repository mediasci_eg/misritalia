<?php 
namespace App\Models\Base ;

class ContactPage extends BaseModel
{
    protected $table = 'contact_page';
    protected $guarded = ['id'];
    public $timestamps = false;

    //======Rules====== 
    function rules(){
        return [ 
            'title' => 'required' , 
            'type' => 'required' , 
        ];

    }
    

    
}