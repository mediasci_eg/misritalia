<?php 
namespace App\Models\Base ;

class Partner extends BaseModel
{
    protected $table = 'partners';
    protected $guarded = ['id'];
    

    //======Default Values====== 
    protected $attributes = [
	'sort' => 1,
    ];

    //======Rules====== 
    function rules(){
        return [ 
            'title' => 'required' , 
            'description' => 'required' , 
            'image' => 'required' , 
            'sort' => 'nullable|numeric' , 
        ];

    }
    

    
}