<?php

namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public $errors;

    public function validate()
    {
        $data = $this->attributes;

        // make a new validator object
        $v = \Validator::make($data, $this->rules());

        // check for failure
        if ($v->fails() || !empty($this->errors)) {
            $errors = $v->errors()->toArray();
            if (!empty($this->errors))
                $errors = array_merge($errors, $this->errors);

            // set errors and return false
            $this->errors = $errors;
            return false;
        }

        // validation pass
        return true;
    }
    public function rules(){
        return [] ;
    }

    public function errors()
    {
        return $this->errors;
    }

    function uploadImage($name)
    {
        $class = $this->getClassName();
        $image = \Request::file($class . '.' . $name);
        $input[$name] = $image;

        $v = \Validator::make($input, [
            $name => 'image',
        ]);
        if ($v->fails()) {
            $errors = $v->errors()->toArray();
            $this->errors[$name] = $errors[$name];
            return false;
        }

        $name = md5(microtime()) . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('uploads'), $name);
        return $name;
    }

    public function viewErrors($className = '')
    {
        if ($this->errors == NULL)
            return;
        $return = '<script>';
        foreach ($this->errors as $column => $error) {
            if ($className != '') //if You want to change the class name in the column id
                $class = $className;
            else
                $class = $this->getClassName();

            $column = '[name="' . $class . '[' . $column . ']"]';

            $return .= "$('" . $column . "').parents('.form-group').addClass('has-error');
			";
            $return .= "$('" . $column . "').parents('.form-group').append('<span class=\'help-block\'>" . $error[0] . "</span>');
			";

            $return .= "if(!$('" . $column . "').length){
				alert('" . $error[0] . "');
			}";
        }
        $return .= '</script>';
        return $return;
    }

    private function getClassName()
    {
        $class = explode('\\', get_called_class());
        $class = end($class);
        return $class;
    }
}
