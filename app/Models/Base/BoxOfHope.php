<?php
namespace App\Models\Base ;

class BoxOfHope extends BaseModel
{
    protected $table = 'box_of_hope';
    protected $guarded = ['id'];
    public $timestamps = false;

    //======Rules======
    function rules(){
        return [
            'description' => 'required' ,
        ];

    }


    //======Relations======
    public function volumes()
    {
        return $this->hasMany('App\Models\BoxOfHopeVolum', 'box_of_hope_id');
    }

}
