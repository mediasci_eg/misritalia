<?php
namespace App\Models\Base ;

class LetsGrowTab extends BaseModel
{
    protected $table = 'lets_grow_tabs';
    protected $guarded = ['id'];


    //======Rules======
    function rules(){
        return [
            'lets_grow_id' => 'required' ,
            'title' => 'required' ,
            'description' => 'required' ,
        ];

    }


    //======Relations======
    public function tabs()
    {
        return $this->hasMany('App\Models\LetsGrowSubTab', 'lets_grow_tab_id');
    }
    public function letsGrow()
    {
        return $this->belongsTo('App\Models\LetsGrow', 'lets_grow_id');
    }

}
