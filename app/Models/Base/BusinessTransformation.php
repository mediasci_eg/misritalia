<?php 
namespace App\Models\Base ;

class BusinessTransformation extends BaseModel
{
    protected $table = 'business_transformation';
    protected $guarded = ['id'];
    

    //======Rules====== 
    function rules(){
        return [ 
            'description' => 'required' , 
        ];

    }
    

    //======Relations======
    public function tabs()
    {
        return $this->hasMany('App\Models\BusinessTransformationTab', 'table_id');
    }
    
}