<?php 
namespace App\Models\Base ;

class Construction extends BaseModel
{
    protected $table = 'constructions';
    protected $guarded = ['id'];
    

    //======Rules====== 
    function rules(){
        return [ 
            'development_id' => 'required|numeric' , 
            'description' => 'required' , 
            'video' => 'required' , 
            'date' => 'required' , 
        ];

    }
    

    //======Relations======
    public function development()
    {
        return $this->belongsTo('App\Models\Development', 'development_id');
    }
    
}