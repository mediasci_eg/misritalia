<?php
namespace App\Models\Base ;

class LetsGrowSubTab extends BaseModel
{
    protected $table = 'lets_grow_sub_tabs';
    protected $guarded = ['id'];

    protected $casts = [
        'images' => 'array'
    ];
    //======Rules======
    function rules(){
        return [
            'lets_grow_tab_id' => 'required' ,
            'title' => 'required' ,
            'description' => 'required' ,
        ];

    }


    //======Relations======
    public function letsGrowTab()
    {
        return $this->belongsTo('App\Models\LetsGrowTab', 'lets_grow_tab_id');
    }

}
