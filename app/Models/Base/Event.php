<?php 
namespace App\Models\Base ;

class Event extends BaseModel
{
    protected $table = 'events';
    protected $guarded = ['id'];
    

    //======Rules====== 
    function rules(){
        return [ 
            'title' => 'required' , 
            'sub_title' => 'required' , 
            'date' => 'required' , 
            'content' => 'required' , 
            'image' => 'required' , 
        ];

    }
    

    
}