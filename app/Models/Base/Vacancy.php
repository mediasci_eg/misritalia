<?php 
namespace App\Models\Base ;

class Vacancy extends BaseModel
{
    protected $table = 'vacancies';
    protected $guarded = ['id'];
    

    //======Default Values====== 
    protected $attributes = [
	'is_current' => 1,
    ];

    //======Rules====== 
    function rules(){
        return [ 
            'title' => 'required' , 
            'about' => 'required' , 
            'requirments' => 'required' , 
            'is_current' => 'required' , 
        ];

    }
    

    
}