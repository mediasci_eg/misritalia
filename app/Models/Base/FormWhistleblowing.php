<?php
namespace App\Models\Base ;

class FormWhistleblowing extends BaseModel
{
    protected $table = 'form_whistleblowing';
    protected $guarded = ['id'];


    //======Rules======
    function rules(){
        return [
            'subject' => 'required' ,
            'data' => 'required' ,
            'files' => 'required' ,
        ];

    }



}
