<?php
namespace App\Models\Base ;

class BoxOfHopeVolum extends BaseModel
{
    protected $table = 'box_of_hope_volums';
    protected $guarded = ['id'];
    public $timestamps = false;

    //======Rules======
    function rules(){
        return [
            'box_of_hope_id' => 'required|numeric' ,
            'title' => 'required' ,
            'description' => 'required' ,
            'date' => 'required' ,
            //'images' => 'required' ,
        ];

    }


    //======Relations======
    public function boxOfHope()
    {
        return $this->belongsTo('App\Models\BoxOfHope', 'box_of_hope_id');
    }

}
