<?php 
namespace App\Models\Base ;

class News extends BaseModel
{
    protected $table = 'news';
    protected $guarded = ['id'];
    

    //======Rules====== 
    function rules(){
        return [ 
            'title' => 'required' , 
            'content' => 'required' , 
            'image' => 'required' , 
        ];

    }
    

    
}