<?php 
namespace App\Models\Base ;

class BusinessTransformationTab extends BaseModel
{
    protected $table = 'business_transformation_tabs';
    protected $guarded = ['id'];
    

    //======Default Values====== 
    protected $attributes = [
	'has_files' => 0,
    ];

    //======Rules====== 
    function rules(){
        return [ 
            'table_id' => 'required|numeric' , 
            'title' => 'required' , 
            'content' => 'required' , 
            'has_files' => 'required' , 
        ];

    }
    

    //======Relations======
    public function businessTransformation()
    {
        return $this->belongsTo('App\Models\BusinessTransformation', 'table_id');
    }
    
}