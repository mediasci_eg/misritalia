<?php 
namespace App\Models\Base ;

class MessageOfHope extends BaseModel
{
    protected $table = 'message_of_hope';
    protected $guarded = ['id'];
    public $timestamps = false;

    

    //======Relations======
    public function tabs()
    {
        return $this->hasMany('App\Models\MessageOfHopeTab', 'message_of_hope_id');
    }
    
}