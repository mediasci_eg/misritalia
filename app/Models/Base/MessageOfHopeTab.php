<?php 
namespace App\Models\Base ;

class MessageOfHopeTab extends BaseModel
{
    protected $table = 'message_of_hope_tabs';
    protected $guarded = ['id'];
    public $timestamps = false;

    //======Rules====== 
    function rules(){
        return [ 
            'message_of_hope_id' => 'required|numeric' , 
            'title' => 'required' , 
        ];

    }
    

    //======Relations======
    public function messageOfHope()
    {
        return $this->belongsTo('App\Models\MessageOfHope', 'message_of_hope_id');
    }
    
}