<?php 
namespace App\Models\Base ;

class Member extends BaseModel
{
    protected $table = 'members';
    protected $guarded = ['id'];
    public $timestamps = false;

    //======Rules====== 
    function rules(){
        return [ 
            'name' => 'required' , 
            'position' => 'required' , 
            'image' => 'required' , 
            'team' => 'required' , 
        ];

    }
    

    
}