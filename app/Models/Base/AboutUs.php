<?php 
namespace App\Models\Base ;

class AboutUs extends BaseModel
{
    protected $table = 'about_us';
    protected $guarded = ['id'];
    

    //======Rules====== 
    function rules(){
        return [ 
            'founder_message_link' => 'required' , 
            'inspiration_link' => 'required' , 
            'board_of_directors_link' => 'required' , 
            'executive_management_link' => 'required' , 
            'management_team_link' => 'required' , 
            'newsletter_description' => 'required' , 
            'newsletter_files' => 'required' , 
        ];

    }
    

    
}