<?php 
namespace App\Models\Base ;

class LetsGrow extends BaseModel
{
    protected $table = 'lets_grow';
    protected $guarded = ['id'];
    

    //======Rules====== 
    function rules(){
        return [ 
            'descrption' => 'required' , 
            'video' => 'required' , 
        ];

    }
    

    //======Relations======
    public function tabs()
    {
        return $this->hasMany('App\Models\LetsGrowTab', 'lets_grow_id');
    }
    
}