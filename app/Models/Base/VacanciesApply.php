<?php 
namespace App\Models\Base ;

class VacanciesApply extends BaseModel
{
    protected $table = 'vacancies_apply';
    protected $guarded = ['id'];
    

    //======Rules====== 
    function rules(){
        return [ 
            'vacancy_id' => 'required|numeric' , 
            'first_name' => 'required' , 
            'last_name' => 'required' , 
            'email' => 'required' , 
            'phone' => 'required' , 
            'cv' => 'required' , 
        ];

    }
    

    //======Relations======
    public function vacancy()
    {
        return $this->belongsTo('App\Models\Vacancy', 'vacancy_id');
    }
    
}