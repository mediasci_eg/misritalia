<?php
namespace App\Models\Base ;

class Csr extends BaseModel
{
    protected $table = 'csr';
    protected $guarded = ['id'];


    //======Default Values======
    protected $attributes = [
	'has_sub' => 0,
	'has_2_titles' => 0,
    ];

    //======Rules======
    function rules(){
        return [
            'title' => 'required' ,
            'description' => 'required' ,
            'image' => 'required' ,
            'parent_id' => 'nullable|numeric' ,
            'has_sub' => 'required' ,
            'has_2_titles' => 'required' ,
        ];

    }


    //======Relations======
    public function parent()
    {
        return $this->belongsTo('App\Models\Csr', 'parent_id');
    }
    public function children()
    {
        return $this->hasMany('App\Models\Csr', 'parent_id');
    }

}
