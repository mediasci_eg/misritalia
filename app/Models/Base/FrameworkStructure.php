<?php 
namespace App\Models\Base ;

class FrameworkStructure extends BaseModel
{
    protected $table = 'framework_structure';
    protected $guarded = ['id'];
    

    //======Rules====== 
    function rules(){
        return [ 
            'title' => 'required' , 
            'content' => 'required' , 
        ];

    }
    

    
}