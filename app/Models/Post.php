<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends BaseModel
{
    protected $casts = [
        'data' => 'array',
        'images' => 'array',
    ];
    protected $fillable = ['title', 'content', 'data'];
}