<?php

namespace App\Models;

use App\Models\BaseModel;

class DevelopmentBrochure extends BaseModel {

    protected $fillable = ['brochure','type', 'title', 'development_id'];
    //=========Rules===============
    public $rules = [
        'brochure' => "required",
        'type' => "required",
        'development_id' => "required",
    ];

}
