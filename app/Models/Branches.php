<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;

class Branches extends BaseModel
{
    

    protected $fillable = ['lng','lat','address','name', 'phone' , 'email'];

    //=========Rules===============
    public $rules=[
        'name' => "required",
        //'content' => "required",
        'address' => "required",
//        'is_featured' => "required",
        'phone' => "required",
        'email' => "required",
        'lat' => "required",
        'lng' => "required",
        //'detail_image' => "required",
   
    ];


}
