<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;

class Development extends BaseModel {

    protected $fillable = ['title', 'slug', 'image', 'logo', 'overview_image','overview_video', 'overview_content', 'location_image', 'location_content', 'design_image',
        'design_content', 'unit_types_image', 'unit_types_content', 'video', 'masterplan' , 'cover_image' , 'home_image' , 'location_link'];
    //=========Rules===============
    public $rules = [
        'title' => "required",
        'image' => "required",
        'logo' => "required",
        /*'overview_image' => "required",
        'overview_content' => "required",
        'location_image' => "required",
        'location_content' => "required",
        'design_image' => "required",
        'design_content' => "required",
        'design_content' => "required",
        'unit_types_image' => "required",
        'unit_types_content' => "required",
        'lat' => "required",
        'lng' => "required",*/
    ];

    public function slider() {
        return $this->hasMany(DevelopmentSlider::class, 'development_id');
    }

    public function brochures() {
        return $this->hasMany(DevelopmentBrochure::class, 'development_id');
    }

    public function unitTypes()
    {
        return $this->hasMany(DevelopmentUnitType::class, 'development_id')->orderBy('sort');
    }
}
