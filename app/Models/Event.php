<?php

namespace App\Models;

class Event extends BaseModel
{
    protected $table = 'events';
    protected $fillable = ['title', 'sub_title', 'images', 'content', 'image', 'date'];
    protected $casts = [
        'images' => 'array',
    ];
    public $rules = [
        'title' => "required",
        //'sub_title' => "required",
        'image' => "required",
        //'date' => "required",
        'content' => "required",
    ];
}