<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends BaseModel
{
	protected $table = 'slider';
    protected $guarded = ['id'];
    
}
