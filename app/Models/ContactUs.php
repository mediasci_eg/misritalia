<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;

class ContactUs extends BaseModel
{
    use SoftDeletes;
    protected $fillable = ['phone','email','message','name','inquiry_type','development_id','request_type','is_homeowner','unit_number'];

    public function development()
    {
        return $this->belongsTo('App\Models\Development');
    }
}
