<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;

class DevelopmentsInquiries extends BaseModel
{
    use SoftDeletes;
    protected $fillable = ['first_name','last_name', 'phone' 
    , 'email' , 'message', 'development_id',
    'utm_source' , 'utm_campaign',
    'sent'
    ];
    

    public $rules = [
        'first_name' => "required",
        'last_name' => "required",
        'phone' => "required",
        'email' => "required",
        //'message' => "required",
        'development_id' => "required"
        
    ];

    public function Development()
{
    return $this->belongsTo('App\Models\Development');
}

}
