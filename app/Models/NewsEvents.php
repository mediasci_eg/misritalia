<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsEvents extends BaseModel {

    protected $table = 'news_events';
    protected $fillable = ['title', 'images' , 'video' , 'content', 'slug', 'is_featured', 'square_image','rectangle_image','detail_image', 'date', 'type'];
    protected $casts = [
        'images' => 'array',
    ];
    public $rules = [
        'title' => "required",
        //'content' => "required",
        'slug' => "required",
//        'is_featured' => "required",
        'square_image' => "required",
        'rectangle_image' => "required",
        //'detail_image' => "required",
        'date' => "required",
        'type' => "required",
    ];

}
