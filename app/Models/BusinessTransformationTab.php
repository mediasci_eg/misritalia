<?php
namespace App\Models ;

class BusinessTransformationTab extends Base\BusinessTransformationTab
{
	protected $casts =[
        'files' =>'array'
    ];
}
