<?php
function selected($option1 , $option2){
    if(is_array($option2)){
        if(in_array($option1, $option2))
            return 'selected';	
	}
    elseif($option1 == $option2)
        return 'selected';
	
}
function checked($option1 , $option2){
    if(is_array($option2)){
        if(in_array($option1, $option2))
            return 'checked';
	}
    elseif($option1 == $option2)
        return 'checked';
}
function local(){
    $whitelist = array( '127.0.0.1', '::1' );
    return in_array( $_SERVER['REMOTE_ADDR'], $whitelist);
}
function str_url($str){
	return str_replace(' ','-',$str);
}
?>