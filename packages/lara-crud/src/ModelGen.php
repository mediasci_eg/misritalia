<?php

namespace HossamAldeen\LaraCrud;

class ModelGen extends BaseGenerators{

    protected $table;
    protected $model;
    private $columns;

    public function __set($key, $value) {
        if ($key == 'table') {
            $this->table = $value;
            $this->model = Names::model($value);
        }
    }

    function check() {
        $files = $this->files();
        $list = $this->checkFiles($files);
        return $list;
    }

    function generate($file, $view=false) {
        $files = $this->files();

        if ($file == 'base'){
            if($view){
                echo '<pre>'.htmlspecialchars($files['base']).'</pre>';
                die;
            }
            else
                file_put_contents(app_path() . '/Models/Base/' . $this->model . '.php', $files['base']);            
        }
            

        if ($file == 'model'){
            if($view){
                echo '<pre>'.htmlspecialchars($files['model']).'</pre>' ;
                die;
            }
            else
                file_put_contents(app_path() . '/Models/' . $this->model . '.php', $files['model']);
        }
            
    }

    function files() {
        $data['table'] = $this->table;
        $data['model'] = $this->model;
        $this->columns = $this->columns($this->table , false , false);
        $data['relations'] = $this->relations();
		
        $data['rules'] = $this->rules();
        $data['extra'] = $this->extra();

        $files['base'] = view('LaraCrud::templates.Model.base.Model', $data)->render();
        $files['model'] = view('LaraCrud::templates.Model.Model', $data)->render();
        return $files;
    }

    function relations($table='') {
        if($table == '')
            $table = $this->table ;
        $return = '';
        $relations = parent::relations($table);
        
        $relations_arr = ['relations'=>[] , 'repeat'=>[]] ;
        foreach ($relations as $relation) {
            if ($relation['type'] == 'belongsTo') {
                $method = Names::tableSingleVar($relation['table']) ;
                if(in_array($method, $relations_arr['relations'])){
                    $repeat = $relations_arr['repeat'][$method]++;
                    $method .= $repeat; 
                }else{
                    $relations_arr['relations'][] = $method ;
                    $relations_arr['repeat'][$method] = 0 ;
                }
                $return .= '
    public function ' . $method . '()
    {
        return $this->belongsTo(\'App\Models\\' . Names::model($relation['table']) . '\', \'' . $relation['column'] . '\');
    }';
            } else {
                
                $relation_table = Names::relationMethod($relation['table'] , $this->table) ;		
                $method = Names::tablePluralVar($relation_table) ;
				
                if(in_array($method, $relations_arr['relations'])){
                    $repeat = $relations_arr['repeat'][$method]++;
                    $method .= $repeat; 
                }else{
                    $relations_arr['relations'][] = $method ;
                    $relations_arr['repeat'][$method] = 0 ;
                }
                $return .= '
    public function ' .$method. '()
    {
        return $this->hasMany(\'App\Models\\' . Names::model($relation['table']) . '\', \'' . $relation['column'] . '\');
    }';
            }
        }
        
        return $return;
    }

    function rules() {
        $fields = [];
        $return = '';
        $hasUnique = false;
        $escape = ['id', 'created_at', 'updated_at' , 'deleted_at'];
        //dd($this->columns);
        foreach ($this->columns as $column) {
            if (in_array($column->Field, $escape))
                continue;
            $isUnique = false;
            $rules = [];
            preg_match('/(\w+)\((\d+)\)/', $column->Type, $matches);

            if ($column->Null == 'NO')
                $rules[] = 'required';
            else
                $rules[] = 'nullable';
            if (isset($matches[1]) && $matches[1] == 'int')
                $rules[] = 'numeric';
            if ($column->Key == 'UNI') {
                $rules[] = '\Illuminate\Validation\Rule::unique(\'' . $this->table . '\')->ignore($this->id)';
                $isUnique = true;
                $hasUnique = true;
            }
            //if($column->Field == 'image' or $column->Field == 'img')
            //$rules[] = 'image' ;
        if(count($rules)==1 && $rules[0]=='nullable')
            unset($rules[0]);
            
        if (!empty($rules)) {
            if ($isUnique) {
                $fields[$column->Field] = '[';
                foreach ($rules as $rule) {
                    if (!strstr($rule, 'Rule'))
                        $rule = "'" . $rule . "'";
                    $fields[$column->Field] .= '
                    ' . $rule . ' , ';
                }
                $fields[$column->Field] .= '
            ]';
            } else
                $fields[$column->Field] = '\'' . implode('|', $rules) . '\'';
        }
        }
        if (!empty($fields)) {


            $return = ' 
    function rules(){
        return [ ';
        foreach ($fields as $field => $rule) {
            $return .= '
            \'' . $field . '\' => ' . $rule . ' , ';
        }
        $return .= "
        ];

    }";
        }
        return $return;
    }

    function extra() {
        $return = '';
		
        // timestamp 
        $timestamp = ['created_at', 'updated_at'];
        $hasTimestamp = false;
        $hasDefault = false;
        $hasDeletedAt = false;
        $return = '';
        
        foreach ($this->columns as $column) {
            if($column->Field=='deleted_at'){
                $hasDeletedAt = true ;
            }
            if($column->Default!=NULL){
                $hasDefault = true ;
                $defaults[] = [
                    'field'=>$column->Field,
                    'value'=>$column->Default,
                ];
            }
            if (in_array($column->Field, $timestamp)) {
                $hasTimestamp = true;
            }
        }
		
        if ($hasDeletedAt)
            $return .= 'use \Illuminate\Database\Eloquent\SoftDeletes;
    ';
		
        if ($hasTimestamp == false)
            $return .= 'public $timestamps = false;';
		
		if ($hasDefault){
			$return .='

    //======Default Values====== 
    protected $attributes = [';
                foreach($defaults as $default)
				$return .='
	\''.$default['field'].'\' => '.$default['value'].',';
			$return .='
    ];';
			
		}
            
        return $return;
    }

    //=====================


    private function checkFiles($files) {
        $return = [];
        $filepath['model'] = app_path() . '/Models/' . $this->model . '.php';
        $filepath['base'] = app_path() . '/Models/Base/' . $this->model . '.php';
        foreach ($files as $file => $content) {
            if (!file_exists($filepath[$file])) {
                $return[$file] = 'new';
            } elseif (file_get_contents($filepath[$file]) == $content) {
                $return[$file] = 'not changed';
            } else {
                $return[$file] = 'changed';
            }
        }
        return $return;
    }


}

?>