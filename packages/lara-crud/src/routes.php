<?php
Route::any('laracrud','HossamAldeen\LaraCrud\GenerateController@index');
Route::any('laracrud/model','HossamAldeen\LaraCrud\GenerateController@model');
Route::any('laracrud/viewmodel','HossamAldeen\LaraCrud\GenerateController@viewmodel');
Route::any('laracrud/crud','HossamAldeen\LaraCrud\GenerateController@crud');
Route::any('laracrud/viewcrud','HossamAldeen\LaraCrud\GenerateController@viewcrud');
?>