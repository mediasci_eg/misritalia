<?php

namespace HossamAldeen\LaraCrud;

use Illuminate\Support\ServiceProvider;

class LaraCrudServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';
		include __DIR__.'/helpers/helpers.php';

        $this->app->make('HossamAldeen\LaraCrud\GenerateController');
        $this->loadViewsFrom(__DIR__.'/views', 'LaraCrud');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
