<?php

namespace HossamAldeen\LaraCrud;

use App\Http\Controllers\Controller;

class GenerateController extends Controller {

    function __construct() {
		if (!request()->is('laracrud/*'))return;
        if (request()->has('form') && !is_array($_POST['form'])) {
            $_POST['form'] = json_decode($_POST['form'], true);
        }
        if (!request()->has('form')) {
            request()->merge(['form' => []]);
            $_POST['form'] = [];
        }

        if (request()->has('index') && !is_array($_POST['index'])) {
            $_POST['index'] = json_decode($_POST['index'], true);
        }
        
        if (!request()->has('index')) {
            request()->merge(['index' => []]);
            $_POST['index'] = [];
        }
        
        if (request()->has('relatedTables') && !is_array($_POST['relatedTables'])) {
            $_POST['relatedTables'] = json_decode($_POST['relatedTables'], true);
        }

        if (request()->has('crud') && !is_array($_POST['crud'])) {
            $_POST['crud'] = json_decode($_POST['crud'], true);
        }

        if (request()->has('columnsTypes') && !is_array($_POST['columnsTypes'])) {
            $_POST['columnsTypes'] = json_decode($_POST['columnsTypes'], true);
        }
        
    }

    public function index() {
        return view('LaraCrud::index');
    }

    public function crud() {
        $data['tables'] = array_map('reset', \DB::select('SHOW TABLES'));
        if (request()->method() == 'POST') {
            $crudGen = new CRUDGen;
            $crudGen->table = $_POST['table'];
            if (request()->has('form'))
                $crudGen->form = $_POST['form'];

            if (request()->has('index'))
                $crudGen->index = $_POST['index'];

            if (request()->has('template'))
                $crudGen->template = $_POST['template'];

            if (request()->has('columnsTypes')) 
                $crudGen->columnsTypes = $_POST['columnsTypes'];
            
            if (request()->has('relatedTables')) 
                $crudGen->relatedTables = $_POST['relatedTables'] ;
			
			if (request()->has('crud')) 
                $crudGen->crud = $_POST['crud'] ;
        }

        if (isset($_POST['checkOptions'])) {
            $data['file'] = $this->getOptions($_POST['table']);
			$data['types'] = $crudGen->crudTypes();
            $data['columns'] = $crudGen->columns($_POST['table']);
			
			if(isset($data['file']->columnsTypes))
				$data['columnsTypes'] = $data['file']->columnsTypes->{$_POST['table']};
			else
				$data['columnsTypes'] = $crudGen->crudColumnsTypes($data['columns']);
			
            if($crudGen->template == 'multi'){
                $data['relatedTables'] = $crudGen->relatedTables($_POST['table']);
            }
        } elseif (isset($_POST['check'])) {
            $this->setOptions($_POST['table']);
            $crudGen->check();
            $data['list'] = $crudGen->check();
        } elseif (isset($_POST['view'])) {
            $crudGen->files = [$_POST['view']];
            $crudGen->viewResult = true;
            $crudGen->generate();
        } elseif (isset($_POST['generate'])) {
            $crudGen->files = array_keys($_POST['files']);
            $crudGen->generate();
            $data['link'] = url('backend/' . $_POST['table']);
        }


        return view('LaraCrud::crud', $data);
    }

    public function model() {
        $data['tables'] = array_map('reset', \DB::select('SHOW TABLES'));
        if (isset($_POST['checkall'])) {
            $modelGen = new ModelGen;
            foreach ($data['tables'] as $table) {
                $modelGen->table = $table;
                $data['lists'][$table] = $modelGen->check();
            }
        }
        if (isset($_POST['check'])) {
            $modelGen = new ModelGen;
            $modelGen->table = $_POST['table'];
            $data['lists'][$_POST['table']] = $modelGen->check();
        } elseif (isset($_POST['generate'])) {
            $modelGen = new ModelGen;
            foreach ($_POST['files'] as $table => $files) {
                $modelGen->table = $table;

                if (isset($files['base']))
                    $modelGen->generate('base');

                if (isset($files['model']))
                    $modelGen->generate('model');
            }
        }


        return view('LaraCrud::model', $data);
    }

    public function viewmodel() {
        $modelGen = new ModelGen;
        $modelGen->table = $_GET['table'];
        if ($_GET['file'] == 'base')
            $modelGen->generate('base', true);

        if ($_GET['file'] == 'model')
            $modelGen->generate('model', true);
    }

    public function viewcrud() {
        $modelGen = new CRUDGen;
        $modelGen->table = $_GET['table'];
        if ($_GET['file'] == 'base')
            $modelGen->generate('base', true);

        if ($_GET['file'] == 'model')
            $modelGen->generate('model', true);
    }

    private function getOptions($table) {
        $file = [];
        if (file_exists(__dir__ . './sessions/' . $_POST['table']))
            $file =  json_decode(file_get_contents(__dir__ . './sessions/' . $_POST['table']));
		return $file;
    }

    private function setOptions($table) {
        if (!is_dir(__dir__ . './sessions'))
            mkdir(__dir__ . './sessions');
		$saved = [
		'form' => @$_POST['form'], 
		'index' => @$_POST['index'] ,
		'columnsTypes' => @$_POST['columnsTypes'] ,
		'crud' => @$_POST['crud'] ,
		'relatedTables'=>@$_POST['relatedTables'] ] ;
        file_put_contents(__dir__ . './sessions/' . $table, json_encode($saved));
    }

}

?>