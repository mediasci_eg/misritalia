@extends('LaraCrud::layout')
@section('content')
<h1 style='padding-left: 15px;' >Model Generator</h1>
<div class='row' style='max-width: 100%;'>
    <form  method='post'>
        <div class='col-md-6' >
            <div class='col-md-6' >
                <select name='table' class='form-control'  >
                    <option>Choose Your Table</option>
                    @foreach($tables as $table)
                    <option <?= (@$_POST['table'] == $table) ? 'selected' : '' ?> >{{$table}}</option>
                    @endforeach
                </select>
            </div>
            <div class='col-md-6' >
                <input type='submit' name='check' value='check' class='btn btn-primary'>
                <input type='submit' name='checkall' value='check all' class='btn btn-default'>
            </div>
            @if(isset($lists))
            <div class='clearfix' ></div><br>	
            <div class='col-md-12' >
                <table class='table table-borderd' >
                    <tr>
                        <th>File</th>
                        <th>Status</th>
                        <th>check</th>
                        <th>view file</th>
                    </tr>
                    @foreach($lists as $table=>$list)
                    <?php
                    $files = ['base', 'model'];
                    $color = $checked = $hide = ['base' => '', 'model' => ''];

                    foreach ($files as $file) {
                        if ($list[$file] == 'changed')
                            $color[$file] = '#ffd78e';
                        elseif ($list[$file] == 'new') {
                            $color[$file] = '#bdf5bd';
                            $checked[$file] = 'checked';
                        } else
                            $hide[$file] = 'hide';
                    }
                    ?>

                    <tr>
                        <th colspan='4' style='background:#e0f1ff;text-align: center;' >{{ucfirst($table)}}</th>
                    </tr>
                    <tr style='background:{{$color['base']}}' >
                        <td>Base</td>
                        <td>{{$list['base']}}</td>
                        <td><input class='{{$hide['base']}}' type='checkbox' {{$checked['base']}} value='1' name='files[{{$table}}][base]' ></td>
                        <td>
                            <a href='{{url('laracrud/viewmodel/?table='.$table.'&file=base')}}' target="_blank" >view file</a>
                        </td>
                    </tr>
                    <tr style='background:{{$color['model']}}' >
                        <td>Model</td>
                        <td>{{$list['model']}}</td>
                        <td>
                            <input class='{{$hide['model']}}' type='checkbox' {{$checked['model']}} value='1' name='files[{{$table}}][model]' >
                        </td>
                        <td>
                            <a href='{{url('laracrud/viewmodel/?table='.$table.'&file=model')}}' target="_blank">view file</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            @endif
            <div class='clearfix' ></div><br>
            <div class='col-md-12' >
                @if(isset($_POST['check']) || isset($_POST['checkall']) )
                <input type='submit' name='generate' value='generate' class='btn btn-success' >
                @endif
            </div>
        </div>


    </form>
</div>
@stop