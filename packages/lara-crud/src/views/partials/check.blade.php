<table class='table table-borderd' >
    <tr>
        <th>File</th>
        <th>Status</th>
        <th>check</th>
    </tr>
    @foreach($list as $file=>$status)
    <?php $color = $checked = $hide = ''; ?>
    @if($status == 'changed')
    <?php $color = '#ffd78e'; ?>	
    @elseif($status == 'new')
    <?php
    $color = '#bdf5bd';
    $checked = 'checked';
    ?>	
    @else
    <?php $hide = 'hide'; ?>
    @endif
    <tr style='background:{{$color}}' >
        <td>{{$file}}</td>
        <td>{{$status}}</td>
        <td><input class='{{$hide}}' type='checkbox' {{$checked}} value='1' name='files[{{$file}}]' ></td>
        <td>
            <button type='submit' name='view' value='{{$file}}' onclick="$('form').attr('target', '_blank')" >view file</button>
        </td>
    </tr>
    @endforeach
</table>
<input type='hidden' name='form' value="{{json_encode($_POST['form'])}}">
<input type='hidden' name='crud' value="{{json_encode($_POST['crud'])}}">
<input type='hidden' name='index' value="{{json_encode($_POST['index'])}}">
<input type='hidden' name='columnsTypes' value="{{json_encode($_POST['columnsTypes'])}}">
@isset($_POST['relatedTables'])
<input type='hidden' name='relatedTables' value="{{json_encode($_POST['relatedTables'])}}">
@endisset