@if($crud)
<div class='col-md-12 row' >
	<div class='col-md-12'>
		<h3>CRUD </h3>
	</div>
	<div class='col-md-2' style='font-weight: bold;'>
		<input type='hidden' name='crud[create]' value='0'>
		<input type='checkbox' name='crud[create]' <?= (!isset($file->crud) || $file->crud->create ==1 ) ? 'checked' : '' ?> value='1'>
		Create
	</div>
	
	<div class='col-md-2' style='font-weight: bold;'>
		<input type='hidden' name='crud[update]' value='0'>
		<input type='checkbox' name='crud[update]' <?= (!isset($file->crud) || $file->crud->update ==1 ) ? 'checked' : '' ?> value='1'>
		Update
	</div>
	
	<div class='col-md-2' style='font-weight: bold;'>
		<input type='hidden' name='crud[view]' value='0'>
		<input type='checkbox' name='crud[view]' <?= (isset($file->crud) && $file->crud->view ==1 ) ? 'checked' : '' ?> value='1'>
		View
	</div>
	
	<div class='col-md-2' style='font-weight: bold;'>
		<input type='hidden' name='crud[delete]' value='0'>
		<input type='checkbox' name='crud[delete]' <?= (!isset($file->crud) || $file->crud->delete ==1 ) ? 'checked' : '' ?> value='1'>
		Delete
	</div>
	
</div>
@endif
<div class='clearfix' ></div>
<div class='col-md-12' >
    <h3>Columns </h3>
    <table class='table table-borderd' >
        <tr>
            <th>Form</th>
            <th>Index</th>
            <th>Column</th>
            <th>Type</th>
            <th>Relation</th>
            <th>Relation Column</th>
        </tr>

        <?php
        foreach ($columns as $column) {
            ?>
            <tr>
                <th><input name='form[{{$table}}][]' type='checkbox' value='{{$column->Field}}' <?= (@in_array($column->Field, $file->form->{$table}) || !isset($file->form->{$table})) ? 'checked' : '' ?> /></th>
                <th><input name='index[{{$table}}][]' type='checkbox' value='{{$column->Field}}' <?= (@in_array($column->Field, $file->index->{$table}) || !isset($file->index->{$table})) ? 'checked' : '' ?> /></th>
                <th>{{$column->Field}}</th>
                <th>
                    <select class='form-control' name='columnsTypes[{{$table}}][{{$column->Field}}][type]' >
                        @foreach($types as $type)
                            <option <?= selected(@$columnsTypes->{$column->Field}->type , $type)?> >{{$type}}</option>
                        @endforeach
                    </select>
                    @if(@$columnsTypes->{$column->Field}->type == 'select')
                        <span style='font-size:12px' >Values: </span><input type='text' name='columnsTypes[{{$table}}][{{$column->Field}}][values]' value='{{$columnsTypes->{$column->Field}->values}}' class='form-control' />
                    @endif
                </th>

                @if(isset($column->relation))
                <th>{{$column->relation->table}}
                    <input type='hidden' name='columnsTypes[{{$table}}][{{$column->Field}}][relation][table]' value='{{$column->relation->table}}'>
                </th>
                <th>
                    <select class='form-control' name='columnsTypes[{{$table}}][{{$column->Field}}][relation][column]' >
                        @foreach($column->relation->columns as $relColumn)
                            <option>{{$relColumn->Field}}</option>
                        @endforeach
                    </select>
                </th>
                @endif	
            </tr>	
<?php } ?>
    </table>
</div>
