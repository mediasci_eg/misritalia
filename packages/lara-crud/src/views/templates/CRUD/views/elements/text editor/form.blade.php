                        
                    <?php 
                        use HossamAldeen\LaraCrud\Names;
                    ?>
                    <div class="form-group col-md-6">
                        <label for="{{$model}}{{ucfirst($column)}}">{{Names::column($column)}}</label>
                        <textarea name='{!!$name!!}' class="form-control editor" id="{{$model}}{{ucfirst($column)}}" placeholder="Enter {{Names::column($column)}}"><?='{!!'.$value.'!!}'?></textarea>
                    </div>
@push('footer')
{{'@'}}include('back.partials.texteditor.frola')
@endpush