<?php 
use HossamAldeen\LaraCrud\Names;
$relation = $columns[$table][$column]['relation'] ;
?>
                            <?= '<?php $'.Names::tablePluralVar($relation['table']).'='.'\App\Models\\'.Names::model($relation['table']).'::all() ?>' ?>
						
                            <th>
                                <select name='{{$column}}' class="form-control" >
                                    <option value='' >all</option>
                                    <?='@foreach($'.Names::tablePluralVar($relation['table']).' as $'.Names::tableSingleVar($relation['table']).')'?>

                                        <option <?='<?=selected($'.Names::tableSingleVar($relation['table']).'->id , request(\''.$column.'\') )?>'?> value='<?='{{$'.camel_case(str_singular($relation['table'])).'->id}}'?>' ><?='{{$'.camel_case(str_singular($relation['table'])).'->'.$relation['column'].'}}'?></option>
                                    <?='@endforeach'?>
                                            
                                </select>
                            </th>