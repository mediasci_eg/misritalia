                        
                    <?php 
                        use HossamAldeen\LaraCrud\Names;
                        $relation = $columns[$column]['relation'] ;
                    ?>
                    <div class="form-group col-md-6">
                        <?= '<?php $'.Names::tablePluralVar($relation['table']).'='.'\App\Models\\'.Names::model($relation['table']).'::all() ?>' ?>
                        
                        <label for="{{$model}}{{ucfirst($column)}}">{{Names::column($column)}}</label>
                        <select name='{!!$name!!}' class="form-control" id="{{$model}}{{ucfirst($column)}}">
                            <option value='' >Choose {{Names::tableSingle($relation['table'])}}</option>
                            <?='@foreach($'.Names::tablePluralVar($relation['table']).' as $'.Names::tableSingleVar($relation['table']).')'?>

                                <option <?='<?=selected($'.Names::tableSingleVar($relation['table']).'->id , '.$value.')?>'?> value='<?='{{$'.Names::tableSingleVar($relation['table']).'->id}}'?>' ><?='{{$'.Names::tableSingleVar($relation['table']).'->'.$relation['column'].'}}'?></option>
                            <?='@endforeach'?>

                        </select>
                    </div>