                    <?php 
                        use HossamAldeen\LaraCrud\Names;
                    ?>
                    <div class="form-group col-md-6">
                        <label for="{{$model}}{{ucfirst($column)}}">{{Names::column($column)}}</label>
                        <input type="file" data-name='{!!$name!!}[]' class="form-control images" id="{{$model}}{{ucfirst($column)}}"  >	
                        <ul class='preview-container sortable' >
                            {{'@'}}if($model->{{$column}} != '')
                                {{'@'}}foreach(explode(',', <?=$value?>) as $image) 
                                <li class='image_container' draggable="true" ondragenter="dragenter(event)" ondragstart="dragstart(event)" >
                                    <a href='#' class='delete_image' >x</a>
                                    <input type="hidden" name="{!!$name!!}[]"  value="<?='{{$image}}'?>" />
                                    <img  style="width:100px;margin:0 3px" src="<?='{{url(\'uploads/\'.$image)}}'?>">
                                </li>
                                {{'@'}}endforeach
                            {{'@'}}endif
                        </ul>      
                    </div>
@push('footer')
{{'@'}}push('footer')
<script>
    function uploadImage(selector) {
        var file_data = selector.prop("files")[0];

        var form_data = new FormData();
        form_data.append("file", file_data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').val()
            },
            type: 'post',
            url: "<?='{{url(\'backend/home/upload-image\')}}'?>",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            beforeSend: function () {
                selector.parent().parent().find(".uploading").removeClass('hide');
            },
            complete: function (result) {
                var content = $.parseJSON(result.responseText);
                if (content.status == 0)
                {
                    alert('Error can\'t upload the image');
                    return;
                }

                var element = selector.parent().parent().find('.preview-container');

                if (typeof selector.attr('data-name') != 'undefined')
                    name = selector.attr('data-name');
                else
                    name = 'images[]';
                var text = '\n\
                <li class="image_container" >\n\
                        <a href="#" class="delete_image" >x</a>\n\
                        <img style="width:100px;margin:0 3px" id=""  src="<?='{{url(\'uploads\')}}'?>/' + content.file + '" />\n\
                        <input type="hidden" name="' + name + '"  value="' + content.file + '" />\n\
                </li> \n\
                ';

                if (typeof element.attr('data-hasOne') == 'undefined')
                    element.append(text).show();
                else
                    element.html(text).show();
            }
        });
    }
    $(document).ready(function () {
        //--------------------------
        $(document).on("change", "input:file.images", function () {
            uploadImage($(this));
        });
        $(document).on('click', '.delete_image', function (e) {
            e.preventDefault();
            var id = $(this).parent().find('img').attr('id');
            if (id > 0)
            {
                $('form').append("<input type='hidden' name='deleted_images[]' value='" + id + "' >");
            }
            $(this).parent().remove();
        });
    });
        //--------------------------
</script>
{{'@'}}endpush

@endpush