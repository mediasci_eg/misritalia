                        
                    <?php 
                        use HossamAldeen\LaraCrud\Names;
                    ?>
                    <div class="form-group col-md-6">
                        <label for="{{$model}}{{ucfirst($column)}}">{{Names::column($column)}}</label>
                        <select name='{!!$name!!}' class="form-control" id="{{$model}}{{ucfirst($column)}}">
                            <option value='' >Choose {{Names::column($column)}}</option>
@foreach(explode(',' , $columns[$column]['values']) as $element)
                            <option <?='<?=selected(\''.$element.'\' , '.$value.')?>'?> value='{{$element}}' >{{ucfirst($element)}}</option>
@endforeach
                        </select>
                    </div>