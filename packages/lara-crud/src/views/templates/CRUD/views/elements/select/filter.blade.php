                                <th>
                                    <select name='{{$column}}' class="form-control" >
                                        <option value='' >All</option>
                                @foreach(explode(',' , $columns[$table][$column]['values']) as $value)
                                        <option <?='<?=selected( \''.$value.'\' , request(\''.$column.'\') )?>'?> value='{{$value}}' >{{ucfirst($value)}}</option>
                                @endforeach
                                    </select>
                                </th>
