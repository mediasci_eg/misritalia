                        
                    <?php 
                        use HossamAldeen\LaraCrud\Names;
                    ?>
                    <div class="form-group col-md-6">
                        <label for="{{$model}}{{ucfirst($column)}}">{{Names::column($column)}}</label>
                        <input type="number" name='{!!$name!!}' class="form-control" id="{{$model}}{{ucfirst($column)}}" placeholder="Enter {{Names::column($column)}}" value='<?='{{'.$value.'}}'?>'>
                    </div>