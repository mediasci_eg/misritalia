<?php 
use HossamAldeen\LaraCrud\Names;
?>
<table class='table table-striped'>
@foreach($columns[$table] as $column=>$element)
	<tr>
	  <th>{{Names::column($column)}}</th>
@if(isset($columns['relation']))
<?php $relation = $element['relation'] ?>
	  <td><?='{{$model->'.Names::tableSingleVar($relation['table']).'->'.$relation['column'].'}}'?></td>
@elseif($element['type']=='image')
	  <td><img src=<?='{{url(\'uploads/\'.$model->'.$column.')}}'?> style='width:100px' ></td>
@elseif($element['type']=='checkbox')
	  <td>
		{{'@'}}if(<?='$model->'.$column?>=='1')
                    <i class="fa fa-check"></i>
		{{'@'}}else
                    <i class="fa fa-close"></i>
		{{'@'}}endif
	   </td>
@else
	   <td><?='{{$model->'.$column.'}}'?></td>
@endif
	</tr>
@endforeach
</table>
  
  
