<?php 
use HossamAldeen\LaraCrud\Names;
?>
{{'@'}}extends('backend.layout.master')
{{'@'}}if($model->exists)
	{{'@'}}section('title' , 'Update {{Names::tableSingle($table)}} #'.$model->id)	
{{'@'}}else
	{{'@'}}section('title' , 'Create {{Names::tableSingle($table)}}')
{{'@'}}endif

{{'@'}}section('content')
<div class="card">
    <div class="card-content collapse show">
        <div class="card-body card-dashboard">
            <form method='post' <?=($hasImage)?'enctype="multipart/form-data"':''?> >
                <div class="row">
                    <?='{{ csrf_field() }}'?>		
                    @include('LaraCrud::templates.CRUD.views._form' , [
                        'form'=>$form[$table] ,
                        'columns'=>$columns[$table] ,
                    ] )
@isset($relatedTables)
@foreach($relatedTables as $relatedTable=>$type)

<?php 
if($type=='one_to_one')
    $section = Names::tableSingle($relatedTable) ;
else
    $section = Names::tablePlural($relatedTable) ;

$method = Names::relationMethod($relatedTable , $table) ;
$method = Names::tablePluralVar($method) ;
?>
                    <section class='{{Names::tablePluralVar($relatedTable)}} w-100'>
                        <div class="col-md-12">
                            <h3>{{$section}}</h3>
                        </div>
                        <?='<?php'?> 
                            $count['{{Names::tablePluralVar($relatedTable)}}'] = 0 ;
                            if(!$model->exists)
                                ${{Names::tablePluralVar($relatedTable)}} = [new \App\Models\{{Names::model($relatedTable)}}];
                            else
                                ${{Names::tablePluralVar($relatedTable)}} = $model->{{$method}};
                        <?='?>'?> 
                        {{'@'}}foreach(${{Names::tablePluralVar($relatedTable)}} as ${{Names::tableSingleVar($relatedTable)}})
                        <?='<?php'?> $count['{{Names::tablePluralVar($relatedTable)}}']++ <?='?>'?>
                        
                        @include('LaraCrud::templates.CRUD.views._form' , [
                            'form'=>$form[$relatedTable] ,
                            'columns'=>$columns[$relatedTable] ,
                            'type'=>$type,
                            'table'=>$relatedTable ,
                            'model'=>Names::model($relatedTable),
                        ] )
                        
                        {{'@'}}endforeach
                    </section>
@endforeach
@endisset
                    
                    <div class="col-md-12">
                        <input type='submit' value='Save' name='save' class='btn btn-primary' >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stack('footer')

{{'@'}}if(isset($errors))
    {{'@'}}push('footer')
        <?='{!!$model->viewErrors()!!}'?>	
    {{'@'}}endpush
{{'@'}}endif


{{'@'}}stop


@isset($relatedTables)
{{'@'}}push('footer')
<script>
    $(document).ready(function(){
        count = <?='<?='?>json_encode($count)<?='?>'?> ;
        $('.add').click(function(){
            var content = $(this).parents('.section_content')[0].outerHTML+'\n\
                                <div class="clearfix" ></div>';
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $section.append(content);
            var $lastItem = $section.find('.section_content').last();
            $lastItem.find('input , select , textarea').each(function(){
                $(this).val('');
                var name = $(this).attr('name');
                if(typeof name!='undefined'){
                    var newName = name.replace(/\d+/ ,count[$section.attr('class')] );
                    $(this).attr('name' , newName);
                }
            })    
            
            var $lastItemBtn = $lastItem.find('.add');            
            $lastItemBtn.val('X');
            $lastItemBtn.removeClass('add');
            $lastItemBtn.removeClass('btn-success');
            $lastItemBtn.addClass('btn-danger');
            $lastItemBtn.addClass('remove');
        })
        
        $(document).on('click','.remove' , function(){
            var $section = $(this).closest('section');
            count[$section.attr('class')]++;
            $(this).closest('.section_content').remove();
        })
    })
   
</script>
{{'@'}}endpush
@endisset

{{'@'}}section('breadcrumbs')
    <li class="breadcrumb-item" ><a href="<?='{{url(\'backend/'.$table.'\')}}'?>">{{Names::tablePlural($table)}}</a></li>
    {{'@'}}if($model->exists)
        <li class="breadcrumb-item active">Update</li>
    {{'@'}}else
        <li class="breadcrumb-item active">Create</li>
    {{'@'}}endif
{{'@'}}stop