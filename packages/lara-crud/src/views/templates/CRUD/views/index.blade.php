<?php 
use HossamAldeen\LaraCrud\Names;
?>
{{'@'}}extends('backend.layout.master')
@if($hasDeletedAt)
{{'@'}}section('title')
    {{'@'}}if(!isset($deleted))
    {{ucfirst(str_replace('_',' ', $table))}}
    {{'@'}}else
    Deleted {{ucfirst(str_replace('_',' ', $table))}}
    {{'@'}}endif
{{'@'}}stop

@else
{{'@'}}section('title' , '{{ucfirst(str_replace('_',' ', $table))}}')
@endif
{{'@'}}section('content')


<div class="card">
    <div class="card-header">
        <div class="btn-group float-md-right">
@if($crud['create']==1)
            <a href="<?='{{url(\'backend/'.$table.'/create\')}}'?>" class='btn btn-info' ><i class='fa fa-plus mr-1'></i>Create</a>
@endif
@if($hasDeletedAt)
            {{'@'}}if(!isset($deleted))
                    <a href='<?='{{url(\'backend/'.$table.'/deleted\')}}'?>' class='btn btn-warning' >Deleted</a>
            {{'@'}}else
                    <a href='<?='{{url(\'backend/'.$table.'\')}}'?>' class='btn btn-primary' >{{$table}}</a>
            {{'@'}}endif
@endif
@if($hasSort)
            <a data-toggle="modal" data-target="#sort" class='btn btn-primary active' >Sort</a>
@endif
        </div>
    </div>
    <div class="card-content overflow-auto collapse show">
        <div class="card-body card-dashboard">
                <form method='get' class='search' >
                    <table class="table table-striped table-bordered zero-configuration">
                        <tr>
                            <th style="min-width: 65px">#</th>
@foreach($index[$table] as $column)
                            <th>{{Names::column($column) }}</th>
@endforeach
                            <th style="width: 65px">Actions</th>
                        </tr>
                        <tr>			
                            <th><input type='text' name='id' value='<?='{{request(\'id\')}}'?>' class='form-control'></th>
@foreach($index[$table] as $column)
@if(view()->exists('LaraCrud::templates.CRUD.views.elements.'.$columns[$table][$column]['type'].'.filter'))
                        @include('LaraCrud::templates.CRUD.views.elements.'.$columns[$table][$column]['type'].'.filter')
@else

                            <th></th>
@endif
@endforeach
                            <th style="width: 65px">
                                
                                <input type='submit' class='d-none' name='search'>
                            </th>
						
                        </tr>
                        {{'@'}}if($data->isNotEmpty())
                            {{'@'}}foreach($data as $row)
                                <tr>
                                    <td><?='{{$row->id}}'?></td>
@foreach($index[$table] as $column)

@if(view()->exists('LaraCrud::templates.CRUD.views.elements.'.$columns[$table][$column]['type'].'.index'))
                                    @include('LaraCrud::templates.CRUD.views.elements.'.$columns[$table][$column]['type'].'.index')
@else

                                    <td><?='{{$row->'.$column.'}}'?></td>
@endif
@endforeach
                                    <td>
@if($hasDeletedAt)
                                            {{'@'}}if(!isset($deleted))
@endif
@if($crud['view']==1)
                                            <a href="javascript:void" title='View' class='view' data-id='<?='{{$row->id}}'?>' >
                                                <i class="fa fa-lg fa-eye"></i> 
                                            </a>
@endif
@if($crud['update']==1)
                                            <a href="<?='{{url(\'backend/'.$table.'/update/\'.$row->id.\'\')}}'?>" title='Edit'>
                                                <i class="fa fa-lg fa-edit"></i> 
                                            </a>
@endif
@if($crud['delete']==1)
                                            <a href="<?='{{url(\'backend/'.$table.'/delete/\'.$row->id.\'\')}}'?>" title='Delete' onclick="return confirm('Are you sure you want to delete ?')" >
                                                <i class="fa fa-lg fa-remove"></i> 
                                            </a>
@endif
@if($hasDeletedAt)
                                            {{'@'}}else
                                                <a href="<?='{{url(\'backend/'.$table.'/restore/\'.$row->id.\'\')}}'?>" title='Restore' >
                                                    <i class="fa fa-lg fa-reply"></i> 
                                                </a>
                                            {{'@'}}endif
@endif
                                    </td>
                                </tr>
                            {{'@'}}endforeach
                        {{'@'}}else
                            <tr>
                                <td class='text-center' colspan='100'>No Data Available</td>
                            </tr> 
                        {{'@'}}endif
                    </table>
                </form>
            
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <?='{{ $data->appends(request()->except(\'page\'))->links() }}'?>
			
            </div>
        </div>
    </div>
</div>
@if($hasSort)
<div class="modal fade" id="sort" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="post" action='./backend/{{kebab_case($controller)}}/sort'>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <ul class="sortable">
                  {{'@'}}foreach($data as $row)
                        <?='{{ csrf_field() }}'?>
                        <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                            <?='{{$row->'.$titleColumn.'}}'?>
                            <input type='hidden' name='id[]' value='<?='{{$row->id}}'?>'>
                        </li>
                  {{'@'}}endforeach
              </ul>
              <style>
                  .sortable {
                      padding: 0;
                  }
                  .sortable li{
                      list-style: none;
                      cursor: pointer;				
                  }
              </style>
            </div>
            <div class="modal-footer">
              <button type="submit" name='sort' class="btn btn-primary">Sort</button>
            </div>
        </form>
    </div>
  </div>
</div>

@endif
{{'@'}}stop

{{'@'}}push('footer')
@if($hasSort)
   <script src="<?='{{url(\'admin/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js\')}}"'?> ></script>
   <script>
        $( function() {
            $( ".sortable" ).sortable();
        } );
   </script>
@endif
    <script>
        $('.search input , .search select').change(function(){
            $('.search input[type=submit]').trigger('click');
        });
        $('.view').click(function(){
            var id=$(this).data().id;
            $.ajax({
                url:"<?='{{url(\'backend/'.$table.'/view\')}}'?>/"+id,
                success:function(data){
                    $('#modal').find('.modal-title').html('View {{ucfirst(str_singular($table))}} #'+id);
                    $('#modal').find('.modal-body').html(data);
                    $('#modal').modal();
                }
            })
        })
    </script>
{{'@'}}endpush

{{'@'}}section('breadcrumbs')
@if($hasDeletedAt)
    {{'@'}}if(!isset($deleted))    
        <li class="breadcrumb-item active">{{Names::tablePlural($table)}}</li>
    {{'@'}}else
        <li class="breadcrumb-item "><a href='<?='{{url(\'backend/'.$table.'\')}}'?>'>{{Names::tablePlural($table)}}</a></li>
        <li class="breadcrumb-item active">Deleted {{Names::tablePlural($table)}}</li>
    {{'@'}}endif
@else
    <li class="breadcrumb-item active">{{Names::tablePlural($table)}}</li>
@endif
{{'@'}}stop

@if($hasSort)
{{'@'}}push('header')
    <link rel="stylesheet" href="<?='{{url(\'admin/app-assets/vendors/css/ui/jquery-ui.min.css\')}}"'?>">
{{'@'}}endpush
@endif