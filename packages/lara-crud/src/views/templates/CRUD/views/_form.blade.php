<?php 
use HossamAldeen\LaraCrud\Names;
?>

@if(isset($type) && $type == 'one_to_many')
<?php $col = (count($form)>1)?'10 row':'' ?>
<?php $row = (count($form)>1)?'row pl-1':'' ?>

                <div class='section_content {{$row}}'>
                    <div class='col-md-{{$col}}' >
@endif

<?php
foreach($form as $column){

    $value = '$model->'.$column ;
    if($template =='single')
        $name = $model.'[{{$model->id}}]['.$column.']' ;
    elseif(isset($type)){
        $name = $model.'[{{$count[\''.Names::tablePluralVar($table).'\']}}]['.$column.']' ;
        $value = '$'.Names::tableSingleVar($relatedTable).'->'.$column ;
    }
    else
        $name = $model.'['.$column.']' ;
?>

                    @include('LaraCrud::templates.CRUD.views.elements.'.$columns[$column]['type'].'.form')

<?php } ?>


@if(isset($type) && $type == 'one_to_many')
                    </div>
                    <div style='margin-top: 25px;' class='col-md-2' >
                        {{'@'}}if($count['{{Names::tablePluralVar($table)}}'] ==1)
                        <input type='button' class='add btn btn-success' value="+">
                        {{'@'}}else
                        <input type='button' class='remove btn btn-danger' value="X">
                        {{'@'}}endif
                    </div>
                    </div>
@endif
                    <div class='clearfix' ></div>