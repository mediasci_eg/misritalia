<?php
use HossamAldeen\LaraCrud\Names;
echo '<?php ' 
?>

namespace App\Http\Controllers\Backend;

use App\Models\{{$model}} ;
<?php
if(isset($relatedTables)):
    foreach($relatedTables as $relatedTable=>$type):
?>
use App\Models\{{Names::model($relatedTable)}} ;
<?php
    endforeach;
endif ?>

class {{$controller}}Controller extends BaseController
{
    public function index({{$model}} $model)
    {
        $model = $this->search($model);
@if($hasSort)
        $model = $model->orderBy('{{$sortColumn}}');
@endif

        $data['data'] = $model->paginate(20);
        return view('backend.{{$table}}.index',$data);
    }
@if($hasDeletedAt)

    public function deleted({{$model}} $model){
        $data['deleted'] = true ;
        $model = $this->search($model);
        $model = $model->onlyTrashed();
        $data['data'] = $model->paginate(20);
        return view('backend.{{$table}}.index',$data);
    }
@endif

@if($crud['create']==1)
    public function create()
    {
        $model = new {{$model}} ;
        if (request()->has('save')){
@if($hasSort)
            $model->{{$sortColumn}} = 1 ;
@endif
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Created Successfully');
                return redirect('backend/{{$table}}');				
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.{{$table}}.form',$data);
    }
@endif

@if($crud['update']==1)
    public function update($id)
    {
        $model = {{$model}}::find($id) ;
        if (request()->has('save')){
            $store = $this->store($model);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                return redirect('backend/{{$table}}');				
            }else{
                $data['errors'] = true ;
            }
        }

        $data['model'] = $model ;
        return view('backend.{{$table}}.form',$data);
    }
@endif

@if($crud['update']==1 || $crud['create']==1 ) 
    private function store($model){
        //\DB::beginTransaction();
        try {
            $valid = true;
            $model->fill(request('{{$model}}'));

<?php   foreach($columns[$table] as $column=>$element): 
            if($element['type']=='image'): ?>
            if(request()->hasFile('{{$model}}.{{$column}}')) 
                $model->{{$column}} = $model->uploadImage('{{$column}}');

<?php       endif ; 
            if($element['type']=='images'): ?>
            if(request()->has('{{$model}}.{{$column}}'))
                $model->{{$column}} = implode(',', request()->{{$model}}['{{$column}}']);
			else
				$model->{{$column}} = '';

<?php       endif ; 
        endforeach; ?>
            if($model->validate())
                $model->save() ;
            else
                $valid = false;
<?php
if(isset($relatedTables)):
    foreach($relatedTables as $relatedTable=>$type):
        $name = Names::model($relatedTable) ;
?>
            {{$name}}::where('{{str_singular($table)}}_id' , $model->id)->delete();
            foreach(request('{{$name}}') as $elements){
                ${{$name}} = new {{$name}} ;
                ${{$name}}->fill($elements) ;
                ${{$name}}->{{str_singular($table)}}_id = $model->id ;
                if(${{$name}}->validate())
                    ${{$name}}->save() ;
                else
                    $valid = false;
            }
            
<?php
    endforeach;
endif ?>
            if(!$valid)
				throw new \Exception('Not Saved');	
            //\DB::commit();
            return true ;			
        } catch (\Exception $e) {
            //throw $e;	
            //\DB::rollback();
            return false;
        }
    }
@endif

@if($crud['view']==1)
    public function view($id){
        $data['model'] = {{$model}}::find($id) ;
        return view('backend.{{$table}}.view',$data);
    }
@endif
	
@if($crud['delete']==1)
    public function delete($id){
		try {
        $model = {{$model}}::destroy($id);
        if($model)
            session()->flash('msg', 'Deleted Successfully');
		} catch (\Throwable $th) {
            //throw $th;
            session()->flash('error', 'Can\'t Delete it may connected to other data');
        }
        return back();
    }
@endif
	
@if($hasDeletedAt)
    public function restore($id){
        $model = {{$model}}::whereId($id)->withTrashed()->first() ;
        $model->restore();
        session()->flash('msg', 'Restored Successfully');
        return redirect()->back();
    }
@endif
@if($hasSort)
    public function sort(){
        $i=1 ;
        foreach(request()->get('id') as $id){
            $model = {{$model}}::find($id);
            $model->{{$sortColumn}} = $i;
            $model->save();
            $i++ ;	
        }
        return back();
    }
@endif
	
    private function search($model){
        if(request()->has('search')){
            if(request('id')!='')
                $model = $model->where('id' , request('id'));
@foreach($index[$table] as $column)
@if(isset($columns[$table][$column]['type']) && $columns[$table][$column]['type']=='text')
            if(request('{{$column}}')!='')	
                $model = $model->where('{{$column}}' ,'like' , '%'.request('{{$column}}').'%');
@elseif(isset($columns[$table][$column]['type']) && $columns[$table][$column]['type']!='image' &&  $columns[$table][$column]['type']!='textarea' )
            if(request('{{$column}}')!='')
                $model = $model->where('{{$column}}' , request('{{$column}}'));
@endif
@endforeach
        }
@if(!$hasSort)
		$model = $model->orderByDesc('id');
@endif
        return $model ;
    }
	
    

}