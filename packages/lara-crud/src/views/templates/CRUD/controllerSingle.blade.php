<?php
echo '<?php ' 
?>

namespace App\Http\Controllers\Backend;

use App\Models\{{$model}} ;

class {{$controller}}Controller extends BaseController
{

    public function index()
    {
        $models = {{$model}}::all() ;
        if (request()->has('save')){
            $store = $this->store($models);
            if($store===true){
                session()->flash('msg', 'Updated Successfully');
                return redirect('backend/{{$table}}');				
            }else{
                $data['errors'] = true ;
            }
        }

        $data['models'] = $models ;
        return view('back.{{$table}}.form',$data);
    }

    private function store($models){
        //\DB::beginTransaction();
        try {
            $valid = true;
            foreach($models as $model){
                $model->fill(request('{{$model}}')[$model->id]);

@foreach($columns[$table] as $column=>$element)
@if($element['type']=='image')
                if(request()->hasFile('{{$model}}.{{$column}}')) 
                    $model->{{$column}} = $model->uploadImage('{{$column}}');
@endif
@if($element['type']=='images')
                if(request()->has('{{$model}}.{{$column}}'))
                    $model->{{$column}} = implode(',', request()->{{$model}}['{{$column}}']);
@endif
@endforeach
            if($model->validate())
                $model->save() ;
            else
                $valid = false;
            }
            if(!$valid)
                throw new \Exception('Not Saved');	
            //\DB::commit();
            return true ;			
        } catch (\Exception $e) {
            //throw new \Exception($e);	
            //\DB::rollback();
            return false;
        }
    }

}