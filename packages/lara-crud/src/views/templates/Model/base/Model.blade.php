<?php
echo '<?php ' 
?>

namespace App\Models\Base ;

class {{$model}} extends BaseModel
{
    protected $table = '{{$table}}';
    protected $guarded = ['id'];
    {!!$extra!!}

    @if($rules!='')
//======Rules======{!!$rules!!}
    @endif


    @if($relations!='')
//======Relations======{!!$relations!!}
    @endif

}