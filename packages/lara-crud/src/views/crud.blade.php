<?php 
use HossamAldeen\LaraCrud\Names ;
?>
@extends('LaraCrud::layout')
@section('content')
<h1 style='padding-left: 15px;' >CRUD Generator</h1>

<div class='row' style='max-width: 100%;'>
    <form method='post' >
        <div class='col-md-8' >
            <div class='col-md-6' >
                <select name='table' required class='form-control'  >
                    <option value=''>Choose Your Table</option>
                    @foreach($tables as $table)
                        <option <?= selected(@$_POST['table'] , $table)?> >{{$table}}</option>
                    @endforeach
                </select>
            </div>
            <div class='col-md-3' >
                <select class='form-control' name='template' >
                    <option value='crud' {{selected('crud' , request('template') )}} >CRUD</option>
                    <option value='multi' {{selected('multi' , request('template') )}}>Multi</option>
                    <option value='single' {{selected('single' , request('template') )}} >Single</option>
                </select>
            </div>
            <div class='col-md-3' >
                <input type='submit' name='checkOptions' value='check options' class='btn btn-primary' onclick="$('form').attr('target' , '')" >
            </div>
            <div class="clearfix" ></div>
			
			@if(isset($link))
            <div class='col-md-9' >
                <a target='_blank' href="{{$link}}">Go To The Page</a> <br>
                Add this to your routes 
                <?php 
                    $controller = Names::controller($_POST['table']) ;
                    $route = Names::route($_POST['table']) ;
                ?>
                <pre>
 
/* ########################## {{$controller}} ##################################### */
Route::any('{{$route}}', '{{$controller}}Controller@index');
Route::any('{{$route}}/create', '{{$controller}}Controller@create');
Route::any('{{$route}}/update/{id}', '{{$controller}}Controller@update');
Route::any('{{$route}}/delete/{id}', '{{$controller}}Controller@delete');
                </pre>
            </div>
            @endif
            @if(isset($_POST['checkOptions']))
                @include('LaraCrud::partials.checkOptions' , [
						'table'=>$_POST['table'],
						'crud'=>TRUE ,
					])
                @isset($relatedTables)
                    <div class='col-md-12' >
                        <h3>Related Tables</h3>
                        @foreach($relatedTables as $relatedTable)
                        <?php 
                        //dd($file->relatedTables->{$relatedTable['table']});
                        ?>
                            <div class='relatedTables' >
                                <input type="checkbox" checked class="checkRelatedTable">
                                <label style="width: 110px" >{{$relatedTable['table']}}</label>
                                <select style='display: inline-block;width: 200px;margin-left: 10px;' name='relatedTables[{{$relatedTable['table']}}]' class='form-control'>
                                    <option value='one_to_many' <?= @selected($file->relatedTables->{$relatedTable['table']} ,'one_to_many' )?> >One to Many</option>
                                    <option value='one_to_one'  <?= @selected($file->relatedTables->{$relatedTable['table']} ,'one_to_one' )?> >One to One</option>
                                </select>
                                <div class='clearfix' ></div>
                                <div class="row" >
                                    @include('LaraCrud::partials.checkOptions' , [
                                        'columns'=>$relatedTable['columns'] ,
                                        'columnsTypes'=>$relatedTable['columnsTypes'] ,
                                        'table'=>$relatedTable['table'] ,
                                        'crud'=>FALSE ,
                                    ])
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <script>
                    $('.checkRelatedTable').click(function(){
                        $(this).parent().remove()
                    })
                    </script>
                @endisset
                
            @elseif(isset($_POST['check']))
                @include('LaraCrud::partials.check')
            @endif
            <div class='clearfix' ></div>
            <div class='col-md-6' >
                @if(isset($_POST['check']))
                <input type='submit' name='generate' value='generate' class='btn btn-primary' onclick="$('form').attr('target' , '')" >
                @elseif(isset($_POST['checkOptions']))
                <input type='submit' name='check' value='check' class='btn btn-primary' onclick="$('form').attr('target' , '')" >
               
                @endif
            </div>
        </div>


    </form>
</div>

@stop