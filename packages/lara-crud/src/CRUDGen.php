<?php

namespace HossamAldeen\LaraCrud;

class CRUDGen extends BaseGenerators {

    protected $table;
    protected $model;
    public $files;
    public $form;
    public $index;
    public $crud;
    public $template;
    public $columnsTypes;
    public $relatedTables;
    public $viewResult = false;

    public function __set($key, $value) {
        if ($key == 'table') {
            $this->table = $value;
            $this->model = Names::model($value);
        }
    }

    function check() {
        $files = $this->filesContent();
        $list = $this->checkFiles($files);
        return $list;
    }

    function generate() {
        $files = $this->files();
        foreach ($files as $file => $info) {
            if (in_array($file, $this->files)) {
                $path = explode('/', $info['path']);
                array_pop($path);
                $folder = implode('/', $path);
                if (!is_dir($folder))
                    mkdir($folder);

                if ($this->viewResult == true) {
                    echo '<pre>' . htmlspecialchars($this->filesContent($file)[0]) . '</pre>';
                    die;
                } else
                    file_put_contents($info['path'], $this->filesContent($file));
            }
        }
    }

    function filesContent($file = '') {
        $columns = $this->columns($this->table, true, false);
        $data['table'] = $this->table;
        $data['controller'] = Names::controller($this->table);
        $data['model'] = $this->model;
        $data['form'] = $this->form;
        $data['index'] = $this->index;
        $data['crud'] = $this->crud;
        $data['template'] = $this->template;
        $data['columns'] = $this->columnsTypes;
        if($this->relatedTables !='' ){
            $data['relatedTables'] = $this->relatedTables;
        }
        $data = array_merge($data, $this->columnFeatures($columns));
        $files = $this->files($file);
        foreach ($files as $file => $info) {
            $filesContent[$file] = view($info['template'], $data)->render();
        }


        return $filesContent;
    }

    private function files($file = '') {
        $return = [];
        if ($this->template == 'single') {
            $return['controller'] = [
                'template' => 'LaraCrud::templates.CRUD.controllerSingle',
                'path' => app_path() . '/Http/Controllers/Back/' . Names::controller($this->table) . 'Controller.php',
            ];

            $return['form'] = [
                'template' => 'LaraCrud::templates.CRUD.views.formSingle',
                'path' => resource_path() . '/views/backend/' . $this->table . '/form.blade.php',
            ];
        } else {
            $return['controller'] = [
                'template' => 'LaraCrud::templates.CRUD.controller',
                'path' => app_path() . '/Http/Controllers/Backend/' . Names::controller($this->table) . 'Controller.php',
            ];

            $return['index'] = [
                'template' => 'LaraCrud::templates.CRUD.views.index',
                'path' => resource_path() . '/views/backend/' . $this->table . '/index.blade.php',
            ];

            $return['view'] = [
                'template' => 'LaraCrud::templates.CRUD.views.view',
                'path' => resource_path() . '/views/backend/' . $this->table . '/view.blade.php',
            ];

            $return['form'] = [
                'template' => 'LaraCrud::templates.CRUD.views.form',
                'path' => resource_path() . '/views/backend/' . $this->table . '/form.blade.php',
            ];
        }


        if ($file == '')
            return $return;
        else
            return [$return[$file]];
    }

    private function checkFiles($files) {
        $return = [];
        $filesInfo = $this->files();

        foreach ($files as $file => $content) {
            if (!file_exists($filesInfo[$file]['path'])) {
                $return[$file] = 'new';
            } elseif (file_get_contents($filesInfo[$file]['path']) == $content) {
                $return[$file] = 'not changed';
            } else {
                $return[$file] = 'changed';
            }
        }
        return $return;
    }

    function relations($table='') {
        $return = [];
        $relations = parent::relations($table);
        foreach ($relations as $relation) {
            $return[$relation['column']] = $relation['table'];
        }

        return $return;
    }

    function crudTypes() {
        $types = [
            'text',
            'textarea',
            'text editor',
            'number',
            'relation',
            'select',
            'date',
            'checkbox',
            'password',
            'image',
            'images',
            'sort',
        ];
        return $types;
    }

    function crudColumnsTypes($columns) {
        $return = new \stdClass();

        foreach ($columns as $column) {
            preg_match('/(\w+)\((\d+)\)/', $column->Type, $matches);

            if (in_array($column->Field, ['image', 'img', 'banner', 'logo', 'photo']))
                $return->{$column->Field} = (object) ['type' => 'image'];

            elseif (in_array($column->Field, ['sort', 'order']))
                $return->{$column->Field} = (object) ['type' => 'sort'];

            elseif (in_array($column->Field, ['images']))
                $return->{$column->Field} = (object) ['type' => 'images'];

            elseif (in_array($column->Field, ['pass', 'password']))
                $return->{$column->Field} = (object) ['type' => 'password'];

            elseif (isset($column->relation))
                $return->{$column->Field} = (object) ['type' => 'relation'];

            elseif (isset($matches[1]) && $matches[1] == 'int')
                $return->{$column->Field} = (object) ['type' => 'number'];

            elseif ($column->Type == 'text')
                $return->{$column->Field} = (object) ['type' => 'textarea'];

            elseif ($column->Type == 'date')
                $return->{$column->Field} = (object) ['type' => 'date'];

            elseif (isset($matches[1]) && $matches[1] == 'tinyint')
                $return->{$column->Field} = (object) ['type' => 'checkbox'];

            elseif (strstr($column->Type, 'enum')) {
                preg_match("/^enum\(\'(.*)\'\)$/", $column->Type, $matches);
                $values = str_replace("','", ",", $matches[1]);
                $return->{$column->Field} = (object) ['type' => 'select', 'values' => $values];
            } else {
                $return->{$column->Field} = (object) ['type' => 'text'];
            }

        }

        return $return;
    }

    function relatedTables($table){
        $relations = parent::relations($table);
        $return = [] ;
        foreach($relations as $relation){
            if($relation['type'] == 'hasMany')
            {
                $columns = $this->columns($relation['table']);
                foreach($columns as $key=>$column){
                    if($column->Field == $relation['column'])
                        unset($columns[$key]); //remove table_id
                }
                $return[] = [
                    'table'=> $relation['table'] ,
                    'columns'=> $columns ,
                    'columnsTypes'=> $this->crudColumnsTypes($columns) ,
                ] ;
            }
        }

        return $return ;
    }
}

?>