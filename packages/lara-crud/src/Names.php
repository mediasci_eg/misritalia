<?php

namespace HossamAldeen\LaraCrud;

class Names {
	
	static function controller($table){
        return ucfirst(camel_case($table));
    }
	
	static function model($table) {
        return ucfirst(camel_case(str_singular($table)));
    }
	
	static function column($column){
		return ucfirst(str_replace(['_id' , '_'] ,['' , ' '] ,$column));
	}

	static function tableSingle($table) {
		return str_singular(ucfirst(str_replace(['_'] ,[' '] ,$table))) ;
	}
	
	static function tablePlural($table) {
		return str_plural(ucfirst(str_replace(['_'] ,[' '] ,$table))) ;
	}
	
	static function tableSingleVar($table) {
		return camel_case(str_singular($table));
	}
	
	static function tablePluralVar($table) {
		return camel_case(str_plural($table));
	}
	
	static function route($table) {
		return str_replace(['_'] ,['-'] , str_singular($table));
	}

        static function relationMethod($relation , $table) {
            if(strstr($relation , $table.'_' ))
                $method = str_replace($table.'_' , '' , $relation ) ;
            else if(strstr($relation , str_singular($table).'_' ))
                $method = str_replace(str_singular($table).'_' , '' , $relation ) ;
            else
                $method = $relation ;
            return $method ;
        }
	
	static function titleColumn($columns){
		foreach($columns as $col) {
            if (in_array($col->Field , ['name' , 'title' ]  )) {
                $titleColumn = $col->Field;
				break ;
            }
        }
		if(isset($titleColumn))
			return $titleColumn ;
		else
			return 'id' ;
		
	}
}