<?php

namespace HossamAldeen\LaraCrud;

class BaseGenerators {

    function relations($table) {
        if($table == '')
            $table = $this->table ;
        $return = [] ;
        $relations = \DB::select("SELECT 
			`TABLE_NAME`,                            
			`COLUMN_NAME`,                           
			`REFERENCED_TABLE_NAME`,                 
			`REFERENCED_COLUMN_NAME`                 
			FROM
			`INFORMATION_SCHEMA`.`KEY_COLUMN_USAGE`
			where CONSTRAINT_SCHEMA='" . env('DB_DATABASE') . "' and  REFERENCED_TABLE_NAME is not null and (TABLE_NAME = '" . $table . "' 
			or REFERENCED_TABLE_NAME = '" . $table . "' ) ");
        foreach($relations as $relation){
            if ($relation->TABLE_NAME == $table) {
                $return[] = [
                    'table'=> $relation->REFERENCED_TABLE_NAME ,
                    'column'=> $relation->COLUMN_NAME ,
                    'type'=> 'belongsTo'
                ] ;
            }else{
                $return[] = [
                    'table'=> $relation->TABLE_NAME ,
                    'column'=> $relation->COLUMN_NAME ,
                    'type'=> 'hasMany'
                ] ;
                
            }
        }
        return $return;
    }

    public function columns($table, $withRelation = true, $escapeColumns = true) {
        $columns = \DB::select("SHOW COLUMNS FROM `" . $table . "` ");

        if ($escapeColumns) {
            $escape = ['id', 'created_at', 'updated_at', 'deleted_at'];
            foreach ($columns as $k => $column) {
                if (in_array($column->Field, $escape))
                    unset($columns[$k]);
            }
        }

        if ($withRelation) {
            $relations = $this->relations($table);
            foreach ($relations as $relColumn => $relTable) {
                foreach ($columns as $column) {
                    if ($column->Field == $relColumn) {
                        $column->relation = (object) [
                            'table' => $relTable,
                            'columns' => $this->columns($relTable, false),
                        ];
                    }
                }
            }
        }
        return $columns;
    }

    public function columnFeatures($columns) {
        $data['hasImage'] = false;
        $data['hasImages'] = false;
        $data['hasSort'] = false;
        $data['hasTextEditor'] = false;
        $data['hasDeletedAt'] = false;
        $data['sortColumn'] = '';

        foreach ($columns as $k => $column) {
            if ($column->Field == 'deleted_at') {
                $data['hasDeletedAt'] = true;
                break;
            }
        }

        $data['titleColumn'] = Names::titleColumn($columns);

        foreach ($this->columnsTypes[$this->table] as $col => $element) {
            if (in_array($element['type'], ['image', 'images'])) {
                $data['hasImage'] = true;
            }
            if ($element['type'] == 'sort') {
                $data['hasSort'] = true;
                $data['sortColumn'] = $col;
            }
			
            if ($element['type'] == 'texteditor') {
				$data['hasTextEditor'] = true ;
			}
			if ($element['type'] == 'images') {
				$data['hasImages'] = true ;
			}
        }

        return $data;
    }

}
