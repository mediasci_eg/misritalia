<script type="text/javascript">
    var media_trans=<?= json_encode(trans('media-manager::media'))?>;
</script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
<link rel="stylesheet" href="{{url('vendor/elsayed_nofal/media-manager/app.css')}}">
@if(App::isLocale('ar'))
<link rel="stylesheet" href="{{url('vendor/elsayed_nofal/media-manager/app-ar.css')}}">
@endif
<script src="{{url('vendor/elsayed_nofal/media-manager/vue.js')}}"></script>
<script src="{{url('vendor/elsayed_nofal/media-manager/jquery.blockUI.js')}}"></script>
<script src="{{url('vendor/elsayed_nofal/media-manager/jquery-ui.js')}}"></script>
<script src="{{url('vendor/elsayed_nofal/media-manager/app.js')}}"></script>