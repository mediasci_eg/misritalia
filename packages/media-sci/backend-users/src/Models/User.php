<?php

namespace MediaSci\BackendUsers\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable{
    protected $table='backend_users';
    protected $fillable=['name','email','image'];
    

}