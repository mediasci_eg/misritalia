-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2019 at 03:25 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `misritalia`
--

-- --------------------------------------------------------

--
-- Table structure for table `developments`
--

CREATE TABLE `developments` (
  `id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `overview_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `overview_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `design_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `design_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_types_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_types_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_in_homepage` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `developments`
--

INSERT INTO `developments` (`id`, `title`, `image`, `logo`, `overview_image`, `overview_content`, `location_image`, `location_content`, `design_image`, `design_content`, `unit_types_image`, `unit_types_content`, `video`, `lat`, `lng`, `is_in_homepage`, `created_at`, `updated_at`) VALUES
(1, 'CBP', '29a1a43be9e5beef29d0f05de0599919.jpeg', '14abed5eff95009ebdea83e6b5c38b1b.jpeg', '188ee495cc0e0f149b31619f466aba72.jpeg', 'For the first time in Egypt, Cairo Business Park introduces the concept of stand-alone offices & corporate buildings within a 360 gated compound in a fully integrated business community.', '3382e93b868b1d9dbe16eeea62eee971.jpeg', 'The business hub is supporting the presence of small, medium & large enterprises to cater for different needs.', 'ee252e31f50f34df9a0cd5cf833d3cce.jpeg', 'The park area serves as a visual, psychological and social break haven.', 'c5531886564594b0564c9c0eb3a9f638.jpeg', 'Being a prestigious business hub, Cairo Business Park will help you build the image you desire for your business.', NULL, '30.0264549', '31.52832079999996', 0, '2019-11-25 13:18:32', '2019-12-24 09:26:20'),
(5, 'Corporate', '1ef0b624589fdd60cbd12c9073dd589c.jpeg', 'badf0a7fbaccf0e9653478a5a7c74c86.jpeg', '0537028a9eac908fa0ff1fe62339f43e.jpeg', 'Misr Italia Properties has been the vanguard of developing Egypt\'s leading real estate market for years.', 'f999f5b273c25bf1f288e4c490243e64.jpeg', 'We have been known for providing the paramount projects for residential, commercial, hospitality and leisure properties across Egypt.', 'f358062ea3ed0437838434af6a382308.jpeg', 'We aim to stay on the market lead with more forward thinking and trend setting projects by listening to our customers.', 'c00da99dc0a39caee88f4d8d16944554.jpeg', 'Built on a philosophy of creativity and innovation, we offer our clients trend-setting projects with unique concepts and designs that are unmatched in the field.', NULL, '30.08521859999999', '31.339712200000008', 0, '2019-11-26 09:05:40', '2019-12-24 10:31:27'),
(6, 'Garden 8', '0e1f55e89e986db40910ae91c4c897f6.jpeg', 'b79c27b443e96cab7a405df7d578ed67.png', '217c9cf34c37a35cbc43fd298ea0a4e4.jpeg', 'La Nuova Vista is at the heart of the residential location of New Cairo. Accessible by an excellent road network, LNV fits into the New Cairo landscape that features unrivalled facilities for relaxation and recreation.', '8e8aa1db228ed5dbcfaf1e49eeaf0460.jpeg', 'An elegant recreational hub that aims to offer world class shopping, dining & entertainment facilities. This master designed project induces the modernistic elegant feel of an entertainment & retail service mall serving the luxurious community with more mix of facilities.', '6b59cc81598bbd33c440c6e0d7d1c28f.jpeg', 'Garden 8 is the only full-fledge community mall in New Cairo with a large outdoor court featuring exclusive landscape.', 'f8e0ed487ae6d6bda19b4b394e0ceb42.jpeg', 'Where modern design & elegance meet, lies Garden 8, a trendy yet sophisticated community mall that aims at serving a remarkable hangout & shopping experience.', NULL, '30.00741300000001', '31.491318200000023', 1, '2019-12-03 07:40:18', '2019-12-24 10:36:14'),
(7, 'IL BOSCO', 'b7ed51d38942c253045187bdaef69b6b.jpeg', '9be4fe9b59c5771c6d453df9e03d8990.png', 'ba9b3f6ea6d0353df9be81b680a60115.jpeg', 'A world class project with its revolutionary concept, a concept that will create a new outlook to green living in Egypt. For the first time in the Middle East and Africa, a forest-like environment will be built to show people the new meaning of an ecological green life system. It’s a project that will create a cross section of environments to serve the community with the most inventive concept. IL BOSCO is your gateway to the new 21st century urban ecology and innovative design. A groundbreaking experience is awaiting IL BOSCO residents.', '18c5b0889dc4211d21e648764b2f6283.jpeg', 'A ‘spot on location’ at the New Capital, IL BOSCO is located at the heart of the Green river and right by Mohamed Bin Zayed St., which will be lying next to some of the most active districts around the New Capital.', '2a3cdefb79f71386cc7a2ca19ae85721.jpeg', 'The villas are located at the heart of a magnificent green area that overlooks the beautiful meandering pathways, open green spaces, and community gardens. With the open terrace in every villa, residents can enjoy an early morning breakfast or a family picnic.', 'faec4d3db0e2b49f6989bcc1718b6118.jpeg', 'The apartments are surrounded by beautiful sceneries like striking plantations, unique-looking wildflowers, and fascinating forest pathways. They are designed with a high level of elegancy to maintain the overall luxurious look that is consistent with the design of the project.', NULL, '29.9051433', '31.719545899999957', 0, '2019-12-24 10:41:40', '2019-12-24 10:41:40'),
(8, 'IL BOSCO CITY', '519fc349fa6ef3ad26f09825d02e9fdb.jpeg', 'e72c4fd183eaeb6fdcc4e2feea72be5d.png', '44640a14850efd1ecbd361214bd164a1.jpeg', 'The concept behind Il Bosco City is focusing on the key aspect of the city.', 'fd675d042143c03899e7829198cbb1a8.jpeg', 'Il Bosco City is located just minutes away from the New Capital, moments away from Al Fattah El Aleem Mosque and directly positioned on Cairo’s main roads. It is perfectly located at the heart of the Mostakbal City.', '6ffb6cbfdc1c52f53b6a5fc1b73b43a2.jpeg', 'Rooted in the urban landscaping that allows for the city concept to be emerged in the natural greenery, bringing to life “The City of Nature”. .', 'e6a2528632c5a81f4a6ceaeb61074727.jpeg', 'Localizing the notion of “IL BOSCO”, meaning the forest; the creative approach executed towards the urban landscape and architecture derived from a sequence of functionality followed by the aesthetics of the desired lifestyle that is embodied by the citizens of the artistic-botanic city.', NULL, '30.0264549', '31.52832079999996', 0, '2019-12-24 10:47:09', '2019-12-24 10:47:09'),
(9, 'Kai Sokhna', '27d1d6e6275a4f5998441608ba873c0e.png', '4f100d79cd10dae0d480a13eaba4da51.jpeg', '4eb9783e88f1c18b85aeffaf37ea9ef3.png', '“Kai” in Hawaiian means Sea, which conveys the comfort of the get-away, which will help you recover and restore your senses. Our project is all about being exclusive while offering breathtaking scenery to all our owners. Kai shows Elegance and sophistication with the simplest of forms.', '21cf575bf139bc76bc495832ba69777e.jpeg', 'Third row Pool Deck Chalets are only 150 meters away from the beach. Pool Deck Chalets include 1, 2, & 3 bedroom chalets with pools located on two levels and second ground floor. Pool Deck Chalets are 9.5 meters above sea level to allow second floors to have full sea view.', '1acbff28c02cb15f194774b219ce307e.jpeg', 'First row is 50 meters away from the sea with a great view for the sea horizon and its 3.5 meters above sea ievel; we offer each villa a private garden and an optional private pool. One can enjoy the sea view from the garden and infinity pool.', '5153a45a6f2a417512b0cdfb05b49966.png', 'Second row Senior Chalets are only 100 meters away from the beach. The ground floor enjoys a beautiful private garden as well as a private pool. Second row Senior Chalets are 6.5 meters above sea level to allow both first and second floors to have full sea view.', NULL, '29.92745479999999', '32.40141930000004', 0, '2019-12-24 10:52:12', '2019-12-24 10:52:12'),
(10, 'LNV', 'ee8dab92ce51f19048ac8a04654ea83a.jpeg', 'bc2f0accfeb8cf5586eb31fb9441fe2d.jpeg', 'd4b3399407b1b4bed18c083a88c385a9.jpeg', 'La Nuova Vista is an exclusive community based residential compound that accommodates to families perfectly with lush landscape gardens and pathways throughout.', '3b731d93bea81099b50c1cedf8c4d41f.jpeg', 'La Nuova Vista is at the heart of the residential location of New Cairo. Accessible by an excellent road network, LNV is just a few km from Ring Road, Suez-Road, Road 90 and  10 minutes from Cairo International Airport.', 'f04ec7dd919fe3a81d279a8d1862918b.jpeg', 'Urban landscape design was creatively allocated by the world renowned design house TREDIC UK with the beautiful collaboration with the brilliant mastermind designer, the architect Eng. Shehab Mazhar.', '130d9c808a91d8a952ec6d5a6e6988b9.jpeg', 'La Nuova Vista brings back the bliss of family reunions with the most serene setting that transits you to the breathtaking beauty of nature and creates an ease and peacefulness of mind.', NULL, '30.0342394', '31.468470300000035', 0, '2019-12-24 10:58:30', '2019-12-24 10:58:30'),
(11, 'VINCI', '96d3b55e58522d4f1e175b726df30655.jpeg', '65dafb02d71dc433571acd8bc995108d.png', '658ed026395b9c4fb6aa17ccdcc247f5.jpeg', 'Vinci, has a very distinctive design concept, one that is unmatched! It is inspired by art and its various phases.', 'd62d994618482e5fc5728318d28852ba.jpeg', 'Vinci is perfectly located at the heart of the New Capital City, on the main road of the New Capital. It is situated in the residential region of the diplomatic area with easy access to the main lines of transportation.', 'ebed504e94417228709b410828438172.jpeg', 'It integrates the art elements of every art style into its urban and landscape design using dynamic approaches to set the standards for contemporary living, created to exude the warm earthy accent of a home whilst overlooking scenic landscapes.', 'fcd508a3c7d39b4cfd384ee6df21d12a.jpeg', 'Behind the creation of this project comes the interior designer, Hany Saad Innovations creating a unique look and style to the exteriors and interiors of the project.', NULL, '29.948894', '31.70303690000003', 0, '2019-12-24 11:06:17', '2019-12-24 11:06:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `developments`
--
ALTER TABLE `developments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `developments`
--
ALTER TABLE `developments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
