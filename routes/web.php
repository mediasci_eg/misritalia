<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['namespace' => 'Front'], function () {
	Route::post('save', 'LandingController@save');
	Route::get('thank-you/{slug}', 'LandingController@thankYou');
});
Route::domain('sitevisit.misritaliaproperties.com')->group(function () {
	Route::get('/', 'Front\LandingController@sitevisit');
});

Route::domain('inquiry.misritaliaproperties.com')->group(function () {
	Route::get('/', 'Front\LandingController@inquiry');
});
Route::domain('www.misritaliaproperties.com')->group(function () {
	Route::get('/', function(){
		return redirect('https://misritaliaproperties.com/');
	});
});

Route::domain('events.misritaliaproperties.com')->group(function () {
	Route::get('/', 'Front\LandingController@events');
});

Route::domain('{slug}.misritaliaproperties.com')->group(function () {
    Route::post('save', 'Front\LandingController@save');
    Route::get('thank-you', 'Front\LandingController@thankYou');
    Route::get('/home', 'Front\HomeController@index');
    Route::get('/', 'Front\LandingController@index');
});

Route::group(['namespace' => 'Front'], function () {
    Route::get('/', 'HomeController@index');
    Route::post('/landing/save', 'LandingController@save');
    Route::get('/landing/thank-you/{slug}', 'LandingController@thankYou');
    Route::get('/landing/{slug}', 'LandingController@index');
    Route::get('/integrate', 'LandingController@integrate');
    Route::get('/sitevisit', 'LandingController@sitevisit');
    Route::get('/inquiry', 'LandingController@inquiry');
    Route::get('/events', 'LandingController@events');
    Route::get('/test-mail', 'MailController@index');
    Route::get('/test', 'TestController@index');
    Route::any('/query', 'QueryController@index');
    Route::any('/pull', 'PullController@index');
});
