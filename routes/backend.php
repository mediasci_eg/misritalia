<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::group(['prefix' => 'backend', 'namespace' => 'Backend', 'middleware' => ['auth:admin']], function () {
    Route::get('/', 'HomeController@index');

	/* ########################## BusinessTransformation ##################################### */
	Route::any('business_transformation', 'BusinessTransformationController@index');
	Route::any('business_transformation/create', 'BusinessTransformationController@create');
	Route::any('business_transformation/update/{id}', 'BusinessTransformationController@update');
	Route::any('business_transformation/delete/{id}', 'BusinessTransformationController@delete');


	/* ########################## FrameworkStructure ##################################### */
	Route::any('framework_structure', 'FrameworkStructureController@index');
	Route::any('framework_structure/create', 'FrameworkStructureController@create');
	Route::any('framework_structure/update/{id}', 'FrameworkStructureController@update');
	Route::any('framework_structure/delete/{id}', 'FrameworkStructureController@delete');
    // ############################ Posts Crud ##############################
    Route::any('posts', 'PostsController@index');
    Route::any('posts/create', 'PostsController@create');
    Route::any('posts/update/{post}', 'PostsController@update');
    Route::any('posts/delete/{post}', 'PostsController@delete');
    Route::any('posts/store', 'PostsController@store');
    /* ########################## Vacancies ##################################### */
    Route::any('vacancies', 'VacanciesController@index');
    Route::any('vacancies/create', 'VacanciesController@create');
    Route::any('vacancies/update/{id}', 'VacanciesController@update');
    Route::any('vacancies/delete/{id}', 'VacanciesController@delete');
    /* ########################## VacanciesApply ##################################### */
    Route::any('vacancies_apply', 'VacanciesApplyController@index');
    Route::any('vacancies_apply/create', 'VacanciesApplyController@create');
    Route::any('vacancies_apply/update/{id}', 'VacanciesApplyController@update');
    Route::any('vacancies_apply/delete/{id}', 'VacanciesApplyController@delete');

    // ############################ News&Events Crud ##############################
    Route::get('news-events', 'NewsEventsController@index');
    Route::get('news-events/create', 'NewsEventsController@create');
    Route::get('news-events/update/{news_events}', 'NewsEventsController@update');
    Route::post('news-events/delete/{news_events}', 'NewsEventsController@delete');
    Route::post('news-events/store', 'NewsEventsController@store');

    /* ########################## News ##################################### */
    Route::any('news', 'NewsController@index');
    Route::any('news/create', 'NewsController@create');
    Route::any('news/update/{id}', 'NewsController@update');
    Route::any('news/delete/{id}', 'NewsController@delete');

    /* ########################## Events ##################################### */
    Route::any('events', 'EventsController@index');
    Route::any('events/create', 'EventsController@create');
    Route::any('events/update/{id}', 'EventsController@update');
    Route::any('events/delete/{id}', 'EventsController@delete');
    Route::post('events/store', 'EventsController@store');
    /* ####################### Settings ##################################### */
    Route::get('settings', 'SettingsController@anyIndex');
    Route::post('settings/update_settings', 'SettingsController@anyUpdate');


    /* ########################## Branches Pages ################################### */
    Route::any('branches', 'BranchesController@index');
    Route::get('branches', 'BranchesController@index');
    Route::post('branches/store', 'BranchesController@store');
    Route::get('branches/create', 'BranchesController@create');
    Route::get('branches/update/{id}', 'BranchesController@update');
    Route::post('branches/delete/{branch}', 'BranchesController@delete');

    /* ########################## Developments Pages ################################### */
    Route::any('branches', 'BranchesController@index');
    Route::get('developments', 'DevelopmentController@index');
    Route::post('developments/store', 'DevelopmentController@store');
    Route::get('developments/create', 'DevelopmentController@create');
    Route::get('developments/update/{id}', 'DevelopmentController@update');
    Route::any('developments/delete/{branch}', 'DevelopmentController@delete');


    // ############################ Contact-Us Crud ##############################
    Route::get('contact-us', 'ContactUsController@index');
    Route::get('contact-us/trash', 'ContactUsController@indexTrashed');
    Route::post('contact-us/delete/{contacts}', 'ContactUsController@delete');
    Route::post('/postajax', 'ContactUsController@ajview');

    // ############################ Developments Inquiries Crud ##############################
    Route::get('developments-inquiries', 'DevelopmentsInquiriesController@index');
    Route::get('developments-inquiries/trash', 'DevelopmentsInquiriesController@indexTrashed');
    Route::post('developments-inquiries/delete/{developments_inquiries}', 'DevelopmentsInquiriesController@delete');
    Route::post('developments-inquiries/postajax', 'DevelopmentsInquiriesController@ajview');

    Route::any('backend/ajax/upload', 'Helpers\AjaxController@anyUpload');
    Route::any('backend/ajax/editorupload', 'Helpers\AjaxController@anyEditorupload');
    Route::any('backend/ajax/removeimage', 'Helpers\AjaxController@anyRemoveimage');
    Route::any('backend/ajax/browse', 'Helpers\AjaxController@anyBrowse');
    Route::any('upload-image', 'UploadImageController@index');
    Route::any('upload-pdf', 'UploadPdfController@index');

    /* ########################## Slider ##################################### */
    Route::any('slider', 'SliderController@index');
    Route::any('slider/create', 'SliderController@create');
    Route::any('slider/update/{id}', 'SliderController@update');
    Route::any('slider/delete/{id}', 'SliderController@delete');
    Route::any('slider/sort', 'SliderController@sort');

    /* ########################## Members ##################################### */
    Route::any('members', 'MembersController@index');
    Route::any('members/create', 'MembersController@create');
    Route::any('members/update/{id}', 'MembersController@update');
    Route::any('members/delete/{id}', 'MembersController@delete');
    Route::any('members/sort', 'MembersController@sort');

    /* ########################## Constructions ##################################### */
    Route::any('constructions', 'ConstructionsController@index');
    Route::any('constructions/create', 'ConstructionsController@create');
    Route::any('constructions/update/{id}', 'ConstructionsController@update');
    Route::any('constructions/delete/{id}', 'ConstructionsController@delete');

    /* ########################## BoxOfHope ##################################### */
    Route::any('box-of-hope', 'BoxOfHopeController@index');
    Route::any('box-of-hope/create', 'BoxOfHopeController@create');
    Route::any('box-of-hope/update/{id}', 'BoxOfHopeController@update');
    Route::any('box-of-hope/delete/{id}', 'BoxOfHopeController@delete');

    /* ########################## FoundersMessage ##################################### */
    Route::any('founders_message', 'FoundersMessageController@index');
    Route::any('founders_message/create', 'FoundersMessageController@create');
    Route::any('founders_message/update/{id}', 'FoundersMessageController@update');
    Route::any('founders_message/delete/{id}', 'FoundersMessageController@delete');

    /* ########################## Csr ##################################### */
    Route::any('csr', 'CsrController@index');
    Route::any('csr/create', 'CsrController@create');
    Route::any('csr/update/{id}', 'CsrController@update');
    Route::any('csr/delete/{id}', 'CsrController@delete');

    /* ########################## CorporateGovernance ##################################### */
    Route::any('corporate_governance', 'CorporateGovernanceController@index');
    Route::any('corporate_governance/create', 'CorporateGovernanceController@create');
    Route::any('corporate_governance/update/{id}', 'CorporateGovernanceController@update');
    Route::any('corporate_governance/delete/{id}', 'CorporateGovernanceController@delete');

    /* ########################## MessageOfHope ##################################### */
    Route::any('message-of-hope', 'MessageOfHopeController@index');
    Route::any('message-of-hope/create', 'MessageOfHopeController@create');
    Route::any('message-of-hope/update/{id}', 'MessageOfHopeController@update');
    Route::any('message-of-hope/delete/{id}', 'MessageOfHopeController@delete');

    /* ########################## FormWhistleblowing ##################################### */
    Route::any('form-whistleblowing', 'FormWhistleblowingController@index');
    Route::any('form-whistleblowing/create', 'FormWhistleblowingController@create');
    Route::any('form-whistleblowing/update/{id}', 'FormWhistleblowingController@update');
    Route::any('form-whistleblowing/view/{id}', 'FormWhistleblowingController@view');
    Route::any('form-whistleblowing/delete/{id}', 'FormWhistleblowingController@delete');

    /* ########################## Partners ##################################### */
    Route::any('partners', 'PartnersController@index');
    Route::any('partners/create', 'PartnersController@create');
    Route::any('partners/update/{id}', 'PartnersController@update');
    Route::any('partners/delete/{id}', 'PartnersController@delete');
    Route::any('partners/sort', 'PartnersController@sort');

    /* ########################## LetsGrow ##################################### */
    Route::any('lets_grow', 'LetsGrowController@index');
    Route::any('lets_grow/create', 'LetsGrowController@create');
    Route::any('lets_grow/update/{id}', 'LetsGrowController@update');
    Route::any('lets_grow/delete/{id}', 'LetsGrowController@delete');

	/* ########################## AboutUs ##################################### */
	Route::any('about_us', 'AboutUsController@index');
	Route::any('about_us/create', 'AboutUsController@create');
	Route::any('about_us/update/{id}', 'AboutUsController@update');
    Route::any('about_us/delete/{id}', 'AboutUsController@delete');

    /* ########################## ContactPage ##################################### */
    Route::any('contact_page', 'ContactPageController@index');
    Route::any('contact_page/create', 'ContactPageController@create');
    Route::any('contact_page/update/{id}', 'ContactPageController@update');
    Route::any('contact_page/delete/{id}', 'ContactPageController@delete');

    /* ########################## CsrPartners ##################################### */
    Route::any('csr_partners', 'CsrPartnersController@index');
    Route::any('csr_partners/create', 'CsrPartnersController@create');
    Route::any('csr_partners/update/{id}', 'CsrPartnersController@update');
    Route::any('csr_partners/delete/{id}', 'CsrPartnersController@delete');

    /* ########################## Inspiration ##################################### */
    Route::any('inspiration', 'InspirationController@index');
    Route::any('inspiration/create', 'InspirationController@create');
    Route::any('inspiration/update/{id}', 'InspirationController@update');
    Route::any('inspiration/delete/{id}', 'InspirationController@delete');
});

/* ########################## Ajax Upload ##################################### */
Route::group(['middleware'=>['auth:admin']],function(){

    Route::any('backend/ajax/upload', 'Helpers\AjaxController@anyUpload');
    Route::any('backend/ajax/editorupload', 'Helpers\AjaxController@anyEditorupload');
    Route::any('backend/ajax/removeimage', 'Helpers\AjaxController@anyRemoveimage');
    Route::any('backend/ajax/browse', 'Helpers\AjaxController@anyBrowse');
});
