<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['namespace' => 'Api'], function () {
    Route::get('developments', 'Developments@index');
    Route::get('about-us', 'AboutUsController@index');
    Route::get('inspiration', 'InspirationController@index');
    Route::get('developments/all', 'Developments@all');
    Route::get('developments/home', 'Developments@home');
    Route::get('developments/{id}', 'DevelopmentsDetails@index');
    Route::post('developments-inquiries', 'DevelopmentsInquiriesApi@index');
    Route::get('home', 'Homepage@index');
    Route::get('home/slider', 'Homepage@slider');
    Route::any('contact', 'Contact@index');
    Route::any('news', 'NewsController@index');
    Route::any('news/details/{id}', 'NewsController@details');
    Route::any('events', 'EventController@index');
    Route::any('event/details/{id}', 'EventController@details');
    Route::any('vacancies', 'VacancyController@index');
    Route::post('vacancies/apply', 'VacancyController@apply');
    Route::any('vacancy/{id}', 'VacancyController@details');
    Route::any('members/{id}', 'MembersController@index');
    Route::any('box-of-hope', 'BoxOfHopeController@index');
    Route::any('message-of-hope', 'MessageOfHopeController@index');
    Route::any('constructions', 'ConstructionsController@index');
    Route::any('whistleblowing', 'WhistleblowingController@index');
    Route::any('partners', 'PartnersController@index');
    Route::any('lets-grow', 'letsGrowController@index');
    Route::any('founders-message', 'FoundersMessageController@index');
    Route::any('framework-structure', 'FrameworkStructureController@index');
    Route::any('business-transformation', 'BusinessTransformationController@index');
    Route::any('corporate-governance', 'CorporateGovernanceController@index');
    Route::any('csr', 'CsrController@index');
    Route::any('csr/partners', 'CsrController@partners');
    Route::any('contact-page', 'ContactPageController@index');
    Route::any('test', 'Test@index');
});
